from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^logout/$', views.logoutView.as_view(), name="logout"),
    url(r'^$', views.warehouse, name='warehouse'),
    url(r'^WarehouseFilter/$', views.WarehouseFilter),
    url(r'^WrhsIn$', views.WrhsIn),
    url(r'^VolumeFilter/$', views.VolumeFilter),
    url(r'^AmountFilter/$', views.AmountFilter),
    url(r'^WrhsOut/$', views.WrhsOut),
    url(r'^wrhsPeriod/$', views.wrhsPeriod),
    url(r'^export/$',views.wrhsExport),
    url(r'^sendMessage/$', views.exportClick),
]