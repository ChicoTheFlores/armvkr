from django.contrib import admin
from .models import *


class adminCat(admin.ModelAdmin):
    model = Categories1

    def save_model(self, request, obj, form, change):
        obj.cat_title = obj.cat_title.replace(' ', '_')
        obj.save()


class adminSub(admin.ModelAdmin):
    model = Subcategories1

    def save_model(self, request, obj, form, change):
        obj.subcat_title = obj.subcat_title.replace(' ', '_')
        obj.save()


class adminGood(admin.ModelAdmin):
    model = Goods1

    def save_model(self, request, obj, form, change):
        obj.title = obj.title.replace(' ', '_')
        obj.save()


admin.site.register(Categories1, adminCat)
admin.site.register(Subcategories1, adminSub)
admin.site.register(Goods1, adminGood)
admin.site.register(WarehouseInOut)
