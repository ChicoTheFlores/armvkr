from django.contrib import admin
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.conf import settings
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('enter.urls')),
    url(r'^session/', include('session.urls')),
    url(r'^warehouse/', include('warehouse.urls')),
    url(r'^admins/', include('admins.urls')),
    url(r'^booking/', include('booking.urls')),
    url(r'^daycalendar/', include('daycalendar.urls')),
    url(r'^schedule/', include('shiftchange.urls')),
]
urlpatterns+=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)