from django.apps import AppConfig


class ShiftchangeConfig(AppConfig):
    name = 'shiftchange'
