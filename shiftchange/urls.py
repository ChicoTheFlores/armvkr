from django.conf.urls import url, include

from . import views
urlpatterns = [
    url(r'^$', views.LoginScheduleForm.as_view(), name="login_enter"),
    url(r'^Tables/$', views.schedule, name='schedule'),
    url(r'^ChoicePref/$', views.ChoicePref),
    url(r'^FreePref/$', views.FreePref),
    url(r'^FreePrefOk/$', views.FreePrefOk),
]