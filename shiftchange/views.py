from django.shortcuts import render
from .models import *
from django.contrib.auth.forms import AuthenticationForm
from session.views import logoutView
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from admins.models import *

from daycalendar.models import ShiftStart
from booking.models import *
from enter.views import LoginEnterForm
from datetime import datetime, timezone, date, timedelta
from django.views.generic.edit import FormView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login


class LoginScheduleForm(FormView):
    form_class = AuthenticationForm
    template_name = "schedule/auth.html"
    success_url = "/schedule/"

    def get(self, request):
        if request.user.is_authenticated:
            return HttpResponseRedirect("/schedule/Tables/")
        else:
            context = {}
            context["form"] = LoginScheduleForm()
            return self.render_to_response(self.get_context_data())

    def post(self, request):
        userP = request.POST['username']
        passP = request.POST['password']
        user = authenticate(username=userP, password=passP)
        if user is not None:
            if user.is_active:
                login(request, user)
                prof_object = Profile.objects.get(
                    user=User.objects.get(username=userP))
                prof_object.enter_time = datetime.now(tzl)
                prof_object.save()
                return HttpResponseRedirect("/schedule/Tables/")
            else:
                return HttpResponseRedirect("/schedule/")
        else:
            return HttpResponseRedirect("/schedule/")


def schedule(request):

    context = {}
    BaseSum = 0
    BonusSum = 0
    PremiumSum = 0
    FineSum = 0
    totalRate = 0
    listSalary = UserSalary.objects.filter(
        user=Profile.objects.get(user=request.user), is_payed=False)
    listPremium = PremiumFine.objects.filter(
        user=Profile.objects.get(user=request.user), is_payed=False)
    for l in listSalary:
        BaseSum += l.BaseSum
        BonusSum += l.BonusSum
        BonusSum += l.payedProcent
    for pf in listPremium:
        if pf.type == "P":
            PremiumSum += pf.amount
            pf.type = 'Премия'
        else:
            FineSum += pf.amount
            pf.type = 'Штраф'
    BonusSum += PremiumSum - FineSum
    if BonusSum < 0:
        BonusSum = 0
    totalRate = BonusSum + BaseSum
    context['BonusSum'] = BonusSum
    context['BaseSum'] = BaseSum
    context['totalSum'] = totalRate
    planM = PlanTotal.objects.filter(type="M")
    planE = PlanTotal.objects.filter(type="E")
    planR = PlanTotal.objects.filter(type="R")
    thisUser = request.user.username
    startHoliday = datetime.today() + timedelta(days=(datetime.today().weekday() -
                                                      2*(datetime.today().weekday())+1))
    endHoliday = datetime.today() + timedelta(days=(7-datetime.today().weekday()))
    fullHolidays = Weekends.objects.filter(
        date__gte=startHoliday).exclude(date__gt=endHoliday)
    for plan in planM:
        try:
            vac = VacantedShifts.objects.get(type=plan.type, day=plan.day)
            plan.myshift = "vacanted-shift"
        except:
            if thisUser == plan.profile.user.username:
                plan.myshift = "my-shift"

    i = 0
    for plan in planE:
        try:
            holiday = fullHolidays.get(date=startHoliday + timedelta(days=i))
            plan.isPreHoliday = True
        except:
            plan.isPreHoliday = False
        try:
            vac = VacantedShifts.objects.get(type=plan.type, day=plan.day)
            plan.myshift = "vacanted-shift"
        except:
            if thisUser == plan.profile.user.username:
                plan.myshift = "my-shift"
        i += 1
    i = 0
    for plan in planR:
        try:
            holiday = fullHolidays.get(date=startHoliday + timedelta(days=i))
            plan.isPreHoliday = True
        except:
            plan.isPreHoliday = False
        try:
            vac = VacantedShifts.objects.get(type=plan.type, day=plan.day)
            plan.myshift = "vacanted-shift"
        except:
            if thisUser == plan.profile.user.username:
                plan.myshift = "my-shift"
        i += 1
    context['planM'] = planM
    context['planE'] = planE
    context['planR'] = planR
    prefsE = Profile.objects.get(user=User.objects.get(
        username=request.user)).prefs.filter(type="E")
    i = 0
    for pref in prefsE:
        try:
            holiday = fullHolidays.get(date=startHoliday + timedelta(days=i))
            pref.isPreHoliday = True
        except:
            pref.isPreHoliday = False
        i += 1
    context['prefsM'] = Profile.objects.get(
        user=User.objects.get(username=request.user)).prefs.filter(type="M")
    context['prefsE'] = prefsE

    if PlanChanged.objects.first().profile != None:
        context['isSunday'] = True
    else:
        context['isSunday'] = False
    planChangedE = PlanChanged.objects.filter(type="E")
    planChangedR = PlanChanged.objects.filter(type="R")

    startHoliday += timedelta(days=7)
    endHoliday += timedelta(days=7)
    fullHolidays = Weekends.objects.filter(
        date__gte=startHoliday).exclude(date__gt=endHoliday)
    i = 0
    for pref in planChangedE:
        try:
            holiday = fullHolidays.get(date=startHoliday + timedelta(days=i))
            pref.isPreHoliday = True
        except:
            pref.isPreHoliday = False
        i += 1
    i = 0
    for pref in planChangedR:
        try:
            holiday = fullHolidays.get(date=startHoliday + timedelta(days=i))
            pref.isPreHoliday = True
        except:
            pref.isPreHoliday = False
        i += 1
    context['salary'] = listSalary
    context['premium'] = listPremium
    context['planChangedM'] = PlanChanged.objects.filter(type="M")
    context['planChangedE'] = planChangedE
    context['planChangedR'] = planChangedR
    context['weekendTime'] = ShiftStart.objects.first().timeweekend
    return render(request, 'schedule/schedule.html', context)


@csrf_exempt
def ChoicePref(request):
    if (request.method == "POST"):
        if request.is_ajax():
            Nice = request.POST['dict'].split(';')[:-1]
            i = 0
            countMinus = 0
            shift = "M"
            daysAll = Days.objects.all()
            profile = Profile.objects.get(
                user=User.objects.get(username=request.user))
            prof = profile.prefs.all()
            for n in Nice:
                obj = prof.filter(day=daysAll[i], type=shift).first()
                obj.pref = n
                obj.save()
                if n == "-":
                    countMinus += 1
                i += 1
                if i == 7:
                    shift = "E"
                    i = 0
            profile.restB = countMinus
            profile.save()
            return JsonResponse(onemes("good"))


@csrf_exempt
def FreePref(request):
    if (request.method == "POST"):
        if request.is_ajax():
            Free = request.POST['dict'].split(',')[:-1]
            for free in Free:
                parsefree = free.split("-")
                vac = VacantedShifts.objects.create(day=Days.objects.get(
                    id=parsefree[1]), type=parsefree[0], profile=Profile.objects.get(user=request.user))
                vac.save()
            return JsonResponse(onemes("good"))


@csrf_exempt
def FreePrefOk(request):
    if (request.method == "POST"):
        if request.is_ajax():
            pr = Profile.objects.get(user=request.user)
            Free = request.POST['dict'].split(',')[:-1]
            com = dict()
            c = 0
            for free in Free:
                parsefree = free.split("-")
                vac = VacantedShifts.objects.get(
                    day=Days.objects.get(id=parsefree[1]), type=parsefree[0])
                if parsefree[0] == "E" and PlanTotal.objects.get(day=Days.objects.get(id=parsefree[1]), type="R").profile == pr or parsefree[0] == "R" and PlanTotal.objects.get(day=Days.objects.get(id=parsefree[1]), type="E").profile == pr:
                    pass
                else:
                    tp = PlanTotal.objects.get(day=Days.objects.get(
                        id=parsefree[1]), type=parsefree[0])
                    tp.profile = pr
                    tp.save()
                    vac.delete()
                    com.update({c: free})
                    c += 1
            return JsonResponse(FourDict(pr.user.username, pr.color, com, c))
