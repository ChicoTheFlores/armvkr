import os
# import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "AWPkamenka.settings")
# django.setup()
import time
from admins.models import *
import schedule
from schedule import *
from daycalendar.models import SettingStart

def buildChangedPlan():
    admins = Profile.objects.all()
    shifts = PlanChanged.objects.exclude(type="R")
    for shift in shifts:
        admin = admins.filter(prefs__day = shift.day, prefs__type = shift.type, prefs__pref = "+").order_by("countG", "countB", "-restB").first()
        if admin != None:
            shift.profile = admin
            shift.save()
            admin.countG += 1
            admin.save()
    shifts = PlanChanged.objects.exclude(type="R").filter(profile = None)
    for shift in shifts:
        admin = admins.filter(prefs__day=shift.day, prefs__type=shift.type, prefs__pref="=").order_by("countG", "countB", "-restB").first()
        if admin != None:
            shift.profile = admin
            shift.save()
            admin.countG += 1
            admin.save()
    shifts = PlanChanged.objects.exclude(type="R").filter(profile = None)
    for shift in shifts:
        admin = admins.filter(prefs__day = shift.day, prefs__type = shift.type).exclude(id=PlanChanged.objects.get(day=shift.day, type="M").profile.id).order_by("countG", "countB", "-restB").first()
        if admin != None:
            # if admin == PlanChanged.objects.filter(day = shift.day, type = "M"):
            shift.profile = admin
            shift.save()
            admin.countG += 1
            admin.countB += 1
            admin.restB -= 1
            admin.save()
    shifts = PlanChanged.objects.exclude(type="R").filter(profile=None)
    for shift in shifts:
        admin = admins.filter(prefs__day = shift.day, prefs__type = shift.type).order_by("countG", "countB", "-restB").first()
        shift.profile = admin
        shift.save()
        admin.countG += 1
        admin.countB += 1
        admin.restB -= 1
        admin.save()
    shifts = PlanChanged.objects.filter(type="R")
    for shift in shifts:
        admin = admins.filter(prefs__day=shift.day, prefs__type="E").exclude(id=PlanChanged.objects.get(day=shift.day, type="E").profile.id).order_by("-countG", "-countB", "restB")
        for a in admin:
            if a.prefs.get(day=shift.day, type="E").pref != "-":
                shift.profile = a
                shift.save()
                a.countG+=1
                a.save()
                if a.prefs.get(day=shift.day, type="E").pref == "+":
                    break
    for shift in shifts.filter(profile = None):
        admin = admins.filter(prefs__day=shift.day, prefs__type="E").exclude(id=PlanChanged.objects.get(day=shift.day, type="E").profile.id).order_by("countB", "-restB", "countG").first()
        shift.profile = admin
        shift.save()
        admin.countG += 1
        admin.countB += 1
        admin.restB -= 1
        admin.save()
    for admin in Profile.objects.all():
        admin.countG = 0
        admin.countB = 0
        admin.restB = 0
        for pref in admin.prefs.all():
            pref.pref = "="
            pref.save()
        admin.save()



def removeChangedPlan():
    planChanged = PlanChanged.objects.all().order_by("day", "type")
    for pc in planChanged:
        obj = PlanTotal.objects.get(day = pc.day, type = pc.type)
        obj.profile = pc.profile
        obj.save()
        pc.profile = None
        pc.save()
    vacanted = VacantedShifts.objects.all()
    for vac in vacanted:
        vac.delete()
def Teeeest():
    obj = SettingStart.objects.first()
    obj.timetodelete = 3
    obj.save()
# schedule.every().saturday.at("00:01").do(buildChangedPlan)
# schedule.every().monday.at("04:00").do(removeChangedPlan)
# schedule.every().tuesday.at("17:00").do(Teeeest)
#
# while True:
#     schedule.run_pending()
#     time.sleep(1)

