from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.admins, name='admins'),
    url(r'^add-reserved/(?P<adminname>\w+)$',
        views.LoginReservedAdminForm.as_view()),
    url(r'^remove-reserved/$', views.remove_reserved),
    url(r'^changecat/$', views.change_cat),
    url(r'^changeperiod/$', views.change_period),
    url(r'^cashregPeriod/$', views.cashregPeriod),
    url(r'^selectOperation/$', views.selectOperation),
    url(r'^getDailyReport/$', views.getDailyReport),
    url(r'^transfer/(?P<adminname>\w+)$', views.transferAdminForm.as_view()),

]
