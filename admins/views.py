
from django.shortcuts import render
from booking.models import *
from .models import *
from daycalendar.models import *
from django import forms
import locale
from django.views.decorators.csrf import csrf_exempt
from enter.models import *
from django.views.generic.edit import FormView
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login
from daycalendar.models import ShiftStart
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from session.views import toFixed, computeAdminZP
from AWPkamenka.timework import *
from django.contrib.auth import logout
from .forms import NewOrderForm, NewPremiumFineForm
from django.db.models import F
from django.forms.widgets import TimeInput, TextInput
from django import forms


class LoginReservedAdminForm(FormView):
    form_class = AuthenticationForm
    template_name = "admins/auth.html"
    success_url = "/admins/add-reserved/"

    def get(self, request, adminname, *args, **kwargs):
        context = {"form": LoginReservedAdminForm()}
        return self.render_to_response(self.get_context_data())

    def post(self, request, adminname, *args, **kwargs):
        userP = request.POST['username']
        passP = request.POST['password']
        if userP == adminname:
            return HttpResponseRedirect("/admins/add-reserved/" + adminname)
        user = authenticate(username=userP, password=passP)
        if user is not None and user.is_active:
            shift = PlanTotal.objects.get(
                type="R", day=Days.objects.get(id=datetime.today().weekday() + 1))
            if shift.eveningActivity:
                prof_object = Profile.objects.get(
                    user=User.objects.get(username=userP))
                t_now = datetime.now(tzl)
                t_tra = t_now.replace(hour=ShiftStart.objects.first().timetransfer.hour,
                                      minute=ShiftStart.objects.first().timetransfer.minute)
                if t_now <= t_tra and (t_tra - t_now).seconds <= 600:
                    prof_object.enter_time = t_tra
                elif t_now >= t_tra and (t_now - t_tra).seconds <= 600:
                    prof_object.enter_time = t_tra
                else:
                    prof_object.enter_time = t_now

                prof_object.reservedWork = True
                prof_object.save()
                return HttpResponseRedirect("/admins/")
            else:
                return HttpResponseRedirect("/admins/add-reserved/" + adminname)
        else:
            return HttpResponseRedirect("/admins/add-reserved/" + adminname)


def getNormalizeTime():
    t_now = datetime.now(tzl)
    t_tra = t_now.replace(hour=ShiftStart.objects.first(
    ).timetransfer.hour, minute=ShiftStart.objects.first().timetransfer.minute)
    if t_now <= t_tra and (t_tra - t_now).seconds <= 600:
        return t_tra.time().strftime("%H:%M")
    elif t_now >= t_tra and (t_now - t_tra).seconds <= 600:
        return t_tra.time().strftime("%H:%M")
    else:
        return t_now.time().strftime("%H:%M")


class RFPAuthForm(AuthenticationForm):
    transferTime = forms.TimeField(widget=TimeInput(
        attrs={'type': 'time'}), label="Время пересмены:", initial=getNormalizeTime())


class transferAdminForm(FormView):
    form_class = RFPAuthForm
    template_name = "admins/transfer.html"
    success_url = "/admins/transfer/(?P<adminname>\w+)"

    def get(self, request, adminname, *args, **kwargs):
        context = {"formTransfer": transferAdminForm()}
        return self.render_to_response(self.get_context_data())

    def post(self, request, adminname, *args, **kwargs):
        userP = request.POST['username']
        passP = request.POST['password']
        timeEndTr = request.POST['transferTime']
        if userP == adminname:
            return HttpResponseRedirect("/admins/transfer/" + adminname)
        shift = ShiftStart.objects.first()
        datetimeEndTr = datetime.now(tzl).replace(
            hour=timeEndTr.hour, minute=timeEndTr.minute)
        prof_object = Profile.objects.get(
            user=User.objects.get(username=adminname))
        if prof_object.user.is_superuser:
            logout(request)
            return HttpResponseRedirect("/")
        else:
            obj = UserSalary.objects.create(user=prof_object,
                                            record_type="Рабочая смена",
                                            datetime_start=prof_object.enter_time.astimezone(
                                                tzl),
                                            datetime_end=datetimeEndTr)
            shiftTimeStart = datetime.now(tzl).replace(
                hour=shift.timestart.hour, minute=shift.timestart.minute, second=0)
            if obj.datetime_start.time() < shift.timestart and obj.datetime_start.time() > shift.timeweekend:
                obj.datetime_start = shiftTimeStart
            dts = obj.datetime_start.astimezone(tzl)
            e_m = obj.datetime_end.minute / 60
            toFixed(e_m, 2)
            s_m = dts.minute / 60
            toFixed(s_m, 2)
            e_h = obj.datetime_end.hour + e_m
            s_h = dts.hour + s_m
            obj.amount = e_h - s_h
            if obj.amount < 0:
                obj.amount = 24 + obj.amount
            obj.BaseSum = obj.user.baseRate * obj.amount
            obj.BonusSum = obj.user.bonusRate * obj.amount
            obj.save()
        logout(request)
        user = authenticate(username=userP, password=passP)
        if user is not None and user.is_active:
            login(request, user)
            if request.user.groups.filter(name='Админ').exists():
                prof_object = Profile.objects.get(
                    user=User.objects.get(username=userP))
                prof_object.enter_time = datetimeEndTr
                prof_object.save()
                return HttpResponseRedirect("/admins/")
            else:
                return HttpResponseRedirect("/admins/transfer/" + adminname)
        else:
            return HttpResponseRedirect("/admins/transfer/" + adminname)


def remove_reserved(request):
    try:
        shift = ShiftStart.objects.first()
        res_object = Profile.objects.get(reservedWork=True)
        if res_object.user.is_superuser:
            logout(request)
            return HttpResponseRedirect("/")
        else:
            objR = UserSalary.objects.create(user=res_object, record_type="Рабочая смена",
                                             datetime_start=res_object.enter_time.astimezone(
                                                 tzl),
                                             datetime_end=None)
            shiftTimeStart = datetime.now(tzl).replace(
                hour=shift.timestart.hour, minute=shift.timestart.minute, second=0)
            if objR.datetime_start.time() < shift.timestart and objR.datetime_start.time() > shift.timeweekend:
                objR.datetime_start = shiftTimeStart
            res_object.reservedWork = False
            res_object.save()

            start = date(objR.datetime_start.year,
                         objR.datetime_start.month, objR.datetime_start.day)
            try:
                weekend = Weekends.objects.get(date=start + timedelta(days=1))
                end = datetime.now(tzl).replace(
                    hour=shift.timeweekend.hour, minute=shift.timeweekend.minute, second=0)
            except:
                end = datetime.now(tzl).replace(
                    hour=shift.timeend.hour, minute=shift.timeend.minute, second=0)
            if end < shiftTimeStart:
                end += timedelta(days=1)

            computeAdminZP(objR, end)
    except:
        pass
    return HttpResponseRedirect("/admins/")


def admins(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/")
    if request.method == 'POST':
        if "name" in request.POST:
            formOrder = NewOrderForm(request.POST)
            if formOrder.is_valid():
                order = formOrder.save(commit=False)
                order.admin = Profile.objects.get(user=request.user)
                if formOrder.cleaned_data["zpadmin"] is not None:
                    order.comment = str(formOrder.cleaned_data["zpadmin"])
                else:
                    order.comment = str(formOrder.cleaned_data["comment"])
                if MoneyOperations.objects.filter(name=formOrder.cleaned_data["cat"]).first().is_one == False:
                    if formOrder.cleaned_data["newSubcat"] != "":
                        typeofMO = TypeOfMO.objects.create(name=formOrder.cleaned_data["newSubcat"], moneyoper=MoneyOperations.objects.filter(
                            name=formOrder.cleaned_data["cat"]).first())
                        order.name = typeofMO
                    else:
                        order.name = TypeOfMO.objects.filter(
                            name=formOrder.cleaned_data["name"]).first()
                else:
                    order.name = TypeOfMO.objects.filter(
                        name=formOrder.cleaned_data["name"], moneyoper__name=formOrder.cleaned_data["name"]).first()
                order.save()
                if order.amount > 0:
                    cash = OperatingCash.objects.first()
                    if order.name.moneyoper.type == "I":
                        cash.cash_sum_now += order.amount
                    else:
                        cash.cash_sum_now -= order.amount
                    cash.save()
                if order.name.moneyoper.is_adminpay:
                    try:
                        period = formOrder.cleaned_data["datepicker"].split(
                            " ~ ")
                        dateSt = date(int(period[0].split(".")[2]), int(period[0].split(".")[1]),
                                      int(period[0].split(".")[0]))
                        dateEnd = date(int(period[1].split(".")[2]), int(period[1].split(".")[1]),
                                       int(period[1].split(".")[0]))
                    except:
                        return HttpResponseRedirect('/admins/')

                    if order.name.name == "Аванс":
                        adminZP = list(
                            UserSalary.objects.filter(user=formOrder.cleaned_data["zpadmin"], is_payed=False,
                                                      datetime_end__date__lte=dateEnd, datetime_end__date__gte=dateSt))[:-3]
                    else:
                        adminZP = list(
                            UserSalary.objects.filter(user=formOrder.cleaned_data["zpadmin"], is_payed=False,
                                                      datetime_end__date__lte=dateEnd, datetime_end__date__gte=dateSt))
                    for zp in adminZP:
                        zp.is_payed = True
                        zp.save()
                    premfine = PremiumFine.objects.filter(
                        user=formOrder.cleaned_data["zpadmin"], is_payed=False, date__range=(dateSt, dateEnd))
                    for pf in premfine:
                        pf.is_payed = True
                        pf.save()
                order.save()
                return HttpResponseRedirect('/admins/')
            else:
                return HttpResponse(str(formOrder.errors) + "<br><a href='/admins/'>Вернуться назад</a>")
        else:
            formPremiumFine = NewPremiumFineForm(request.POST)
            if formPremiumFine.is_valid():
                premiumFine = formPremiumFine.save(commit=False)
                premiumFine.save()
                return HttpResponseRedirect('/admins/')
            else:
                return HttpResponse(str(formOrder.errors) + "<br><a href='/admins/'>Вернуться назад</a>")
    else:
        context = {}
        try:
            context["resAdm"] = "(" + str(Profile.objects.get(
                reservedWork=True).user.username) + ")"
        except:
            pass
        operatingCash = OperatingCash.objects.all().first()
        countSessions = Session.objects.filter(end_dt=None).count()

    
        weekends = Weekends.objects.all().order_by("date")
        context['weekends'] = weekends
        context['operatingCash'] = operatingCash
        context['countSessions'] = countSessions
        context['operatingCash'] = operatingCash
        context['formNewOrder'] = NewOrderForm()
        context['admin_groups'] = request.user.groups.filter(
            name='Админ').exists()
        context['formPremiumFine'] = NewPremiumFineForm()
        orders = Orders.objects.filter(date__date=date.today())
        for order in orders:
            order.type = order.name.moneyoper.get_type_display()
        context['orders'] = orders
        if request.user.is_superuser or request.user.groups.filter(name="Директор").exists():
            context["director"] = True
        context['userList'] = Profile.objects.all()
        context['cats'] = MoneyOperations.objects.all()
        context['subs'] = TypeOfMO.objects.all()
        # context['salary'] = listSalary
        return render(request, 'admins/admins.html', context)


@csrf_exempt
def change_cat(request):
    if (request.method == "POST"):
        idCat = request.POST['idCat']
        moneyoperObj = MoneyOperations.objects.get(id=idCat)
        con = dict()
        subcat = dict()
        i = 0
        for sub in TypeOfMO.objects.filter(moneyoper=moneyoperObj):
            subcat.update({i: [sub.id, sub.name]})
            i += 1
        if moneyoperObj.is_one:
            con.update({0: [moneyoperObj.is_readonlyamount, "1"]})
        else:
            con.update({0: [moneyoperObj.is_readonlyamount, "m"]})
        con.update({1: i})
        con.update({2: subcat})
        con.update({3: moneyoperObj.is_adminpay})
        return JsonResponse(onemes(con))


@csrf_exempt
def change_period(request):
    if (request.method == "POST"):
        period = request.POST['idCat'].split(" ~ ")
        dateSt = date(int(period[0].split(".")[2]), int(
            period[0].split(".")[1]), int(period[0].split(".")[0]))
        dateEnd = date(int(period[1].split(".")[2]), int(
            period[1].split(".")[1]), int(period[1].split(".")[0]))
        idSubcat = request.POST['Subcat']
        idProfile = request.POST['idProfile']
        profile = Profile.objects.get(id=idProfile)
        subcat = TypeOfMO.objects.get(id=idSubcat)
        if subcat.name == "Аванс":
            adminZP = list(UserSalary.objects.filter(user=profile, is_payed=False,
                                                     datetime_end__date__lte=dateEnd, datetime_end__date__gte=dateSt))[:-3]
        else:
            adminZP = list(UserSalary.objects.filter(user=profile, is_payed=False,
                                                     datetime_end__date__lte=dateEnd, datetime_end__date__gte=dateSt))
        totalSum = 0
        bonusSum = 0
        viruchkaSum = 0
        premiumSum = 0
        fineSum = 0
        for zp in adminZP:
            totalSum += zp.BaseSum
            bonusSum += zp.BonusSum
            viruchkaSum += zp.payedProcent
        totalSum = int(totalSum)
        bonusSum = int(bonusSum)
        viruchkaSum = int(viruchkaSum)
        premfine = PremiumFine.objects.filter(
            user=profile, is_payed=False, date__range=(dateSt, dateEnd))
        for pf in premfine:
            if pf.type == "P":
                premiumSum += pf.amount
            else:
                fineSum += pf.amount
        bonusSum += premiumSum + viruchkaSum - fineSum
        if bonusSum < 0:
            bonusSum = 0
        totalSum += bonusSum
        con = dict()
        con.update({0: totalSum})

        return JsonResponse(onemes(con))


def cashregPeriod(request):
    if (request.method == "POST"):
        if request.is_ajax():
            start = request.POST['start']
            end = request.POST['end']
            lfP = set()
            totalsum = 0
            locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
            sD = datetime.strptime(start, '%Y-%m-%d')
            eD = datetime.strptime(end, "%Y-%m-%d") + \
                timedelta(days=1) - timedelta(seconds=1)
            plist = Orders.objects.filter(date__range=(sD, eD)).values('date', 'amount', 'comment', odmen=F(
                'admin__user__username'), operation=F('name__moneyoper__name'), detailed=F('name__name'), type=F('name__moneyoper__type'))
            for p in plist:
                date = str(p['date'].day) + '-' + str(p['date'].month) + '-' + str(
                    p['date'].year) + ' ' + str(p['date'].hour) + ':' + str(p['date'].minute)
                p['date'] = p['date'].strftime('%d %B %Y %H:%M').format()
                if p['type'] == 'I':
                    p['type'] = 'Приход'
                else:
                    p['type'] = 'Расход'
            lfP = list(plist)
            return JsonResponse({"lfP": lfP})


def selectOperation(request):
    if (request.method == "POST"):
        if request.is_ajax():
            cat = request.POST['cat']
            aG = TypeOfMO.objects.all()
            if (cat != "hide"):
                aG = aG.filter(moneyoper__name=cat)
            allG = set()
            for g in aG:
                allG.add(g.name)
            allG = list(allG)
            return JsonResponse({'aG': allG})


def getDailyReport(request):
    if request.is_ajax():
        pass
