
$(document).ready(function () {
    $('.table-cash-content-tr').each(function () {
        var type = ($(this).find('.table-cash-content-tr-type').text());
        if (type == "Приход") $(this).css({ color: 'green' })
        else $(this).css({ color: 'red' });
    });
    $(document).ready(function () {
        computeTotalSum();
        $('#cashregPeriodBtn').click(function () {
            start = $('#cashregPeriod_start').val();
            end = $('#cashregPeriod_end').val();
            sD = new Date(start);
            eD = new Date(end);
            delta = eD.getTime() - sD.getTime();
            if (delta < 0) {
                alert('Неправильно введенные даты!')
            }
            else {
                $.ajax({
                    method: "POST",
                    url: "/admins/cashregPeriod/",
                    data: {
                        'start': start,
                        'end': end,
                    },
                    dataType: 'json',
                    success: function (data) {
                        table = '';
                        for (i = 0; i < data.lfP.length; i++) {
                            table = table + '<tr class="table-cash-content-tr">' + '<td class="table-cash-content-tr-profile">' + data.lfP[i].odmen + '</td>' + '<td class="table-cash-content-tr-date">' + data.lfP[i].date + '</td>' + '<td class ="table-cash-content-tr-type">' + data.lfP[i].type + '</td>' + '<td class=" table-cash-content-tr-operation">' + data.lfP[i].operation + '</td>' + '<td class="table-cash-content-tr-detailed">' + data.lfP[i].detailed + '</td>' + '<td class="table-cash-content-tr-sum">' + data.lfP[i].amount + '</td><td class="table-cash-content-tr-comment">' + data.lfP[i].comment + '</td></tr>';
                        }

                        $('#cashregperiodTable tbody').html(table);
                        $('body').find('.table-cash-content-tr').each(function () {
                            var type = ($(this).find('.table-cash-content-tr-type').text());
                            if (type == "Приход") $(this).css({ color: 'green' })
                            else $(this).css({ color: 'red' });
                        });
                        computeTotalSum();
                    }
                })
            }


        })
        $("#id_datepicker").dateRangePicker({
            startOfWeek: 'monday',
            separator: ' ~ ',
            format: 'DD.MM.YYYY',
            autoClose: false,
            language: 'ru',
            endDate: moment().startOf('month').subtract(1, 'days').format('DD.MM.YYYY')
        });

        $(".date-picker-wrapper").addClass("topWrapper");
        $("#id_datepicker").parent().addClass("groupHidden");
        $("#id_zpadmin").parent().addClass("groupHidden");
        $("#modalAddingOrder #id_name option").not(':selected').each(function () {
            $(this).prop('hidden', true);
        })
        $("#modalAddingOrder #id_admin").parent().addClass("groupHidden");

        $("body").on("change", "#modalAddingOrder #id_cat", function (e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                url: "/admins/changecat/",
                data: {
                    'idCat': $(this).val(),
                },
                headers: { 'X-CSRFToken': '{{ csrf_token }}' },
                dataType: 'json',
                success: function (data) {
                    $("#modalAddingOrder #id_amount").prop("readonly", data.str[0][0]);
                    $("#modalAddingOrder #id_amount").val(Number(0));
                    $("#modalAddingOrder #id_name option").remove();
                    if (data.str[0][1] == "1") {
                        $("#modalAddingOrder #id_newSubcat").parent().addClass("groupHidden");
                        printOption($("#modalAddingOrder #id_name"), data.str[1], data.str[2]);
                        $("#modalAddingOrder #id_name option:first").prop("selected", true);
                        $("#modalAddingOrder #id_name").parent().addClass("groupHidden");
                    }
                    else {
                        $("#modalAddingOrder #id_newSubcat").parent().removeClass("groupHidden");
                        option = '<option value="" selected="">---------</option>'
                        $("#modalAddingOrder #id_name").append(option);
                        printOption($("#modalAddingOrder #id_name"), data.str[1], data.str[2]);
                        $("#modalAddingOrder #id_name").parent().removeClass("groupHidden");
                    }
                    if (data.str[3] == true) {
                        $("#id_datepicker").parent().removeClass("groupHidden");
                        $("#id_zpadmin").parent().removeClass("groupHidden");
                        $("#modalAddingOrder #id_newSubcat").parent().addClass("groupHidden");
                        $("#modalAddingOrder #id_name option:first").remove();
                        $("#modalAddingOrder #id_name option:last").prop("selected", true);
                        $("#modalAddingOrder #id_comment").parent().addClass("groupHidden");

                    }
                    else {
                        $("#id_datepicker").parent().addClass("groupHidden");
                        $("#id_zpadmin").parent().addClass("groupHidden");
                        $("#modalAddingOrder #id_comment").parent().removeClass("groupHidden");

                    }
                }
            })
        })
        $("body").on("focus", "#modalAddingOrder", function (e) {
            e.preventDefault();
            if ($("#modalAddingOrder #id_datepicker").val() != "" && $("#modalAddingOrder #id_zpadmin").val() != "") {
                $.ajax({
                    method: "POST",
                    url: "/admins/changeperiod/",
                    data: {
                        'idCat': $("#modalAddingOrder #id_datepicker").val(),
                        'idProfile': $("#modalAddingOrder #id_zpadmin").val(),
                        'Subcat': $("#modalAddingOrder #id_name").val(),
                    },
                    headers: { 'X-CSRFToken': '{{ csrf_token }}' },
                    dataType: 'json',
                    success: function (data) {
                        console.log(data.str);
                        $("#modalAddingOrder #id_amount").val(Number(data.str[0]));
                    }
                })
            }
        })

        $('.admins_tab').click(function () {
            if (!$(this).hasClass("active")) {
                var i = $(this).index();
                $('.admins_tab').removeClass('active');
                $('.adminsTab').hide().removeClass('active');
                $(this).addClass('active');
                $($("#adminsTabs").children(".adminsTab")[i - 1]).fadeIn(1000).addClass("active");
                var activeWidth = $(this).innerWidth();
                var itemPos = $(this).position();
                $(".adminsSelector").css({
                    "left": itemPos.left + "px",
                    "width": activeWidth + "px"
                });
            }
        })

        var tabs = $('#adminsTabsMenu');
        var selector = $('#adminsTabsMenu').find('a').length;
        var activeItem = tabs.find('.active');
        var activeWidth = activeItem.innerWidth();
        $(".adminsSelector").css({
            "left": activeItem.position().left + "px",
            "width": activeWidth + "px"
        });
    })


    var today = moment().format('YYYY-MM-DD');
    var lastM = moment().subtract(1, 'months').format('YYYY-MM-DD');
    $('#cashregPeriod_start').val(lastM);
    $('#cashregPeriod_end').val(today);
});

function printOption(place, counter, mas) {
    for (var index = 0; index < counter; index++) {
        option = '<option value="' + mas[index][0] + '">' + mas[index][1] + '</option>';
        place.append(option);
    }
}

function computeTotalSum() {
    $td = '#cashregTotalSum';
    $sum = 0;
    $('body').find('.table-cash-content-tr').each(function () {
        if ($(this).css('display') != 'none') {
            if ($(this).find('.table-cash-content-tr-type').text() == 'Приход') {
                $sum += parseInt($(this).find('.table-cash-content-tr-sum').text());
            }

            else {
                $sum -= parseInt($(this).find('.table-cash-content-tr-sum').text());
            }
        }
    })
    $($td).text($sum);
}