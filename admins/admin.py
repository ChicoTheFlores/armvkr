from django.contrib import admin
from django.utils.safestring import mark_safe
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import *
# Register your models here.


class UserSalaryAdmin(admin.ModelAdmin):
    model = UserSalary
    list_display = ["user", "record_type", "BaseSum"]
    list_filter = ["user", "record_type"]


admin.site.register(UserSalary, UserSalaryAdmin)
admin.site.register(TypeOfMO)
admin.site.register(Orders)
admin.site.register(PremiumFine)


class WeekendsAdmin(admin.ModelAdmin):
    model = Weekends
    list_display = ["name", "date"]


admin.site.register(Weekends, WeekendsAdmin)


class MoneyOperationsAdmin(admin.ModelAdmin):
    model = MoneyOperations

    def save_model(self, request, obj, form, change):
        obj.name = obj.name.replace(' ', '_')
        obj.save()
        if obj.is_one:
            try:
                object = TypeOfMO.object.get(moneyoper=obj)
                object.name = obj.name
            except:
                object = TypeOfMO.objects.create(name=obj.name, moneyoper=obj)
            object.save()


admin.site.register(MoneyOperations, MoneyOperationsAdmin)


def eveningActivityOnOff(modeladmin, request, queryset):
    for elem in queryset:
        elem.eveningActivity = not elem.eveningActivity
        elem.save()


eveningActivityOnOff.short_description = "Активность Резерной Смены On/Off"


admin.site.register(Profile)


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    list_display = [' baseRate ', 'bonusRate']
    list_select_related = ('profile',)
    verbose_name_plural = 'Профиль пользователя'
    fk_name = 'user'

# Define a new User admin


class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline,)

    def new_profile(modeladmin, request, queryset):
        for set in queryset:
            user = set.profile
            for d in Days.objects.all():
                p1 = Preferences.objects.create(day=d, type="M")
                p2 = Preferences.objects.create(day=d, type="E")
                user.prefs.add(p1)
                user.prefs.add(p2)
            user.save()
    new_profile.short_description = "Новый админ"
    actions = [new_profile]

    def get_location(self, instance):
        return instance.profile.location

    get_location.short_description = 'Location'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)


class PrefsAdmin(admin.ModelAdmin):
    model = Preferences
    # actions = [new_profile]
    list_filter = ["pref", "day", "type"]
    list_display = ["prefsByAdmin", "pref", "day", "type"]

    def prefsByAdmin(self, obj):
        admin = ""
        try:
            admin = Profile.objects.get(prefs=obj).user.username
        except:
            pass
        return mark_safe('<b style="color: #900;">'+admin+'</b>')
    prefsByAdmin.short_description = "Администратор"


admin.site.register(Preferences, PrefsAdmin)


class PlanTotalAdmin(admin.ModelAdmin):
    model = PlanTotal
    actions = [eveningActivityOnOff]
    list_filter = ["profile", "day", "type"]
    list_display = ["profile", "day", "type", "eveningActivity"]


admin.site.register(PlanTotal, PlanTotalAdmin)


class PlanChangedAdmin(admin.ModelAdmin):
    model = PlanChanged
    list_filter = ["profile", "day", "type"]
    list_display = ["profile", "day", "type"]


admin.site.register(PlanChanged, PlanChangedAdmin)


class VacantedShiftsAdmin(admin.ModelAdmin):
    model = VacantedShifts
    list_filter = ["profile", "day", "type"]
    list_display = ["profile", "day", "type"]


admin.site.register(VacantedShifts, VacantedShiftsAdmin)
