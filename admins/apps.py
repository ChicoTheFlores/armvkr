from django.apps import AppConfig


class AdminsConfig(AppConfig):
    name = 'admins'
    verbose_name = "Администраторы"