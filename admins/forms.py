from django.contrib.admin import widgets
from django.forms import ModelForm, CheckboxSelectMultiple, Textarea, TimeInput, DateInput, CharField, MultiValueField, ChoiceField, MultiWidget, TextInput, Select
from .models import *
from django import forms
from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from AWPkamenka.settings import TIME_ZONE
import pytz
tzl = pytz.timezone(TIME_ZONE)


class NewOrderForm(ModelForm):
    cat = forms.ModelChoiceField(MoneyOperations.objects.filter(
        is_hidden=False), label="Выберите ордер", required=False)
    zpadmin = forms.ModelChoiceField(Profile.objects.filter(
        user__groups__name='Админ'), label="Администратор", required=False)
    newSubcat = forms.CharField(required=False, label="Или введите новый",
                                max_length=50, widget=TextInput(attrs={'placeholder': ''}))
    datepicker = forms.CharField(required=False, label="Выберите период:",
                                 max_length=50, widget=TextInput(attrs={'placeholder': ''}))
    field_order = ['cat', 'name', 'newSubcat', 'zpadmin', 'datepicker']

    class Meta:
        model = Orders
        fields = ('name', 'amount', 'comment', 'admin')
        widgets = {
            'amount': TextInput(attrs={'type': 'number', 'readonly': 'true'}),
            'admin': TextInput(attrs={'type': 'text', 'readonly': 'true'}),
        }


class NewPremiumFineForm(ModelForm):
    class Meta:
        model = PremiumFine
        fields = ('type', 'user', 'amount', 'date', 'comment')
        widgets = {
            'date': DateInput(attrs={'type': 'date'}),
        }
