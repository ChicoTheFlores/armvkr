$(document).ready(function(){
var shift = sessionStorage["shiftBook"];
    if(sessionStorage.getItem('key')){
        if (sessionStorage.getItem('sidActivateFromCalendar')){
            reviewBooking(sessionStorage["sidActivateFromCalendar"]);
            sessionStorage.removeItem("key");
            sessionStorage.removeItem("sidActivateFromCalendar");
        }
        else{
            $("#modalAddingBooking").modal('show');
            startBook = sessionStorage["startBook"].split("-");
            if(startBook[0].length == 1){
                startBook[0] = "0" + startBook[0];
                Start = startBook+24;
            }
            if(startBook[1].length == 1){
                startBook[1] = "0" + startBook[1];
            }
            $("#modalAddingBooking #id_time_start").val(startBook[0] +":" + startBook[1] +":" + "00");

            endBook = sessionStorage["endBook"].split("-");
            if(endBook[0].length == 1){
                endBook[0] = "0" + endBook[0];
            }
            if(endBook[1].length == 1){
                endBook[1] = "0" + endBook[1];
            }
            $("#modalAddingBooking #id_time_end").val(endBook[0] +":" + endBook[1] +":" + "00");

            dBook = sessionStorage["durationBook"].split("-");
            if(dBook[0].length == 1){
                dBook[0] = "0" + dBook[0];
            }
            if(dBook[1].length == 1){
                dBook[1] = "0" + dBook[1];
            }
            $("#modalAddingBooking #id_duration").val(dBook[0] +":" + dBook[1] +":" + "00");

            $('#modalAddingBooking #id_room').val(sessionStorage["roomBook"]);
            $('#modalAddingBooking #id_tariff').val(3);
            $('#modalAddingBooking #id_start_booking').val(sessionStorage["dateBook"]);
            sessionStorage.removeItem("key");
            sessionStorage.removeItem("startBook");
            sessionStorage.removeItem("endBook");
            sessionStorage.removeItem("roomBook");
            sessionStorage.removeItem("dateBook");

            sessionStorage.removeItem("durationBook");
        }
    }
    $('#id_duration').on('change',function () {
        $("#modalAddingBooking #id_time_end").val(changeTime($("#modalAddingBooking #id_time_start").val(), $("#modalAddingBooking #id_duration").val()));
    })
    $('#modalAddingBooking #id_time_end').on('change',function () {
        $("#modalAddingBooking #id_duration").val(creaseData($("#modalAddingBooking #id_time_start").val(), $("#modalAddingBooking #id_time_end").val()));
    })
    $('#modalAddingBooking #id_time_start').on('change',function () {
        $("#modalAddingBooking #id_duration").val(creaseData($("#modalAddingBooking #id_time_start").val(), $("#modalAddingBooking #id_time_end").val()));
    })
    function creaseData(a ,b){
        var shift = sessionStorage["shiftBook"];
        a1 = a;
        b1 = b;
        if ((Number(a.split(":")[0]) < Number(shift.split("-")[0])) || (Number(a.split(":")[0]) == Number(shift.split("-")[0]) &&  Number(a.split(":")[1]) < Number(shift.split("-")[1]))){
            x = Number(a.split(":")[0])+24;
            a1 = String(x) + ":" + a.split(":")[1];
        }
        if ((Number(b.split(":")[0]) < Number(shift.split("-")[0])) || (Number(b.split(":")[0]) == Number(shift.split("-")[0]) &&  Number(b.split(":")[1]) < Number(shift.split("-")[1]))){
            x = Number(b.split(":")[0])+24;
            b1 = String(x) + ":" + b.split(":")[1];
        }
        var hours = Number(b1.split(":")[0]) - Number(a1.split(":")[0]);
        var minutes = Number(b1.split(":")[1]) - Number(a1.split(":")[1]);
        if (minutes < 0){
            hours--;
            minutes = 30;
        }
        if (hours<10){
            var hoursStr = "0" + String(hours);
        }
        if (minutes==0){
            minutes = "0" + String(minutes);
        }
        return (hoursStr + ":" + String(minutes));
    }
    function changeTime(a, b){
        var hours = Number(a.split(":")[0]) + Number(b.split(":")[0]);
        var minutes = Number(a.split(":")[1]) + Number(b.split(":")[1]);
        if (minutes == 60) {
            var minutesStr = "00";
            hours++;
        }
        else if (minutes == 30){
            var minutesStr = "30";
        }
        else{
            var minutesStr = "00";
        }
        if (hours>=24){
            hours-=24;
        }
        if (hours<10){
            var hoursStr = "0" + String(hours);
        }
        else{
            var hoursStr = String(hours);
        }
        return (hoursStr+":"+minutesStr);
    }
});