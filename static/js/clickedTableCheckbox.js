
$(document).ready(function(){
document.oncontextmenu = function() {return false;};
$('.sessionsonoff_tab').click(function(){
		if (!$(this).hasClass("active")) {
            $('#cardIncome').val("");
            $('#cashIncome').val("");
            $( "#session_reset" ).css("display", "none");
            $(".payment_block-change").text(Number(0));
            $(".payment_block-change").css("background", "white")
			var i = $(this).index();
			$('.sessionsonoff_tab').removeClass('active');
            $('.sessionsonoffTab').hide().removeClass('active');
			$(this).addClass('active');
            $($("#sessionsonoffTabs").children(".sessionsonoffTab")[i-1]).fadeIn(1000).addClass("active");
	        var activeWidth = $(this).innerWidth();
	  		var itemPos = $(this).position();
			$(".sessionsonoffSelector").css({
			    "left":itemPos.left + "px",
			    "width": activeWidth + "px"});
		}
	})

   	var tabs = $('#sessionsonoffTabsMenu');
	var selector = $('#sessionsonoffTabsMenu').find('a').length;
	var activeItem = tabs.find('.active');
	var activeWidth = activeItem.innerWidth();
	$(".sessionsonoffSelector").css({
	  "left": activeItem.position().left + "px",
	  "width": activeWidth + "px"});
 var out = {text:""}
 var sidsdeposit = {text:""}
 var out1 = {text:""}
 var out2 = {text:""}

 var numberOut1 = {text:""}
$("#modalEditSession #id_depositSum").parent().css({"position": "relative", "width": "45%"});
$("#modalEditSession #id_adddepositSum").parent().css({"position": "relative", "top": "-60px"});
$("#modalEditSession #id_isCard").parent().parent().parent().css({"position": "relative", "top": "-35px"});
$("#modalEditSession #id_depositSumC").parent().css({"position": "relative", "width": "45%", "left": "55%", "top": "-86px"});

$( "#session_reset" ).css("display", "none");
$("#modalAddingSession #id_commentAddons").parent().css("width", "45%");
$("#modalAddingSession #id_commentAddons").css({"border": "2px solid red", "height": "150px"});
$("#modalAddingSession #id_commentAddonsChange").parent().css({"top": "-168px", "width": "45%", "left": "55%", "position":"relative"});
$("#modalAddingSession #id_commentAddonsChange").css({"border": "2px solid green", "height": "150px"});
$("#modalAddingSession #id_servicesAll").parent().css({"width": "45%", "position":"relative", "top":"-120px"});
$("#modalAddingSession #id_servicesAll").css({"border": "2px solid red", "height": "150px"});
$("#modalAddingSession #id_servicesAllChange").parent().css({"width": "45%", "position":"relative", "left": "55%", "top":"-285px"});
$("#modalAddingSession #id_servicesAllChange").css({"border": "2px solid green", "height": "150px"});
$("#modalAddingSession #id_commentText").parent().css({"position": "relative", "top": "-240px"});
$("#modalAddingSession #id_depositSum").parent().css({"position": "relative", "top": "-200px"});
$("#modalAddingSession #id_isCard").parent().parent().parent().css({"position": "relative", "top": "-170px"});
$("#buttonsSessionForms").parent().css({"position": "relative", "top": "-150px"});
$("#modalAddingSession #id_servicesAllChange").parent().siblings().css({"margin-top": "20px"});
$("#modalAddingSession #id_commentAddonsChange option").each(function(){
    $(this).prop('hidden',true);
})


$("#modalAddingSession #id_servicesAllChange option").each(function(){
    $(this).prop('hidden',true);
})
$.ajax({
    method : "GET" ,
    url : "/session/weekday-tar-dis/",
    data: {
        'idTariff': "d",
    },
    headers: {'X-CSRFToken': '{{ csrf_token }}'},
    dataType: 'json',
    success: function(data){
        $("#modalAddingSession #id_discount option").not(':first').remove();
        for (var i =0; i<data.con[0];i++){
            option = '<option value="'+data.qS[i][0]+'">'+data.qS[i][1]+'</option>';
            $("#modalAddingSession #id_discount").append(option);
        }
        $("#modalAddingSession #id_tariff option").not(':first').remove();
        for (var i =0; i<data.con[1];i++){
            option = '<option value="'+data.cB[i][0]+'">'+data.cB[i][1]+'</option>';
            $("#modalAddingSession #id_tariff").append(option);
        }
        $("#modalAddingSession #id_discount option:not(:selected)").each(function(){
            $(this).prop('hidden',true);
        })
    }
})


$("body").on("change", "#modalEditSession #id_tariff", function(e){
    e.preventDefault();
    $.ajax({
        method : "POST" ,
        url : "/session/tariff-discount/",
        data: {
            'idTariff': $(this).val(),
        },
        headers: {'X-CSRFToken': '{{ csrf_token }}'},
        dataType: 'json',
        success: function(data){
            var i = 0;
            $("#modalEditSession #id_discount option:first").prop("selected", true);
            $("#modalEditSession #id_discount option").not(':first').each(function(){
                if (data.str[i] == $(this).val()){
                    $(this).prop('hidden',false);
                    i++;
                }
                else{
                    $(this).prop('hidden',true);
                }
            })
        }
    })
})

$("body").on("change", "#modalAddingSession #id_discount", function(e){
    e.preventDefault();
    $.ajax({
        method : "POST" ,
        url : "/session/discount-tariff/",
        data: {
            'idDiscount': $(this).val(),
            'idTariff': $("#modalAddingSession #id_tariff").val(),
        },
        headers: {'X-CSRFToken': '{{ csrf_token }}'},
        dataType: 'json',
        success: function(data){
            if (Number(data.str[0]) > 0){
                $("#modalAddingSession #id_numberPersons").prop('min', data.str[0]);
                $("#modalAddingSession #id_numberPersons").prop('max', data.str[0]);
                $("#modalAddingSession #id_numberPersons").val(Number(data.str[0]));
            }
            else {
                $("#modalAddingSession #id_numberPersons").prop('min', data.str[1]);
                $("#modalAddingSession #id_numberPersons").val(Number(data.str[1]));
                $("#modalAddingSession #id_numberPersons").prop('max', 99);

            }

        }
    })
})
$("body").on("change", "#modalAddingSession #id_tariff", function(e){
    e.preventDefault();
    $.ajax({
        method : "POST" ,
        url : "/session/tariff-discount/",
        data: {
            'idTariff': $(this).val(),
        },
        headers: {'X-CSRFToken': '{{ csrf_token }}'},
        dataType: 'json',
        success: function(data){
            var i = 0;
            $("#modalAddingSession #id_discount option:first").prop("selected", true);
            $("#modalAddingSession #id_discount option").not(':first').each(function(){
                if (data.str[i][0] == $(this).val()){
                    $(this).prop('hidden',false);
                    $("#modalAddingSession #id_numberPersons").prop('min', data.str[i][1]);
                    i++;
                }
                else{
                    $(this).prop('hidden',true);
                }
            })
        }
    })
})
$("body").on("change", "#modalAddingSession #id_commentAddons", function(e){
    $("#modalAddingSession #id_commentAddons option:selected").each(function(){
        var ur = $(this)[0];
        $("#modalAddingSession #id_commentAddons option[value='"+ur.value+"']").prop('hidden',true);
        $("#modalAddingSession #id_commentAddons [value='"+ ur.value +"']").prop("selected", false);

        $("#modalAddingSession #id_commentAddonsChange [value='"+ ur.value +"']").prop("hidden", false);
        $("#modalAddingSession #id_commentAddonsChange [value='"+ ur.value +"']").prop("selected", true);
    })

})
$('body').on('click', '#addingSessionABtn', function(e){
    console.log($('#modalAddingSession #id_tariff').val());
    if ($('#modalAddingSession #id_tariff').val() == '') {
        $('#modalAddingSession #id_tariff').val(3)
    }
    $.ajax({
        method: "POST",
        url: '/session/setClientPageStatus/',
        data: {
                'status': 1,
        },
        dataType: 'json',
        success: function(data){}
    })
})
$('body').on('hidden.bs.modal submit','#modalAddingSession', function (){
    $.ajax({
        method: "POST",
        url: '/session/setClientPageStatus/',
        data: {
                'status': 0,
        },
        dataType: 'json',
        success: function(data){}
    })
});
$('body').on('submit','#modalAddingSession', function (){
    $.ajax({
        method: "POST",
        url: '/session/clearClientPageInfo/',
        data: {
                
        },
        dataType: 'json',
        success: function(data){}
    })
});
$('body').on('change','#content-modalAddingSession #id_room', function(e){
    text = $('#id_room :selected').text();
    niceAjax('room', text, '/session/changeClientPage/');
})
$('body').on('change','#id_tariff', function(e){
    text = $('#id_tariff :selected').text();
    niceAjax('tariff', text, '/session/changeClientPage/');
})
$('body').on('change','#id_discount', function(e){
    text = $('#id_discount :selected').text();
    niceAjax('discount', text, '/session/changeClientPage/');
})
$('body').on('keyup','#id_depositSum', function(e){
    text = $('#id_depositSum').val();
    niceAjax('deposit', text, '/session/changeClientPage/');
})
$('body').on('keyup','#id_numberPersons', function(e){
    text = $('#id_numberPersons').val();
    niceAjax('quantity', text, '/session/changeClientPage/');
})
$("body").on("click", "#modalAddingSession #id_commentAddonsChange option", function(e){
        var ur = $(this)[0];
        $("#modalAddingSession #id_commentAddonsChange option[value='"+ur.value+"']").prop('hidden',true);
        $("#modalAddingSession #id_commentAddonsChange option[value='"+ur.value+"']").prop('selected',false);
        $("#modalAddingSession #id_commentAddons option[value='"+ur.value+"']").prop('hidden',false);
        $("#modalAddingSession #id_commentAddonsChange option:not(:hidden)").each(function(){
            var ur = $(this)[0];
            $("#modalAddingSession #id_commentAddonsChange [value='"+ ur.value +"']").prop("selected", true);
        })
})
$("body").on("change", "#modalAddingSession #id_servicesAll", function(e){
    var text = '';
    $("#modalAddingSession #id_servicesAll option:selected").each(function(){
        var ur = $(this)[0];
        
        $("#modalAddingSession #id_servicesAll option[value='"+ur.value+"']").prop('hidden',true);
        $("#modalAddingSession #id_servicesAll [value='"+ ur.value +"']").prop("selected", false);

        $("#modalAddingSession #id_servicesAllChange [value='"+ ur.value +"']").prop("hidden", false);
        $("#modalAddingSession #id_servicesAllChange [value='"+ ur.value +"']").prop("selected", true);
    })
    $('#modalAddingSession #id_servicesAllChange option:selected').each(function(){
        text = text + ($(this).text()) + '; ';
    })
    niceAjax('services', text, '/session/changeClientPage/');
    
})
$("body").on("click", "#modalAddingSession #id_servicesAllChange option", function(e){
        var ur = $(this)[0];
        $("#modalAddingSession #id_servicesAllChange option[value='"+ur.value+"']").prop('hidden',true);
        $("#modalAddingSession #id_servicesAllChange option[value='"+ur.value+"']").prop('selected',false);
        $("#modalAddingSession #id_servicesAll option[value='"+ur.value+"']").prop('hidden',false);
        $("#modalAddingSession #id_servicesAllChange option:not(:hidden)").each(function(){
            var ur = $(this)[0];
            $("#modalAddingSession #id_servicesAllChange [value='"+ ur.value +"']").prop("selected", true);
        })
        var text = '';
        $('#modalAddingSession #id_servicesAllChange option:selected').each(function(){
        text = text + ($(this).text()) + '; ';
    })
    niceAjax('services', text, '/session/changeClientPage/');

})
var pK;
$("body").on("dblclick", ".table-registry-tr", function(e){
    $(this).addClass("simpleTr");
    $(this).removeClass("goldTr");
    sum = Number($(".payment_block-sum").text());
    $(".payment_block-sum").text(sum - Number($(this).find(".SUM p").text()));

    pK = $(this).attr("id");
    e.preventDefault();
    $.ajax({
        method : "POST" ,
        url : "/session/info-session/",
        data: {
            'thisSID': $(this).attr("id"),
        },
        headers: {'X-CSRFToken': '{{ csrf_token }}'},
        dataType: 'json',
        success: function(data){
            $('#modalEditSession form').prop("action", "/session/"+pK+"/edit/");
            $('#modalEditSession #id_room').val(data.str[0][0]);
//            if (data.str[2][0] == true){
//                $('#modalEditSession #id_isCard').prop("checked", true);
//            }
//            else{
//                $('#modalEditSession #id_isCard').prop("checked", false);
//            }
            $('#modalEditSession #id_depositSum').val(data.str[2][1]);
            $('#modalEditSession #id_depositSumC').val(data.str[2][0]);
//            if ($('#modalEditSession #id_depositSum').val() == 0){
//                $("#modalEditSession #id_isCard").parent().parent().parent().prop('hidden',false);
//            }
//            else{
//                $("#modalEditSession #id_isCard").parent().parent().parent().prop('hidden',true);
//            }
            $('#modalEditSession #id_tariff').val(data.str[0][1]);
            $('#modalEditSession #id_discount').val(data.str[0][3]);
            $('#modalEditSession #id_commentText').val(data.str[1][0]);

            $("#modalEditSession").modal('show');
        }
    })
})


$("body").on("click", ".table-registry-tr", function(e){
    if ($(this).hasClass("simpleTr")){
        $(this).removeClass("simpleTr");
        $(this).addClass("goldTr");
        sum = Number($(".payment_block-sum").text());
        $(".payment_block-sum").text(sum + Number($(this).find(".SUM p").text()));
    }
})


$("body").on("mousedown", ".table-registry-tr", function(e){
       if ($(this).hasClass("goldTr")){
            if( e.button == 2 ) {
                $(this).addClass("simpleTr");
                $(this).removeClass("goldTr");
                    sum = Number($(".payment_block-sum").text());
                    $(".payment_block-sum").text(sum - Number($(this).find(".SUM p").text()));

            }
       }
        return true;
    });




$("body").on("click", "#session_stop", function(e){
    out.text = "";
    servs = '';
    sum = 0;
    tf = $(".goldTr").eq(0).find('.registryTariffP').text();
    rm = $(".goldTr").eq(0).find('.registryRoomP').text();
    $(".goldTr").each(function(){
        out.text += $(this).attr("id") + ",";
        if ($(this).find('.registryAddServsP').text() != '') {
            servs += $(this).find('.registryAddServsP').text() + ' ';
        }
    })
     e.preventDefault();
     if (out.text != ""){
        $.ajax({
        method : "POST" ,
        url : "/session/stop-session/",
        data: {
            'out' : out.text,
            'status': 2,
            'servs' : servs,
            'tariff' : tf,
            'room' : rm,
        },
        dataType: 'json',
        success: function(data){
            var i = 0;
            $(".goldTr").each(function(){
                $(this).find(".SUM p").text(data.con[i][2]);
                i++;
            })
            $(".payment_block-sum").text(data.qS[0]);
            niceAjax('aSum', data.qS[0], '/session/changeClientPage/');
        }
     })
     }
});

$("body").on("click", "#session_print", function(e){
    out.text = "";
    $(".goldTr").each(function(){
        out.text += $(this).attr("id") + ",";
    })
     e.preventDefault();
     if (out.text != ""){
        $.ajax({
        method : "POST" ,
        url : "/session/print-session/",
        data: {
            'out' : out.text,
        },
        dataType: 'json',
        success: function(data){

        }
     })
     }
});

$("body").on("click", "#session_pay", function(e){
    out1.text = "";
    $(".goldTr").each(function(){
        out1.text += $(this).attr("id") + ",";
    })
     e.preventDefault();
     $.ajax({
        method : "POST" ,
        url : "/session/session-pay/",
        data: {
            'out1' : out1.text,
            'cardPaymentSum' : $('#cardIncome').val(),
            'cashPaymentSum' : $('#cashIncome').val(),
            'allPaymentSum' : Number($(".payment_block-sum").text()),
        },

        dataType: 'json',
        success: function(data){
            $(".payment_block-sum").text(Number(0));

            $('#cashIncome').val("");
            $('#cardIncome').val("");
            $(".goldTr").each(function(){
                $(this).addClass("simpleTr");
                $(this).removeClass("goldTr");
            })
            if (data.qS == "equal"){
                $("#cash-population-1").text("В кассе: "+ data.con[0][1] +" р.")
                $("#cash-population-2").text("Посетителей: "+ data.con[0][2])
                $(document).find(".payment_block-change").text(data.con[0][0]);
                $(document).find(".payment_block-change").css("background", "white")
                $( "#session_reset" ).css("display", "block");
            }
            else if (data.qS == "dolg"){
                $("#cash-population-1").text("В кассе: "+ data.con[0][1] +" р.")
                $("#cash-population-2").text("Посетителей: "+ data.con[0][2])
                $(document).find(".payment_block-change").text(data.con[0][0]);
                $(document).find(".payment_block-change").css("background", "red")
                $( "#session_reset" ).css("display", "block");
            }
            else {
                $(document).find(".payment_block-change").text(data.qS);
                $(document).find(".payment_block-change").css("background", "black")

            }


        }
     })
});

$("body").on("click", "#session_delete", function(e){
    out2.text = "";
    $(".goldTr").each(function(){
        out2.text += $(this).attr("id") + ",";
    })
     e.preventDefault();
     $.ajax({
        method : "POST" ,
        url : "/session/session-error-delete/",
        data: {
            'out2' : out2.text,
        },

        dataType: 'json',
        success: function(data){
        console.log(data);
            $(".payment_block-sum").text(Number(0));

            $('#cashIncome').val("");
            $('#cardIncome').val("");
            $(".goldTr").each(function(){
                $(this).addClass("simpleTr");
                $(this).removeClass("goldTr");
            })
            $("#wd_id tr").remove();
            for (var i=0;i<data.qS;i+=1){
                var table_row = '<tr class="table-registry-tr simpleTr" id="' + data.con[i][0] + '">';
                table_row+='<td class="SID"><p>'+data.con[i][0]+'</p></td>';
                table_row+='<td><p>';
                table_row+=data.con[i][3];
                table_row+='</p></td><td><p>'+data.con[i][1]+'</p></td>';
                table_row+='<td><p>'+ data.con[i][2] +'</p></td>';
                table_row+='<td><p>'+ data.con[i][9] +'</p></td>';
                table_row+='<td><p title="'+data.con[i][5]+' руб.">'+ data.con[i][5] + '</p></td>';
                table_row+='<td><p>'+ data.con[i][6] +'</p></td>';
                table_row+='<td class="COM"><p>'+data.con[i][7]+'</p></td><td class="SUM"><p>'+data.con[i][8]+'</p></td>';
                table_row+='</tr>'
                $('#wd_id').append(table_row);
            }


        }
     })
});


$("body").on("click", "#session_reset", function(e){
    e.preventDefault();
     $.ajax({
        method : "POST" ,
        url : "/session/session-reset/",
        data: {
            'status' : 0,
              },

        dataType: 'json',
        success: function(data){
            $('#cardIncome').val("");
            $('#cashIncome').val("");
            $( "#session_reset" ).css("display", "none");
            $(".payment_block-change").text(Number(0));
            $(".payment_block-change").css("background", "white")

            $("#wd_id tr").remove();
            for (var i=0;i<data.qS;i+=1){
                var table_row = '<tr class="table-registry-tr simpleTr" id="' + data.con[i][0] + '">';
                table_row+='<td class="SID"><p>'+data.con[i][0]+'</p></td>';
                table_row+='<td><p>';
                table_row+=data.con[i][3];
                table_row+='</p></td><td><p>'+data.con[i][1]+'</p></td>';
                table_row+='<td><p>'+ data.con[i][2] +'</p></td>';
                table_row+='<td><p>'+ data.con[i][9] +'</p></td>';
                table_row+='<td><p title="'+data.con[i][5]+' руб.">'+ data.con[i][5] + '</p></td>';
                table_row+='<td><p>'+ data.con[i][6] +'</p></td>';
                table_row+='<td class="COM"><p>'+data.con[i][7]+'</p></td><td class="SUM"><p>'+data.con[i][8]+'</p></td>';
                table_row+='</tr>'
                $('#wd_id').append(table_row);
            }
            $("#wd_idOff tr").remove();
            for (var i=0;i<data.qS1;i+=1){
                var table_row = '<tr class="table-registry-tr" id="' + data.con1[i][0] + '">';
                table_row+='<td class="SID"><p>'+data.con1[i][0]+'</p></td>';
                table_row+='<td><p>';
                table_row+=data.con1[i][3];
                table_row+='</p></td><td><p>'+data.con1[i][1]+'</p></td>';
                table_row+='<td><p>'+ data.con1[i][2] +'</p></td>';
                table_row+='<td><p>'+ data.con1[i][9] +'</p></td>';
                table_row+='<td><p>'+ data.con1[i][10] +'</p></td>';
                table_row+='<td><p title="'+data.con1[i][5]+' руб.">'+ data.con1[i][5] + '</p></td>';
                table_row+='<td><p>'+ data.con1[i][6] +'</p></td>';
                table_row+='<td class="COM"><p>'+data.con1[i][7]+'</p></td><td><p>'+data.con1[i][11]+' руб.</p></td><td><p>'+data.con1[i][12]+' руб.</p></td><td class="SUM"><p>'+data.con1[i][8]+' руб.</p></td>';
                table_row+='</tr>'
                $('#wd_idOff').append(table_row);
            }
        }
     })
});


   function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
};
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});



});

function niceAjax(field, text, url){
    $.ajax({
        method: "POST",
        url: url,
        data: {
                'field': field,
                'text' : text,
        },
        dataType: 'json',
        success: function(data){}
    })
}

$('#chooseAllinSes').click(function(){
    var checkBox = document.getElementById("chooseAllinSes");
    if (checkBox.checked == true) {
        $("body").find( ".table-registry-tr").each(function(){
            if ($(this).hasClass("simpleTr")){
                $(this).removeClass("simpleTr");
                $(this).addClass("goldTr");
                sum = Number($(".payment_block-sum").text());
                $(".payment_block-sum").text(sum + Number($(this).find(".SUM p").text()));
            }

        })
    }
    else{
        $("body").find( ".table-registry-tr").each(function(){
            if ($(this).hasClass("goldTr")){
                $(this).removeClass("goldTr");
                $(this).addClass("simpleTr");
                sum = Number($(".payment_block-sum").text());
                $(".payment_block-sum").text(sum - Number($(this).find(".SUM p").text()));
            }
        })
    }
})