function reviewBooking(sid){
    $.ajax({
        method : "POST" ,
        url : "/booking/info-booking/",
        data: {
            'thisSID': sid,
        },
        headers: {'X-CSRFToken': '{{ csrf_token }}'},
        dataType: 'json',
        success: function(data){
        console.log(data)
            $('#modalAddingBooking #id_start_booking').val(data.con[0][0]);
            $('#modalAddingBooking #id_time_start').val(data.con[0][1]);
            $('#modalAddingBooking #id_time_end').val(data.con[0][2]);
            $('#modalAddingBooking #id_number_of_persons').val(data.con[0][3]);
            $('#modalAddingBooking #id_room').val(data.con[0][4]);
            $('#modalAddingBooking #id_deposit_card').val(data.con[0][6]);
            $('#modalAddingBooking #id_deposit').val(data.con[0][5]);
            $('#modalAddingBooking #id_tariff').val(data.con[0][7]);
            $('#modalAddingBooking #id_duration').val(data.con[0][8]);
            $('#modalAddingBooking #id_idBooking').val(data.con[0][9]);
            sessionStorage.setItem('shiftBook', data.con[0][10]);
            $('#modalAddingBooking #id_p_num_0').val(data.con[1][0] + data.con[1][1]);
            $('#modalAddingBooking #id_name').val(data.con[2][0]);
            $('#modalAddingBooking #id_commentText').val(data.con[3][0]);
            for (var i =0; i<data.dop[0][0];i++){
                $("#modalAddingBooking #id_commentAddons1 option[value='"+data.com[i][0]+"']").prop('selected',true);
                $("#modalAddingBooking #id_commentAddons1 [value='"+ data.com[i][0] +"']").prop("hidden", false);
                $("#modalAddingBooking #id_commentAddons option[value='"+data.com[i][0]+"']").prop('hidden',true);
            }

            for (var i =0; i<data.dop[1][0];i++){
                $("#modalAddingBooking #id_servicesChange option[value='"+data.ser[i][0]+"']").prop('selected',true);
                $("#modalAddingBooking #id_servicesChange [value='"+ data.ser[i][0] +"']").prop("hidden", false);
                $("#modalAddingBooking #id_services option[value='"+data.ser[i][0]+"']").prop('hidden',true);
            }

            $('#bookingactBtn').prop('hidden',false);
            $('#bookingdelBtn').prop('hidden',false);
            $("#exampleModalLongTitle").text("Редактирование брони");
            $("#modalAddingBooking").modal('show');
        }
    })
}
$(document).ready(function(){
var shift = sessionStorage["shiftBook"];
$("#modalAddingBooking #id_commentAddons").parent().css({"width": "45%", "position": "relative", "top": "-90px"});
$("#modalAddingBooking #id_commentAddons").css({"border": "2px solid red", "height": "150px"});
$("#modalAddingBooking #id_commentAddons1").parent().css({"top": "-258px", "width": "45%", "left": "55%", "position":"relative"});
$("#modalAddingBooking #id_commentAddons1").css({"border": "2px solid green", "height": "150px"});
$("#modalAddingBooking #id_services").parent().css({"width": "45%", "position":"relative", "top":"30px"});
$("#modalAddingBooking #id_services").css({"border": "2px solid red", "height": "150px"});
$("#modalAddingBooking #id_servicesChange").parent().css({"width": "45%", "position":"relative", "left": "55%", "top":"-136px"});
$("#modalAddingBooking #id_servicesChange").css({"border": "2px solid green", "height": "150px"});
$("#modalAddingBooking #id_p_num_1").css({"position":"relative", "left": "15%", "width": "95px", "top": "-38px"});
$("#modalAddingBooking #id_deposit").parent().css({"position":"relative", "top": "-220px"});
$("#modalAddingBooking #id_deposit_card").parent().css({"position":"relative", "top": "-200px"});
$("#modalAddingBooking #id_commentText").parent().css({"position":"relative", "top": "-190px"});
$("#modalAddingBooking #buttonsMenuForm").parent().css({"position":"relative", "top": "-150px"});

$('#modalAddingBooking #id_idBooking').prop('hidden',true);
$('#bookingactBtn').prop('hidden',true);
$('#bookingdelBtn').prop('hidden',true);

$('#modalAddingBooking #id_idBooking').parent().prop('hidden',true);
$("#modalAddingBooking #id_commentAddons1 option").each(function(){
    $(this).prop('hidden',true);
})
$("#modalAddingBooking #id_servicesChange option").each(function(){
    $(this).prop('hidden',true);
})
$("body").on("change", "#modalAddingBooking #id_commentAddons", function(e){
    $("#modalAddingBooking #id_commentAddons option:selected").each(function(){
        var ur = $(this)[0];
        $("#modalAddingBooking #id_commentAddons option[value='"+ur.value+"']").prop('hidden',true);
        $("#modalAddingBooking #id_commentAddons [value='"+ ur.value +"']").prop("selected", false);

        $("#modalAddingBooking #id_commentAddons1 [value='"+ ur.value +"']").prop("hidden", false);
        $("#modalAddingBooking #id_commentAddons1 [value='"+ ur.value +"']").prop("selected", true);
    })
})
$("body").on("click", "#modalAddingBooking #id_commentAddons1 option", function(e){
        var ur = $(this)[0];
        $("#modalAddingBooking #id_commentAddons1 option[value='"+ur.value+"']").prop('hidden',true);
        $("#modalAddingBooking #id_commentAddons1 option[value='"+ur.value+"']").prop('selected',false);
        $("#modalAddingBooking #id_commentAddons option[value='"+ur.value+"']").prop('hidden',false);
        $("#modalAddingBooking #id_commentAddons1 option:not(:hidden)").each(function(){
            var ur = $(this)[0];
            $("#modalAddingBooking #id_commentAddons1 [value='"+ ur.value +"']").prop("selected", true);
        })
})
$("body").on("change", "#modalAddingBooking #id_services", function(e){
    $("#modalAddingBooking #id_services option:selected").each(function(){
        var ur = $(this)[0];
        $("#modalAddingBooking #id_services option[value='"+ur.value+"']").prop('hidden',true);
        $("#modalAddingBooking #id_services [value='"+ ur.value +"']").prop("selected", false);

        $("#modalAddingBooking #id_servicesChange [value='"+ ur.value +"']").prop("hidden", false);
        $("#modalAddingBooking #id_servicesChange [value='"+ ur.value +"']").prop("selected", true);
    })
})
$("body").on("click", "#modalAddingBooking #id_servicesChange option", function(e){
        var ur = $(this)[0];
        $("#modalAddingBooking #id_servicesChange option[value='"+ur.value+"']").prop('hidden',true);
        $("#modalAddingBooking #id_servicesChange option[value='"+ur.value+"']").prop('selected',false);
        $("#modalAddingBooking #id_services option[value='"+ur.value+"']").prop('hidden',false);
        $("#modalAddingBooking #id_servicesChange option:not(:hidden)").each(function(){
            var ur = $(this)[0];
            $("#modalAddingBooking #id_servicesChange [value='"+ ur.value +"']").prop("selected", true);
        })
})
$("body").on("dblclick", ".table-booking-content tr", function(e){
    e.preventDefault();
    reviewBooking($(this).attr("id"));
})

$("body").on("click", "#bookingactBtn", function(e){
    e.preventDefault();
    $.ajax({
        method : "POST" ,
        url : "/booking/session-booking/",
        data: {
            'activatedSID': $("#modalAddingBooking #id_idBooking").val(),
        },
        headers: {'X-CSRFToken': '{{ csrf_token }}'},
        dataType: 'json',
        success: function(data){
            console.log(data);
            $(location).attr('href',"/session/");

        }
    })
});
$("body").on("click", "#bookingdelBtn", function(e){

    e.preventDefault();
    $.ajax({
        method : "POST" ,
        url : "/booking/delete-booking/",
        data: {
            'deletedSID': $("#modalAddingBooking #id_idBooking").val(),
        },
        headers: {'X-CSRFToken': '{{ csrf_token }}'},
        dataType: 'json',
        success: function(data){
            location.reload();
            alert("УСПЕШНО УДАЛЕНО")
        }
    })
});
//
//$("body").on("click", ".sesBookingBtn", function(e){
//    console.log("hujsjdaj")
//    e.preventDefault();
//    $.ajax({
//        method : "POST" ,
//        url : "/booking/session-booking/",
//        data: {
//            'deletedSID': $(this).parent().attr("id").split('-')[1],
//        },
//        headers: {'X-CSRFToken': '{{ csrf_token }}'},
//        dataType: 'json',
//        success: function(data){
//            $(".table-booking-content tr").remove();
//            for (var i=0;i<data.qS[0];i+=1){
//                var table_row = '<tr><td><p>';
//                table_row += data.con[i][0] + '</p></td><td class="table-booking-content-td"><p>';
//                table_row += data.con[i][1] + '</p></td><td><p>';
//                table_row += data.con[i][6] + '</p></td><td><p>';
//                table_row += data.con[i][2] + '</p></td><td><p>';
//                table_row += data.con[i][3] + '</p></td><td id="dep-'+ data.con[i][4] +'"><p>'+ data.con[i][5] +' руб.</p></td><td class="eventsBooking"><div class="payBookingImg ml-auto" id="p-';
//                table_row += data.con[i][4] + '" title="Внести предоплату"><img src="../../../static/img/payBOOKING.png"></div>';
//                table_row += '<div class="sesBookingImg" id="s-'+ data.con[i][4] + '" title="Активировать бронь"><button class="sesBookingBtn" type="submit"><img src="../../../static/img/sesBOOKING.png"></button></div>';
//                table_row += '<div class="delBookingImg mr-auto" id="d-'+ data.con[i][4] + '" title="Снять бронь">';
//                table_row += '<button class="delBookingBtn" type="submit"><img src="../../../static/img/delBOOKING.png"></button>';
//                table_row += '</div></td></tr>';
//                $('.table-booking-content').append(table_row);
//            }
//
//        }
//    })
//});
//
//$("body").on("click", ".BookingAddBtn", function(e){
//            console.log("add");
//            e.preventDefault();
//
//            if (moment($('#endT').val()).unix() < moment($('#startT').val()).unix()){
//                alert("неправильная дата")
//            }
//            else{
//             $.ajax({
//
//                method : "POST" ,
//                url : "/booking/add-booking/",
//                data: {
//
//                    'startT' : $('#startT').val(),
//                    'endT' : $('#endT').val(),
//                    'quantity' : $('#quantity').val(),
//                    'choiceRoom' : $('#choiсe_methodkek').val(),
//                    'choiceTariff' : $('#choiсe_methodlol').val(),
//                },
//                dataType: 'json',
//                success: function(data){
//                    if (data.str[0] == "#"){
//                        alert(data.str)
//                    }
//                    else{
//                    booking = data.str.split(",");
//                    console.log(booking);
//                        var table_row = '<tr><td><p>';
//                        table_row += booking[0] + '</p></td><td class="table-booking-content-td"><p>';
//                        table_row += booking[1] + '</p></td><td><p>';
//                        table_row += booking[5] + '</p></td><td><p>';
//                        table_row += booking[2] + '</p></td><td><p>';
//                        table_row += booking[3] + '</p></td><td id="dep-'+ booking[4] +'"><p>0 руб.</p></td><td class="eventsBooking"><div class="payBookingImg ml-auto" id="p-';
//                        table_row += booking[4] + '" title="Внести предоплату"><img src="../../../static/img/payBOOKING.png"></div>';
//                        table_row += '<div class="sesBookingImg" id="s-'+ booking[4] + '" title="Активировать бронь"><button class="sesBookingBtn" type="submit"><img src="../../../static/img/sesBOOKING.png"></button></div>';
//                        table_row += '<div class="delBookingImg mr-auto" id="d-'+ booking[4] + '" title="Снять бронь">';
//                        table_row += '<button class="delBookingBtn" type="submit"><img src="../../../static/img/delBOOKING.png"></button>';
//                        table_row += '</div></td></tr>';
//                        $('.table-booking-content').append(table_row);
//                    }
//
//                    }
//                    })
//                }
//});
//
//var SIDpaymentBooking = -1;
//    $("body").on("click", ".payBookingImg", function(){
//        SIDpaymentBooking=$(this)[0].id.split("-")[1];
//        console.log("-------");
//        console.log(SIDpaymentBooking);
//        console.log($(this)[0].id);
//       $( "#pay_booking_window" ).show(550);
//    });
//
//    $("body").on("click", ".table-booking-add", function(){
//
//        $( "#pay_booking_window" ).hide(550);
//    });
//
//$("body").on("click", "#btnBooking", function(e){
//    e.preventDefault();
//    cashBook = $('#cashBookingPay').val();
//    if (cashBook ==""){
//        cashBook = 0;
//    }
//    bookcheck = false;
//    if ($(".depositBooking")[0].checked){
//        bookcheck = true;
//    }
//   $.ajax({
//        method : "POST" ,
//        url : "/booking/pay-booking/",
//        data: {
//           'SIDpay' : SIDpaymentBooking,
//           'bookcheck' : bookcheck,
//            'cashPaymentSum' : cashBook,
//        },
//
//        dataType: 'json',
//        success: function(data){
//            $( "#pay_booking_window" ).hide(550);
//            console.log($("#dep-"+SIDpaymentBooking+" p"))
//            $("#dep-"+SIDpaymentBooking+" p").text(cashBook + " руб.");
//            alert("Предоплата успешно внесена!");
//
//        }
//    })
//
//});


 });