
function updateClient()
{
    $.ajax({
        method: "POST",
			url: "/session/updateClientInfo/",
			data: {

			},
			dataType: 'json',
        success: function(data){
            $(".clientRoom").text(data.r);
            $(".clientTariff").text(data.t);     
            $(".clientCount").text(data.q);
            $(".clientDiscont").text(data.d);
            $(".clientServices").text(data.s);
            $(".clientDeposit").text(data.p);
            $(".clientSum").text(data.m);

        }
    });
}
var status = 0;
function checkStatus(){
	$.ajax({
        method: "POST",
			url: "/session/checkClientPageStatus/",
			data: {

			},
			dataType: 'json',
        success: function(data){
            if (data.s != status) {
            	$(".clientPageItem.active").hide().removeClass("active"); 
            	$($('body').children(('.clientPageItem'))[Number(data.s)]).fadeIn(1000).addClass("active");
            	status = data.s;
            }
        }
    });
}
$(document).ready(function(){
    updateClient();
    setInterval('checkStatus()', 1000);
    setInterval('updateClient()',1000);
});