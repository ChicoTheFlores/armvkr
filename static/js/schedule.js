function matrixArray(rows,columns){
  var arr = new Array();
  for(var i=0; i<rows; i++){
    arr[i] = new Array();
    for(var j=0; j<columns; j++){
      arr[i][j] = null;//вместо i+j+1 пишем любой наполнитель. В простейшем случае - null
    }
  }
  return arr;
}

$(document).ready(function(){
  document.oncontextmenu = function() {return false;};
  $('.premiumTableContetntTr').each(function(){
       var type = ($(this).find('.premiumTableContetnt-typeTD').text());
        if (type == "Штраф") $(this).css({color: 'red'})
            else $(this).css({color: 'green'});
    });
    if(window.matchMedia('(max-width: 768px)').matches){
        var rows = matrixArray($("#currentFullHeader th").length-1, 3);
        $("#currentFullHeader th").remove();
        $headertr = '<th></th><th>Утро</th><th>Вечер</th><th>Вечер-2</th>';
        $("#currentFullHeader").append($headertr);
        var i = 0;
        $("#currentFull .morning-shift td").each(function(){
            rows[i][0] = $(this)[0];
            i++;
        })
        var i = 0;
        $("#currentFull .evening-sfift td").each(function(){
            rows[i][1] = $(this)[0];
            i++;
        })
        var i = 0;
        $("#currentFull .evening-2-shift td").each(function(){
            rows[i][2] = $(this)[0];
            i++;
        })
        $("#currentFull tbody tr").remove();
        days = ["Понедельник","Вторник","Среда","Четверг","Пятница","Суббота","Воскресенье"]
        for (var i = 0; i < 7; i++){
            $("#currentFull tbody").append('<tr class="'+i+'-shift"><th scope="row">'+days[i]+'</th></tr>');
            $("#currentFull tbody ."+i+"-shift").append(rows[i][0]);
            $("#currentFull tbody ."+i+"-shift").append(rows[i][1]);
            $("#currentFull tbody ."+i+"-shift").append(rows[i][2]);
        }
        if ($("#nextFull").length>0){
            var rows = matrixArray($("#nextFullHeader th").length-1, 3);
            $("#nextFullHeader th").remove();
            $headertr = '<th></th><th>Утро</th><th>Вечер</th><th>Вечер-2</th>';
            $("#nextFullHeader").append($headertr);
            var i = 0;
            $("#nextFull .morning-shift td").each(function(){
                rows[i][0] = $(this)[0];
                i++;
            })
            var i = 0;
            $("#nextFull .evening-sfift td").each(function(){
                rows[i][1] = $(this)[0];
                i++;
            })
            var i = 0;
            $("#nextFull .evening-2-shift td").each(function(){
                rows[i][2] = $(this)[0];
                i++;
            })
            $("#nextFull tbody tr").remove();
            days = ["Понедельник","Вторник","Среда","Четверг","Пятница","Суббота","Воскресенье"]
            for (var i = 0; i < 7; i++){
                $("#nextFull tbody").append('<tr class="'+i+'-shift"><th scope="row">'+days[i]+'</th></tr>');
                $("#nextFull tbody ."+i+"-shift").append(rows[i][0]);
                $("#nextFull tbody ."+i+"-shift").append(rows[i][1]);
                $("#nextFull tbody ."+i+"-shift").append(rows[i][2]);
            }
        }
        else{
            var rows = matrixArray($("#teamChangeHeader th").length-1, 2);
            $("#teamChangeHeader th").remove();
            $headertr = '<th></th><th>Утро</th><th>Вечер</th>';
            $("#teamChangeHeader").append($headertr);
            var i = 0;
            $("#teamChange .morning-shift td").each(function(){
                rows[i][0] = $(this)[0];
                i++;
            })
            var i = 0;
            $("#teamChange .evening-shift td").each(function(){
                rows[i][1] = $(this)[0];
                i++;
            })
            $("#teamChange tbody tr").remove();
            days = ["Понедельник","Вторник","Среда","Четверг","Пятница","Суббота","Воскресенье"]
            for (var i = 0; i < 7; i++){
                $("#teamChange tbody").append('<tr class="'+i+'-shift"><th scope="row">'+days[i]+'</th></tr>');
                $("#teamChange tbody ."+i+"-shift").append(rows[i][0]);
                $("#teamChange tbody ."+i+"-shift").append(rows[i][1]);
            }
        }
        $('#scheduleSave').click(function(){
            var dict = "";
            for (var i = 0; i <= 6;i++){
                var obj = $("."+i+"-shift .schedule-shift").first();
                if (obj.hasClass('nice-time')){
                    dict+= '+';
                }
                else if(obj.hasClass('bad-time')){
                    dict+= '-';
                }
                else{
                    dict+= '=';
                }
                dict+=";";
            }
            for (var i = 0; i <= 6;i++){
                var obj = $("."+i+"-shift .schedule-shift").last();
                if (obj.hasClass('nice-time')){
                    dict+= '+';
                }
                else if(obj.hasClass('bad-time')){
                    dict+= '-';
                }
                else{
                    dict+= '=';
                }
                dict+=";";
            }
            $.ajax({
                    method: "POST",
                    url: "/schedule/ChoicePref/",
                    data: {
                        'dict': dict,
                    },
                    dataType: 'json',
                    success: function(data){
                        alert("Ваши изменения успешно внесены");
                    }
                })
        })
    }
    else{
        $('#scheduleSave').click(function(){
            var dict = "";
            $('.schedule-shift').each(function(){
                time = $(this).parent().attr('class');
                list = $(this).attr('class').split(' ');
                day = list[1];
                if ($(this).hasClass('nice-time')){
                    dict+= '+';
                }
                else if($(this).hasClass('bad-time')){
                    dict+= '-';
                }
                else{
                    dict+= '=';
                }
                dict+=";";
            })
            $.ajax({
                    method: "POST",
                    url: "/schedule/ChoicePref/",
                    data: {
                        'dict': dict,
                    },
                    dataType: 'json',
                    success: function(data){
                        alert("Ваши изменения успешно внесены");
                    }
                })
        })
    }
    $('.schedule-shift').click(function () {
         $( this ).toggleClass( "nice-time" );
         if ($(this).hasClass('bad-time')) {
            $(this).removeClass('bad-time')
         }
    })
  $("body").on("dblclick", ".my-shift", function(e){
        if ($(this).hasClass('not-wanted')) {
            $(this).removeClass('not-wanted')
         }
         else{
            $( this ).toggleClass( "not-wanted" );
         }
  });
  $("body").on("dblclick", ".vacanted-shift", function(e){

        if ($(this).hasClass('vacanted-shift-change')) {
            $(this).removeClass('vacanted-shift-change')
            $( this ).toggleClass( "vacanted-shift" );
         }
         else{
             $(this).removeClass('vacanted-shift');
            $( this ).toggleClass( "vacanted-shift-change" );
         }
  });
  $("body").on("dblclick", ".vacanted-shift-change", function(e){

        if ($(this).hasClass('vacanted-shift-change')) {
            $(this).removeClass('vacanted-shift-change')
            $( this ).toggleClass( "vacanted-shift" );
         }
         else{
             $(this).removeClass('vacanted-shift');
            $( this ).toggleClass( "vacanted-shift-change" );
         }
  });
  $("body").on("mousedown", ".schedule-shift", function(e){
    if( e.button == 2 ) {
       $( this ).toggleClass( "bad-time" );
       if ($(this).hasClass('nice-time')) {
	 	$(this).removeClass('nice-time')
	 }
      return false;
    }
    return true;
  });
});

$('#scheduleFreeOk').click(function(){
	var dict = "";
	$('.vacanted-shift-change').each(function(){
        dict += $(this).attr("id") + ","
	})
	
	$.ajax({
			method: "POST",
			url: "/schedule/FreePrefOk/",
			data: {
				'dict': dict,
			},
			dataType: 'json',
			success: function(data){
                alert("Вы успешно забрали данные смены!");
                counter = 0;
                $('.vacanted-shift-change').each(function(){
                    if ($(this).attr("id") == data.com[counter]){
                        $(this).removeClass('vacanted-shift-change');
                        $( this ).toggleClass( "my-shift" );
                        $(this).text(data.dop);
                        $(this).css("background", data.con);
                        counter++;
                    }
                    else{
                        $(this).removeClass('vacanted-shift-change');
                        $(this).toggleClass("vacanted-shift");
                    }

                })
	        }
		})
})

$('#scheduleFree').click(function(){
	var dict = "";
	$('.not-wanted').each(function(){
        dict += $(this).attr("id") + ","
	})
	$.ajax({
			method: "POST",
			url: "/schedule/FreePref/",
			data: {
				'dict': dict,
			},
			dataType: 'json',
			success: function(data){
                alert("Выбранные вами смены стали возможными для выбора. Остальные пользователи будут уведомлены об этом.");
                $('.not-wanted').each(function(){
                    $(this).removeClass('not-wanted');
                    $(this).removeClass('my-shift');
                    $( this ).toggleClass( "vacanted-shift" );
                })
	        }
		})
})
