from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^logout/$', views.logoutView.as_view(), name="logout"),
    url(r'^$', views.booking, name='booking'),
    # url(r'^add-booking/$', views.add_booking),
    url(r'^delete-booking/$', views.delete_booking),
    # url(r'^pay-booking/$', views.pay_booking),
    url(r'^session-booking/$', views.activate_booking),
    url(r'^info-booking/$', views.info_booking),
]