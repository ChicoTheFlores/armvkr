from django.db import models
from django.conf import settings
from django.utils import timezone
from datetime import datetime, time
from django.contrib.auth.models import User
from AWPkamenka.settings import TIME_ZONE
import pytz
tzl = pytz.timezone(TIME_ZONE)


class Days(models.Model):
    day = models.CharField(max_length=11)

    def __str__(self):
        return self.day

    class Meta:
        verbose_name = "Дни недели"
        verbose_name_plural = "Дни недели"


class Discounts(models.Model):
    days = models.ManyToManyField(Days, blank=True, verbose_name="Дни недели")
    name = models.CharField(default="", max_length=50,
                            verbose_name="Название скидки", blank=False, unique=True)
    multiplier = models.DecimalField(
        max_digits=3, decimal_places=2, default=1.0, verbose_name="Множитель")
    people_limit = models.PositiveSmallIntegerField(
        default=2, verbose_name="Кол-во людей")
    people_free = models.PositiveSmallIntegerField(
        default=1, verbose_name="Кол-во людей бесплатно")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Скидку"
        verbose_name_plural = "Скидки"


class Service(models.Model):
    name_service = models.CharField(
        default="", max_length=30, verbose_name="Названия услуг", blank=False, unique=True)
    price_service = models.PositiveSmallIntegerField(
        default=0, verbose_name="Стоимость услуги")

    def __str__(self):
        return self.name_service

    class Meta:
        verbose_name = "Платную услугу"
        verbose_name_plural = "Платные услуги"


class AddOn(models.Model):
    name = models.CharField(default="", max_length=50,
                            verbose_name="Названия услуг", blank=False, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Включенную в стоимость услугу"
        verbose_name_plural = "Включенные в стоимость услуги"


class Tarif(models.Model):
    TypeTime = "tp"
    TypePlace = "fp"
    TYPEOfTariffs = (
        (TypeTime, "Повременная оплата"),
        (TypePlace, "Фиксированный прайс")
    )

    name_tariff = models.CharField(default="", max_length=35, verbose_name="Названия тарифа", blank=False,
                                   unique=True)
    type = models.CharField(max_length=25, verbose_name="Тип", blank=False,
                            choices=TYPEOfTariffs, default="Повременная оплата")
    price_tariff = models.PositiveSmallIntegerField(
        default=0, verbose_name="Цена минуты")
    days = models.ManyToManyField(Days, blank=True, verbose_name="Дни недели")
    price_fix1 = models.PositiveSmallIntegerField(
        default=0, verbose_name="Фиксированная цена")
    people_limit1 = models.PositiveSmallIntegerField(
        default=1, verbose_name="Минимальное кол-во людей")
    people_free1 = models.PositiveSmallIntegerField(
        default=0, verbose_name="Кол-во людей бесплатно")
    time_limit = models.TimeField(
        default=time(), verbose_name="Ограничение по времени", null=True, blank=True)
    discounts = models.ManyToManyField(
        Discounts, blank=True, verbose_name="Возможные скидки при данном тарифе")

    def __str__(self):
        return self.name_tariff

    class Meta:
        verbose_name = "Тариф"
        verbose_name_plural = "Тарифы"


class Room(models.Model):
    purple = "purple"
    pink = "pink"
    red = "red"
    orange = "orange"
    yellow = "yellow"
    green = "green"
    teal = "teal"
    cyan = "cyan"
    gray = "gray"
    black = "black"
    coral = "coral"
    khaki = "khaki"
    magenta = "magenta"
    brown = "brown"
    chocolate = "chocolate"
    olive = "olive"
    lime = "lime"
    white = "white"

    TYPEOfColorRoom = (
        (purple, "Пурпурный"),
        (pink, "Розовый"),
        (red, "Красный"),
        (orange, "Оранжевый"),
        (yellow, "Желтый"),
        (green, "Зеленый"),
        (teal, "Речная утка"),
        (cyan, "Морская волна"),
        (gray, "Серый"),
        (black, "Чёрный"),
        (coral, "Коралловый"),
        (khaki, "Хаки"),
        (magenta, "Фукси"),
        (chocolate, "Шоколадный"),
        (olive, "Оливковый"),
        (lime, "Лайм")
    )
    TYPEOfColorFont = (
        (red, "Красный"),
        (gray, "Серый"),
        (black, "Чёрный"),
        (white, "Белый")
    )
    name_room = models.CharField(
        default="", max_length=16, verbose_name="Названия комнат", blank=False, unique=True)
    color = models.CharField(
        max_length=20, verbose_name="Цвет", default="Чёрный", choices=TYPEOfColorRoom)
    capasity = models.PositiveSmallIntegerField(
        default=1, verbose_name="Вместимость")
    font = models.CharField(max_length=20, verbose_name="Цвет шрифта",
                            default="Чёрный", choices=TYPEOfColorFont)

    def __str__(self):
        return self.name_room

    class Meta:
        verbose_name = "Комната"
        verbose_name_plural = "Комнаты"


class CashPay(models.Model):
    amount_cash = models.IntegerField(
        default=0, blank=True, verbose_name="Оплата наличными")
    payment_dt = models.DateTimeField(
        default=datetime.now, blank=True, verbose_name="Время оплаты")
    is_payed = models.BooleanField(
        default=False, verbose_name="Учтено в ордере Выручки")

    def __str__(self):
        try:
            return "SID клиента:  " + str(Session.objects.get(cash=self)) + " = " + str(self.amount_cash) + " р."
        except:
            return str(self.payment_dt)

    class Meta:
        verbose_name = "Оплата наличными"
        verbose_name_plural = "Оплаты наличными"


class CardPay(models.Model):
    amount_card = models.IntegerField(
        default=0, blank=True, verbose_name="Оплата картой")
    payment_dt = models.DateTimeField(
        default=datetime.now, blank=True, verbose_name="Время оплаты")
    is_payed = models.BooleanField(
        default=False, verbose_name="Учтено в ордере Выручки")

    def __str__(self):
        try:
            return "SID клиента:  " + str(Session.objects.get(card=self)) + " = " + str(self.amount_card) + " р."
        except:
            return str(self.payment_dt)

    class Meta:
        verbose_name = "Оплата картой"
        verbose_name_plural = "Оплаты картой"


class UseService(models.Model):
    add_serv = models.ForeignKey(
        Service, blank=True, null=True, verbose_name="Услуга", on_delete=models.SET_NULL)
    count_serv = models.IntegerField(verbose_name="Кол-во", default=1)

    def __str__(self):
        return "SID клиента:  " + str(Session.objects.filter(services=self).first()) + " - " + str(self.add_serv.name_service)

    class Meta:
        verbose_name = "Оказанная услуга"
        verbose_name_plural = "Оказанные услуги"


class UserComments(models.Model):
    text = models.CharField(default="", max_length=150,
                            verbose_name="Текстовый комментарий", blank=True)
    addons = models.ManyToManyField(
        AddOn, blank=True, verbose_name="Включенные в стоимость услуги")

    def __str__(self):
        if self.text == "":
            return str(self.id)
        else:
            return str(self.text)

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"


class PhoneSberbank(models.Model):
    phone = models.CharField(default="", max_length=10,
                             verbose_name="Телефон", blank=True)

    def __str__(self):
        return str(self.phone)

    class Meta:
        verbose_name = "Телефон для отправки переводов через Сбербанк"
        verbose_name_plural = "Телефон для отправки переводов через Сбербанк"


class Deposit(models.Model):
    # session = models.ForeignKey(Session, blank=True, null=True,on_delete=models.CASCADE, verbose_name="Посетитель")
    cash = models.IntegerField(
        default=0, blank=True, verbose_name="Сумма наличными")
    card = models.IntegerField(
        default=0, blank=True, verbose_name="Сумма картой")
    # is_card = models.BooleanField(default=False, verbose_name="Депозит картой", blank=False)

    class Meta:
        verbose_name = "Депозит"
        verbose_name_plural = "Депозиты"


class Session(models.Model):
    SID = models.IntegerField(verbose_name="SID")
    room = models.ForeignKey(Room, blank=True, null=True,
                             verbose_name="Занятая комната", on_delete=models.SET_NULL)
    start_dt = models.DateTimeField(
        verbose_name="Начало сеанса", default=timezone.now)
    end_dt = models.DateTimeField(
        null=True, blank=True, verbose_name="Окончание сеанса")
    company = models.IntegerField(verbose_name="Компания", blank=False)
    tariff = models.ForeignKey(
        Tarif, null=True, on_delete=models.SET_NULL, verbose_name="Тариф")
    sum_session = models.IntegerField(
        default=0, null=True, blank=True, verbose_name="Чек-лист")
    is_active = models.BooleanField(
        default=True, verbose_name="Пользователь активен", blank=False)
    cash = models.ForeignKey(CashPay, blank=True, verbose_name="Наличные",
                             on_delete=models.SET_DEFAULT, default=None, null=True)
    card = models.ForeignKey(CardPay, blank=True, verbose_name="Безнал",
                             on_delete=models.SET_DEFAULT, default=None, null=True)
    services = models.ManyToManyField(
        UseService, blank=True, verbose_name="Оказанные услуги")
    comment = models.ForeignKey(UserComments, blank=True, null=True,
                                on_delete=models.SET_NULL, verbose_name="Комментарий")
    discount = models.ForeignKey(
        Discounts, null=True, on_delete=models.SET_NULL, blank=True, verbose_name="Скидка")
    owner = models.BooleanField(
        verbose_name="Главный в компании", default=False)
    deposit = models.ForeignKey(
        Deposit, blank=True, null=True, on_delete=models.SET_NULL, verbose_name="Депозит")

    def __str__(self):
        return str(self.start_dt)[0:16] + "_" + str(self.company) + "_" + str(self.SID)

    def dict(self, s):
        obj = {
            'DataSes': s,
        }
        return obj

    class Meta:
        verbose_name = "Сессия"
        verbose_name_plural = "Сессии"


class OperatingCash(models.Model):
    cash_sum_now = models.IntegerField(
        default=0, blank=True, verbose_name="Наличные в кассе")

    def __str__(self):
        return "На данный момент в кассе:  " + str(self.cash_sum_now)

    class Meta:
        verbose_name = "Наличные в кассе"
        verbose_name_plural = "Наличные в кассе"


class Booking(models.Model):
    start_booking = models.DateField(
        blank=False, verbose_name="Дата брони", default=timezone.now)
    time_start = models.TimeField(
        verbose_name="Начало брони", default=datetime.now)
    time_end = models.TimeField(
        verbose_name="Окончание брони", default=datetime.now)
    number_of_persons = models.PositiveSmallIntegerField(
        default=0, verbose_name="Кол-во человек")
    room = models.ForeignKey(Room, blank=True, null=True,
                             on_delete=models.SET_NULL, verbose_name="Занятая комната")
    deposit = models.IntegerField(
        default=0, blank=True, verbose_name="Депозит наличными")
    deposit_card = models.IntegerField(
        default=0, blank=True, verbose_name="Депозит картой")
    # is_card = models.BooleanField(default=False, verbose_name="Депозит картой", blank=False)
    tariff = models.ForeignKey(
        Tarif, null=True, on_delete=models.SET_NULL, verbose_name="Тариф")
    comment = models.ForeignKey(UserComments, blank=True, null=True,
                                on_delete=models.SET_NULL, verbose_name="Комментарий")
    services = models.ManyToManyField(
        Service, blank=True, verbose_name="Платные услуги")
    name = models.CharField(default=None, max_length=50,
                            verbose_name="Имя клиента", blank=True)
    phone = models.CharField(default=None, max_length=15,
                             verbose_name="Телефон клиента", blank=True, null=True)

    class Meta:
        verbose_name = "Бронь"
        verbose_name_plural = "Брони"

# function for ajax


def onemes(str):
    dict = {
        'str': str,
    }
    return dict


def fulldict(qSession, con):
    dict = {
        'qS': qSession,
        'con': con,
    }
    return dict


def fullfulldict(qSession, con, qSession1, con1):
    dict = {
        'qS': qSession,
        'con': con,
        'qS1': qSession1,
        'con1': con1,
    }
    return dict


def fullHDdict(qSession, con, allRoom, allGroup, allSID):
    dict = {
        'qS': qSession,
        'con': con,
        'aR': allRoom,
        'aG': allGroup,
        'aS': allSID,
    }
    return dict


def FourDict(f, s, t, fo):
    dict = {
        'dop': f,
        'con': s,
        'com': t,
        'ser': fo,
    }
    return dict


def calendarDict(con0, con, book):
    dict = {
        'qS': con0,
        'con': con,
        'cB': book,
    }
    return dict


def DrawNewTable():
    allSession = Session.objects.filter(is_active=True)
    con = dict()
    i = 0
    for ses in allSession:
        ses.fullprice = 0
        ses.fullservs = ""
        for serv in ses.services.all():
            ses.fullservs += serv.add_serv.name_service + \
                "-" + str(serv.count_serv) + ","
            ses.fullprice += serv.count_serv * serv.add_serv.price_service
        ses.fullservs = ses.fullservs[:-1]
        con.update({i: [ses.SID, ses.company, ses.tariff.name_tariff,
                        ses.room.name_room, ses.fullservs, ses.fullprice]})
        i += 1
    return con


class clientPageInfo(models.Model):
    room = models.CharField(default="---------",
                            max_length=30, verbose_name="Комната", blank=True)
    tariff = models.CharField(
        default="---------", max_length=30, verbose_name="тариф", blank=True)
    discount = models.CharField(
        default="---------", max_length=30, verbose_name="скидка", blank=True)
    deposit = models.CharField(
        default="---------", max_length=30, verbose_name="депозит", blank=True)
    services = models.CharField(
        default="---------", max_length=300, verbose_name="услуги", blank=True)
    quantity = models.CharField(
        default="---------", max_length=10, verbose_name="Кол-во", blank=True)
    aSum = models.CharField(default="---------",
                            max_length=10, verbose_name="Сумма", blank=True)


class clientPageStatus(models.Model):
    status = models.PositiveSmallIntegerField(default=0, blank=False)
