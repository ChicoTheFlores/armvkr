# Generated by Django 2.2.2 on 2019-09-08 16:51

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Deposit',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cash', models.IntegerField(blank=True, default=0, verbose_name='Сумма наличными')),
                ('card', models.IntegerField(blank=True, default=0, verbose_name='Сумма картой')),
            ],
            options={
                'verbose_name_plural': 'Депозиты',
                'verbose_name': 'Депозит',
            },
        ),
        migrations.AddField(
            model_name='session',
            name='deposit',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='booking.Deposit', verbose_name='Депозит'),
        ),
    ]
