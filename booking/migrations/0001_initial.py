# Generated by Django 2.2.2 on 2019-09-08 16:51

import datetime
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AddOn',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=50, unique=True, verbose_name='Названия услуг')),
            ],
            options={
                'verbose_name': 'Включенную в стоимость услугу',
                'verbose_name_plural': 'Включенные в стоимость услуги',
            },
        ),
        migrations.CreateModel(
            name='CardPay',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount_card', models.IntegerField(blank=True, default=0, verbose_name='Оплата картой')),
                ('payment_dt', models.DateTimeField(blank=True, default=datetime.datetime.now, verbose_name='Время оплаты')),
                ('is_payed', models.BooleanField(default=False, verbose_name='Учтено в ордере Выручки')),
            ],
            options={
                'verbose_name': 'Оплата картой',
                'verbose_name_plural': 'Оплаты картой',
            },
        ),
        migrations.CreateModel(
            name='CashPay',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount_cash', models.IntegerField(blank=True, default=0, verbose_name='Оплата наличными')),
                ('payment_dt', models.DateTimeField(blank=True, default=datetime.datetime.now, verbose_name='Время оплаты')),
                ('is_payed', models.BooleanField(default=False, verbose_name='Учтено в ордере Выручки')),
            ],
            options={
                'verbose_name': 'Оплата наличными',
                'verbose_name_plural': 'Оплаты наличными',
            },
        ),
        migrations.CreateModel(
            name='clientPageInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('room', models.CharField(blank=True, default='---------', max_length=30, verbose_name='Комната')),
                ('tariff', models.CharField(blank=True, default='---------', max_length=30, verbose_name='тариф')),
                ('discount', models.CharField(blank=True, default='---------', max_length=30, verbose_name='скидка')),
                ('deposit', models.CharField(blank=True, default='---------', max_length=30, verbose_name='депозит')),
                ('services', models.CharField(blank=True, default='---------', max_length=300, verbose_name='услуги')),
                ('quantity', models.CharField(blank=True, default='---------', max_length=10, verbose_name='Кол-во')),
                ('aSum', models.CharField(blank=True, default='---------', max_length=10, verbose_name='Сумма')),
            ],
        ),
        migrations.CreateModel(
            name='clientPageStatus',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.PositiveSmallIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Days',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('day', models.CharField(max_length=11)),
            ],
            options={
                'verbose_name': 'Дни недели',
                'verbose_name_plural': 'Дни недели',
            },
        ),
        migrations.CreateModel(
            name='Discounts',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=50, unique=True, verbose_name='Название скидки')),
                ('multiplier', models.DecimalField(decimal_places=2, default=1.0, max_digits=3, verbose_name='Множитель')),
                ('people_limit', models.PositiveSmallIntegerField(default=2, verbose_name='Кол-во людей')),
                ('people_free', models.PositiveSmallIntegerField(default=1, verbose_name='Кол-во людей бесплатно')),
                ('days', models.ManyToManyField(blank=True, to='booking.Days', verbose_name='Дни недели')),
            ],
            options={
                'verbose_name': 'Скидку',
                'verbose_name_plural': 'Скидки',
            },
        ),
        migrations.CreateModel(
            name='OperatingCash',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cash_sum_now', models.IntegerField(blank=True, default=0, verbose_name='Наличные в кассе')),
            ],
            options={
                'verbose_name': 'Наличные в кассе',
                'verbose_name_plural': 'Наличные в кассе',
            },
        ),
        migrations.CreateModel(
            name='PhoneSberbank',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(blank=True, default='', max_length=10, verbose_name='Телефон')),
            ],
            options={
                'verbose_name': 'Телефон для отправки переводов через Сбербанк',
                'verbose_name_plural': 'Телефон для отправки переводов через Сбербанк',
            },
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_room', models.CharField(default='', max_length=16, unique=True, verbose_name='Названия комнат')),
            ],
            options={
                'verbose_name': 'Комната',
                'verbose_name_plural': 'Комнаты',
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_service', models.CharField(default='', max_length=30, unique=True, verbose_name='Названия услуг')),
                ('price_service', models.PositiveSmallIntegerField(default=0, verbose_name='Стоимость услуги')),
            ],
            options={
                'verbose_name': 'Платную услугу',
                'verbose_name_plural': 'Платные услуги',
            },
        ),
        migrations.CreateModel(
            name='UseService',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count_serv', models.IntegerField(default=1, verbose_name='Кол-во')),
                ('add_serv', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='booking.Service', verbose_name='Услуга')),
            ],
            options={
                'verbose_name': 'Оказанная услуга',
                'verbose_name_plural': 'Оказанные услуги',
            },
        ),
        migrations.CreateModel(
            name='UserComments',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.CharField(blank=True, default='', max_length=150, verbose_name='Текстовый комментарий')),
                ('addons', models.ManyToManyField(blank=True, to='booking.AddOn', verbose_name='Включенные в стоимость услуги')),
            ],
            options={
                'verbose_name': 'Комментарий',
                'verbose_name_plural': 'Комментарии',
            },
        ),
        migrations.CreateModel(
            name='Tarif',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name_tariff', models.CharField(default='', max_length=35, unique=True, verbose_name='Названия тарифа')),
                ('type', models.CharField(choices=[('tp', 'Повременная оплата'), ('fp', 'Фиксированный прайс')], default='Повременная оплата', max_length=25, verbose_name='Тип')),
                ('price_tariff', models.PositiveSmallIntegerField(default=0, verbose_name='Цена минуты')),
                ('price_fix1', models.PositiveSmallIntegerField(default=0, verbose_name='Фиксированная цена')),
                ('people_limit1', models.PositiveSmallIntegerField(default=1, verbose_name='Минимальное кол-во людей')),
                ('people_free1', models.PositiveSmallIntegerField(default=0, verbose_name='Кол-во людей бесплатно')),
                ('time_limit', models.TimeField(blank=True, default=datetime.time(0, 0), null=True, verbose_name='Ограничение по времени')),
                ('days', models.ManyToManyField(blank=True, to='booking.Days', verbose_name='Дни недели')),
                ('discounts', models.ManyToManyField(blank=True, to='booking.Discounts', verbose_name='Возможные скидки при данном тарифе')),
            ],
            options={
                'verbose_name': 'Тариф',
                'verbose_name_plural': 'Тарифы',
            },
        ),
        migrations.CreateModel(
            name='Session',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('SID', models.IntegerField(verbose_name='SID')),
                ('start_dt', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Начало сеанса')),
                ('end_dt', models.DateTimeField(blank=True, null=True, verbose_name='Окончание сеанса')),
                ('company', models.IntegerField(verbose_name='Компания')),
                ('sum_session', models.IntegerField(blank=True, default=0, null=True, verbose_name='Чек-лист')),
                ('is_active', models.BooleanField(default=True, verbose_name='Пользователь активен')),
                ('owner', models.BooleanField(default=False, verbose_name='Главный в компании')),
                ('card', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='booking.CardPay', verbose_name='Безнал')),
                ('cash', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.SET_DEFAULT, to='booking.CashPay', verbose_name='Наличные')),
                ('comment', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='booking.UserComments', verbose_name='Комментарий')),
                ('discount', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='booking.Discounts', verbose_name='Скидка')),
                ('room', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='booking.Room', verbose_name='Занятая комната')),
                ('services', models.ManyToManyField(blank=True, to='booking.UseService', verbose_name='Оказанные услуги')),
                ('tariff', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='booking.Tarif', verbose_name='Тариф')),
            ],
            options={
                'verbose_name': 'Сессия',
                'verbose_name_plural': 'Сессии',
            },
        ),
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_booking', models.DateField(default=django.utils.timezone.now, verbose_name='Дата брони')),
                ('time_start', models.TimeField(default=datetime.datetime.now, verbose_name='Начало брони')),
                ('time_end', models.TimeField(default=datetime.datetime.now, verbose_name='Окончание брони')),
                ('number_of_persons', models.PositiveSmallIntegerField(default=0, verbose_name='Кол-во человек')),
                ('deposit', models.IntegerField(blank=True, default=0, verbose_name='Депозит')),
                ('is_card', models.BooleanField(default=False, verbose_name='Депозит картой')),
                ('name', models.CharField(blank=True, default=None, max_length=50, verbose_name='Имя клиента')),
                ('phone', models.CharField(blank=True, default=None, max_length=15, null=True, verbose_name='Телефон клиента')),
                ('comment', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='booking.UserComments', verbose_name='Комментарий')),
                ('room', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='booking.Room', verbose_name='Занятая комната')),
                ('services', models.ManyToManyField(blank=True, to='booking.Service', verbose_name='Платные услуги')),
                ('tariff', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='booking.Tarif', verbose_name='Тариф')),
            ],
            options={
                'verbose_name': 'Бронь',
                'verbose_name_plural': 'Брони',
            },
        ),
    ]
