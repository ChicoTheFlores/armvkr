# Generated by Django 2.2.2 on 2019-09-15 09:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('booking', '0007_remove_booking_is_card'),
    ]

    operations = [
        migrations.AddField(
            model_name='room',
            name='capasity',
            field=models.PositiveSmallIntegerField(default=1, verbose_name='Вместимость'),
        ),
        migrations.AddField(
            model_name='room',
            name='font',
            field=models.CharField(choices=[('red', 'Красный'), ('gray', 'Серый'), ('black', 'Чёрный'), ('white', 'Белый')], default='Чёрный', max_length=20, verbose_name='Цвет'),
        ),
    ]
