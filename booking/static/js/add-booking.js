function reviewBooking(sid) {
    $.ajax({
        method: "POST",
        url: "/booking/info-booking/",
        data: {
            'thisSID': sid,
        },
        headers: { 'X-CSRFToken': '{{ csrf_token }}' },
        dataType: 'json',
        success: function (data) {
            $('#modalAddingBooking #id_start_booking').val(data.con[0][0]);
            $('#modalAddingBooking #id_time_start').val(data.con[0][1]);
            $('#modalAddingBooking #id_time_end').val(data.con[0][2]);
            $('#modalAddingBooking #id_number_of_persons').val(data.con[0][3]);
            $('#modalAddingBooking #id_room').val(data.con[0][4]);
            $('#modalAddingBooking #id_deposit_card').val(data.con[0][6]);
            $('#modalAddingBooking #id_deposit').val(data.con[0][5]);
            $('#modalAddingBooking #id_tariff').val(data.con[0][7]);
            $('#modalAddingBooking #id_duration').val(data.con[0][8]);
            $('#modalAddingBooking #id_idBooking').val(data.con[0][9]);
            sessionStorage.setItem('shiftBook', data.con[0][10]);
            $('#modalAddingBooking #id_p_num_0').val(data.con[1][0] + data.con[1][1]);
            $('#modalAddingBooking #id_name').val(data.con[2][0]);
            $('#modalAddingBooking #id_commentText').val(data.con[3][0]);
            for (var i = 0; i < data.dop[0][0]; i++) {
                $("#modalAddingBooking #id_commentAddons1 option[value='" + data.com[i][0] + "']").prop('selected', true);
                $("#modalAddingBooking #id_commentAddons1 [value='" + data.com[i][0] + "']").prop("hidden", false);
                $("#modalAddingBooking #id_commentAddons option[value='" + data.com[i][0] + "']").prop('hidden', true);
            }

            for (var i = 0; i < data.dop[1][0]; i++) {
                $("#modalAddingBooking #id_servicesChange option[value='" + data.ser[i][0] + "']").prop('selected', true);
                $("#modalAddingBooking #id_servicesChange [value='" + data.ser[i][0] + "']").prop("hidden", false);
                $("#modalAddingBooking #id_services option[value='" + data.ser[i][0] + "']").prop('hidden', true);
            }

            $('#bookingactBtn').prop('hidden', false);
            $('#bookingdelBtn').prop('hidden', false);
            $("#exampleModalLongTitle").text("Редактирование брони");
            $("#modalAddingBooking").modal('show');
        }
    })
}
$(document).ready(function () {
    var shift = sessionStorage["shiftBook"];
    $("#modalAddingBooking #id_commentAddons").parent().css({ "width": "45%", "position": "relative", "top": "-160px", "display": "block" });
    $("#modalAddingBooking #id_commentAddons").css({ "border": "2px solid red", "height": "150px" });
    $("#modalAddingBooking #id_commentAddons1").parent().css({ "top": "-326px", "width": "45%", "left": "55%", "position": "relative" });
    $("#modalAddingBooking #id_commentAddons1").css({ "border": "2px solid green", "height": "150px" });
    $("#modalAddingBooking #id_services").parent().css({ "width": "45%", "position": "relative", "top": "0px", "display": "block" });
    $("#modalAddingBooking #id_services").css({ "border": "2px solid red", "height": "150px" });
    $("#modalAddingBooking #id_servicesChange").parent().css({ "width": "45%", "position": "relative", "left": "55%", "top": "-166px" });
    $("#modalAddingBooking #id_servicesChange").css({ "border": "2px solid green", "height": "150px" });
    $("#modalAddingBooking #id_p_num_1").css({ "position": "relative", "left": "15%", "width": "95px", "top": "-38px" });
    $("#modalAddingBooking #id_deposit").parent().css({ "position": "relative", "top": "-310px" });
    $("#modalAddingBooking #id_deposit_card").parent().css({ "position": "relative", "top": "-320px" });
    $("#modalAddingBooking #id_commentText").parent().css({ "position": "relative", "top": "-330px" });
    $("#modalAddingBooking #buttonsMenuForm").parent().css({ "position": "relative", "top": "-310px" });

    $('#modalAddingBooking #id_idBooking').prop('hidden', true);
    $('#bookingactBtn').prop('hidden', true);
    $('#bookingdelBtn').prop('hidden', true);

    $('#modalAddingBooking #id_idBooking').parent().prop('hidden', true);
    $("#modalAddingBooking #id_commentAddons1 option").each(function () {
        $(this).prop('hidden', true);
    })
    $("#modalAddingBooking #id_servicesChange option").each(function () {
        $(this).prop('hidden', true);
    })
    $("body").on("change", "#modalAddingBooking #id_commentAddons", function (e) {
        $("#modalAddingBooking #id_commentAddons option:selected").each(function () {
            var ur = $(this)[0];
            $("#modalAddingBooking #id_commentAddons option[value='" + ur.value + "']").prop('hidden', true);
            $("#modalAddingBooking #id_commentAddons [value='" + ur.value + "']").prop("selected", false);

            $("#modalAddingBooking #id_commentAddons1 [value='" + ur.value + "']").prop("hidden", false);
            $("#modalAddingBooking #id_commentAddons1 [value='" + ur.value + "']").prop("selected", true);
        })
    })
    $("body").on("click", "#modalAddingBooking #id_commentAddons1 option", function (e) {
        var ur = $(this)[0];
        $("#modalAddingBooking #id_commentAddons1 option[value='" + ur.value + "']").prop('hidden', true);
        $("#modalAddingBooking #id_commentAddons1 option[value='" + ur.value + "']").prop('selected', false);
        $("#modalAddingBooking #id_commentAddons option[value='" + ur.value + "']").prop('hidden', false);
        $("#modalAddingBooking #id_commentAddons1 option:not(:hidden)").each(function () {
            var ur = $(this)[0];
            $("#modalAddingBooking #id_commentAddons1 [value='" + ur.value + "']").prop("selected", true);
        })
    })
    $("body").on("change", "#modalAddingBooking #id_services", function (e) {
        $("#modalAddingBooking #id_services option:selected").each(function () {
            var ur = $(this)[0];
            $("#modalAddingBooking #id_services option[value='" + ur.value + "']").prop('hidden', true);
            $("#modalAddingBooking #id_services [value='" + ur.value + "']").prop("selected", false);

            $("#modalAddingBooking #id_servicesChange [value='" + ur.value + "']").prop("hidden", false);
            $("#modalAddingBooking #id_servicesChange [value='" + ur.value + "']").prop("selected", true);
        })
    })
    $("body").on("click", "#modalAddingBooking #id_servicesChange option", function (e) {
        var ur = $(this)[0];
        $("#modalAddingBooking #id_servicesChange option[value='" + ur.value + "']").prop('hidden', true);
        $("#modalAddingBooking #id_servicesChange option[value='" + ur.value + "']").prop('selected', false);
        $("#modalAddingBooking #id_services option[value='" + ur.value + "']").prop('hidden', false);
        $("#modalAddingBooking #id_servicesChange option:not(:hidden)").each(function () {
            var ur = $(this)[0];
            $("#modalAddingBooking #id_servicesChange [value='" + ur.value + "']").prop("selected", true);
        })
    })
    $("body").on("dblclick", ".table-booking-content tr", function (e) {
        e.preventDefault();
        reviewBooking($(this).attr("id"));
    })

    $("body").on("click", "#bookingactBtn", function (e) {
        e.preventDefault();
        $.ajax({
            method: "POST",
            url: "/booking/session-booking/",
            data: {
                'activatedSID': $("#modalAddingBooking #id_idBooking").val(),
            },
            headers: { 'X-CSRFToken': '{{ csrf_token }}' },
            dataType: 'json',
            success: function (data) {
                console.log(data);
                $(location).attr('href', "/session/");

            }
        })
    });
    $("body").on("click", "#bookingdelBtn", function (e) {

        e.preventDefault();
        $.ajax({
            method: "POST",
            url: "/booking/delete-booking/",
            data: {
                'deletedSID': $("#modalAddingBooking #id_idBooking").val(),
            },
            headers: { 'X-CSRFToken': '{{ csrf_token }}' },
            dataType: 'json',
            success: function (data) {
                location.reload();
                alert("УСПЕШНО УДАЛЕНО")
            }
        })
    });
   

});