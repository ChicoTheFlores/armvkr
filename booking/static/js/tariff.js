var $ = django.jQuery;
$(document).ready(function () {
    if ($("#id_type option:selected").val() == "tp") {
        $("#id_price_fix1").parent().parent().prop("hidden", true);
        $("#id_time_limit").parent().parent().prop("hidden", true);
        $("#id_people_limit1").parent().parent().prop("hidden", true);
        $("#id_people_free1").parent().parent().prop("hidden", true);
    }
    else {
        $("#id_price_fix1").parent().parent().prop("hidden", false);
        $("#id_time_limit").parent().parent().prop("hidden", false);
        $("#id_people_limit1").parent().parent().prop("hidden", false);
        $("#id_people_free1").parent().parent().prop("hidden", false);
    }
    $("body").on("change", "#tarif_form #id_type", function (e) {
        if ($("#id_type option:selected").val() == "tp") {
            $("#id_price_fix1").parent().parent().prop("hidden", true);
            $("#id_time_limit").parent().parent().prop("hidden", true);
            $("#id_people_limit1").parent().parent().prop("hidden", true);
            $("#id_people_free1").parent().parent().prop("hidden", true);
        }
        else {
            $("#id_price_fix1").parent().parent().prop("hidden", false);
            $("#id_time_limit").parent().parent().prop("hidden", false);
            $("#id_people_limit1").parent().parent().prop("hidden", false);
            $("#id_people_free1").parent().parent().prop("hidden", false);
        }

    })
});