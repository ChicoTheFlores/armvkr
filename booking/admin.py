from django.contrib import admin
from .models import *


admin.site.register(OperatingCash)


class adminRoom(admin.ModelAdmin):
    model = Room

    def save_model(self, request, obj, form, change):
        obj.name_room = obj.name_room.replace(' ', '_')
        obj.save()


admin.site.register(Room, adminRoom)
admin.site.register(AddOn)
admin.site.register(Discounts)
admin.site.register(Service)


class sessionTariff(admin.ModelAdmin):
    model = Tarif
    list_display = ['name_tariff', 'price_tariff']
    readonly_fields = []

    class Media:
        js = ("js/tariff.js", )

    def save_model(self, request, obj, form, change):
        # self.readonly_fields.append("people_free1")
        obj.name_tariff = obj.name_tariff.replace(' ', '_')
        obj.save()


admin.site.register(Tarif, sessionTariff)
admin.site.register(clientPageStatus)
admin.site.register(PhoneSberbank)
admin.site.register(clientPageInfo)
admin.site.register(Days)

admin.site.register(UserComments)
admin.site.register(CardPay)
admin.site.register(CashPay)
admin.site.register(Deposit)
admin.site.register(UseService)


class sessionAdmin(admin.ModelAdmin):
    model = Session
    list_display = ['SID', 'company', 'room', 'start_dt', 'end_dt', 'tariff',
                    'discount', 'deposit', 'is_active', 'sum_session', 'comment', 'cash', 'card']


admin.site.register(Session, sessionAdmin)


class sessionBookings(admin.ModelAdmin):
    model = Booking
    list_display = ['start_booking', 'time_start', 'time_end',
                    'room', 'number_of_persons', 'tariff', 'deposit']


admin.site.register(Booking, sessionBookings)
