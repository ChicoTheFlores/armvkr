from django.shortcuts import render, render_to_response
from session.views import logoutView
from .models import *
import json
from booking.models import *
from daycalendar.models import *
from escpos.printer import Network  
from escpos.image import EscposImage as EI
from django.utils import dateformat
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from session.forms import BookingForm
from enter.models import *
from admins.models import Profile
from session.views import fullCheck, getSID
from datetime import timedelta


def booking(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/")
    if request.method == 'POST':
        formBooking = BookingForm(request.POST)
        if formBooking.is_valid():
            if formBooking.cleaned_data["idBooking"] == -1:
                booking = formBooking.save(commit=False)
                if (len(formBooking.cleaned_data["commentAddons1"]) > 0) or len(formBooking.cleaned_data["commentText"]):
                    comment = UserComments.objects.create(
                        text=formBooking.cleaned_data["commentText"])
                    comment.addons.set(
                        formBooking.cleaned_data["commentAddons1"])
                    comment.save()
                    booking.comment = comment
            else:
                booking = Booking.objects.get(
                    id=formBooking.cleaned_data["idBooking"])
                if booking.comment:
                    comment = booking.comment
                    comment.text = formBooking.cleaned_data["commentText"]
                else:
                    comment = UserComments.objects.create(
                        text=formBooking.cleaned_data["commentText"])
                comment.addons.set(formBooking.cleaned_data["commentAddons1"])
                comment.save()
                booking.comment = comment
                booking.tariff = Tarif.objects.filter(
                    name_tariff=formBooking.cleaned_data["tariff"]).first()
                booking.room = Room.objects.filter(
                    name_room=formBooking.cleaned_data["room"]).first()
                booking.start_booking = formBooking.cleaned_data["start_booking"]
                booking.name = formBooking.cleaned_data["name"]
                booking.time_start = formBooking.cleaned_data["time_start"]
                booking.time_end = formBooking.cleaned_data["time_end"]
                booking.number_of_persons = formBooking.cleaned_data["number_of_persons"]
                booking.deposit = formBooking.cleaned_data["deposit"]
                booking.deposit_card = formBooking.cleaned_data["deposit_card"]
            try:
                booking.phone = formBooking.cleaned_data["p_num"]
            except:
                pass
            booking.save()
            booking.services.set(formBooking.cleaned_data["servicesChange"])
            booking.save()
            return HttpResponseRedirect('/booking/')
        else:
            # return HttpResponse(str(formBooking.errors)+"<br><a href='/booking/'>Вернуться назад</a>")
            context = {}
            operatingCash = OperatingCash.objects.all().first()
            listBooking = Booking.objects.all()
            for b in listBooking:
                b.d = b.deposit + b.deposit_card
                if b.phone != None and b.phone != "":
                    b.phone = b.phone[:2] + ' (' + b.phone[2:5] + ') ' + b.phone[5:8] + "-" + b.phone[
                                                                                              8:10] + "-" + b.phone[
                                                                                                            10:12]
                b.dop = ""
                for serv in b.services.all():
                    b.dop += serv.name_service + ", "
                if b.comment != None:
                    for serv in b.comment.addons.all():
                        b.dop += serv.name + ", "
                    b.dop = b.dop[:-2]
            filterTariff = Tarif.objects.all()
            filterRoom = Room.objects.all()
            listsess = Session.objects.filter(is_active=True)
            countSessions = listsess.count()
            context['booking'] = listBooking
            context['filterRoom'] = filterRoom
            context['filterTariff'] = filterTariff
            context['operatingCash'] = operatingCash
            context['countSessions'] = countSessions
            context['formBooking'] = BookingForm()
            return render_to_response('awp/booking.html', context)

    else:
        context = {}
        operatingCash = OperatingCash.objects.all().first()
        listBooking = Booking.objects.all().order_by("start_booking", "room")
        for b in listBooking:
            b.d = b.deposit + b.deposit_card
            if b.phone != None and b.phone != "":
                b.phone = b.phone[:2] + ' (' + b.phone[2:5] + ') ' + \
                    b.phone[5:8] + "-" + b.phone[8:10] + "-" + b.phone[10:12]
            b.dop = ""
            for serv in b.services.all():
                b.dop += serv.name_service + ", "
            if b.comment != None:
                for serv in b.comment.addons.all():
                    b.dop += serv.name + ", "
                b.dop = b.dop[:-2]
        filterTariff = Tarif.objects.all()
        filterRoom = Room.objects.all()
        listsess = Session.objects.filter(is_active=True)
        countSessions = listsess.count()
        context['booking'] = listBooking
        context['filterRoom'] = filterRoom
        context['filterTariff'] = filterTariff
        context['operatingCash'] = operatingCash
        context['countSessions'] = countSessions
        context['formBooking'] = BookingForm()
        context['admin_groups'] = request.user.groups.filter(
            name='Админ').exists()
        try:
            context["resAdm"] = "(" + str(Profile.objects.get(
                reservedWork=True).user.username) + ")"
        except:
            pass
        return render(request, 'awp/booking.html', context)


@csrf_exempt
def info_booking(request):
    if (request.method == "POST"):
        thisSID = request.POST['thisSID']
        obj = Booking.objects.get(id=thisSID)
        shift = str(ShiftStart.objects.first().timestart.hour) + \
            "-"+str(ShiftStart.objects.first().timestart.minute)
        con = dict()
        dop = dict()
        commentServ = dict()

        dop.update({0: [0]})
        dn = datetime.now(tzl)
        delta = datetime(dn.year, dn.month, dn.day, obj.time_end.hour, obj.time_end.minute, 0, tzinfo=tzl) - \
            datetime(dn.year, dn.month, dn.day, obj.time_start.hour,
                     obj.time_start.minute, 0, tzinfo=tzl)
        duration = datetime(dn.year, dn.month, dn.day, int(
            delta.seconds/3600), int(delta.seconds % 3600/60), 0, tzinfo=tzl)
        con.update({0: [obj.start_booking, obj.time_start, obj.time_end, obj.number_of_persons,
                        obj.room.id, obj.deposit, obj.deposit_card, obj.tariff.id, duration.time(), obj.id, shift]})
        con.update({1: ["", ""]})
        con.update({2: ["", ""]})
        con.update({3: [""]})
        if obj.phone != None and obj.phone != "":
            con.update({1: [obj.phone[2:5], obj.phone[5:12]]})
        if obj.name != None and obj.name != "":
            con.update({2: [obj.name]})
        if obj.comment:
            con.update({3: [obj.comment.text]})
            i = 0
            for addon in obj.comment.addons.all():
                commentServ.update({i: [addon.id]})
                i += 1
            dop.update({0: [i]})
        servDict = dict()
        i = 0
        for service in obj.services.all():
            servDict.update({i: [service.id]})
            i += 1
        dop.update({1: [i]})
        return JsonResponse(FourDict(dop, con, commentServ, servDict))


@csrf_exempt
def delete_booking(request):
    if (request.method == "POST"):
        obj = Booking.objects.get(id=int(request.POST['deletedSID']))
        try:
            comm = obj.comment
            comm.delete()
        except:
            pass
        obj.delete()
        return JsonResponse(onemes("УСПЕШНО"))


@csrf_exempt
def activate_booking(request):
    if (request.method == "POST"):
        if request.is_ajax():
            activatedSID = request.POST['activatedSID']
            obj = Booking.objects.get(id=activatedSID)
            printerStart = PrinterStart.objects.first()
            if printerStart.is_print:
                printer = Network(printerStart.network)
            countBookingRecord = obj.number_of_persons
            try:
                CompprevSes = Session.objects.filter(start_dt__gte=datetime.now(
                    tzl).replace(day=1, hour=0)).last().company+1
            except:
                CompprevSes = 0
            try:
                IDprevSes = int(Session.objects.all().last().SID)+1
            except:
                IDprevSes = 0
            simpleSum = obj.deposit // countBookingRecord
            addingSum = obj.deposit % countBookingRecord
            simpleSum_card = obj.deposit_card // countBookingRecord
            addingSum_card = obj.deposit_card % countBookingRecord
            while (countBookingRecord > 0):
                t = obj.tariff
                r = obj.room
                ses = Session.objects.create(SID=IDprevSes,
                                             start_dt=datetime.now(tzl),
                                             company=CompprevSes,
                                             tariff=t,
                                             room=r
                                             )
                for service in obj.services.all():
                    newService = UseService.objects.create(add_serv=service)
                    newService.save()
                    ses.services.add(newService)
                    obj.services.remove(service)
                ses.save()
                if obj.comment != None:
                    ses.comment = obj.comment
                    obj.comment = None
                    ses.save()
                if simpleSum > 0:
                    dep = Deposit.objects.create(
                        cash=simpleSum, card=simpleSum_card)
                    if 0 == countBookingRecord-1:
                        dep.cash += addingSum
                        dep.card += addingSum_card
                    dep.save()
                    ses.deposit = dep
                ses.save()
                sid = getSID(ses.company, ses.SID, ses.start_dt)
                if printerStart.is_print:
                    fullCheck(ses, printer, sid)
                IDprevSes += 1
                countBookingRecord -= 1
            obj.delete()
            return JsonResponse(onemes("УСПЕШНО"))


