function GetterCalendar(data) {
    $(".table-calendar-content td").css("background", "white");
    $(".table-calendar-content td").prop('title', '');
    $(".table-calendar-content .tdClicked").text('');
    if (data.qS[0] == "B") {
        $("#timeline").css("display", "none");
        for (var i = 0; i < data.qS[1]; i++) {
            if (data.con[i][4] < data.con[i][1]) {
                cells = (24 + data.con[i][4] - data.con[i][1]) * 2;
            }
            else {
                cells = (data.con[i][4] - data.con[i][1]) * 2;
            }
            if (data.con[i][2] > data.con[i][5]) {
                cells--;
            }
            else if (data.con[i][2] < data.con[i][5]) {
                cells++;
            }
            hourscells = data.con[i][1];
            minutescells = data.con[i][2];
            $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]).css("background", "pink");
            $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]).prop("title", data.con[i][7]);
            $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]).attr("id", "0-" + String(data.con[i][9]));
            Text = data.con[i][8].split(";")
            $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]).text(Text[0]);
            for (var j = 1; j < cells; j++) {
                if (minutescells + 30 == 60) {
                    minutescells = 0;
                    hourscells++;
                    if (hourscells == 24) {
                        hourscells = 0;
                    }
                    obj = $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]);
                    obj.attr("id", String(j) + "-" + String(data.con[i][9]));
                    obj.css("background", "pink");
                    obj.prop("title", data.con[i][7]);
                    if (j == 1) {
                        obj.text(Text[1]);
                    }
                }
                else {
                    minutescells = 30;
                    obj = $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]);
                    obj.attr("id", String(j) + "-" + String(data.con[i][9]));
                    obj.css("background", "pink");
                    obj.prop("title", data.con[i][7]);
                    if (j == 1) {
                        obj.text(Text[1]);
                    }
                }
            }
        }
    }
    else {
        $("#timeline").css("display", "block");
        for (var i = 0; i < data.qS[0]; i++) {
            if (data.qS[2] < data.con[i][2]) {
                cells = (24 + data.qS[2] - data.con[i][2]) * 2;
            }
            else {
                cells = (data.qS[2] - data.con[i][2]) * 2;
            }
            if (data.con[i][3] > data.qS[3]) {
                cells--;
            }
            else if (data.con[i][3] < data.qS[3]) {
                cells++;
            }
            if (data.qS[4] > 0) {
                cells++;
            }
            hourscells = data.con[i][2];
            minutescells = data.con[i][3];
            $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]).css("background", "blue");
            $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]).prop('title', 'Текущая сессия (' + data.con[i][5] + ' чел.), начало в ' + data.con[i][4]);
            $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]).text(data.con[i][6]);

            for (var j = 1; j < cells; j++) {
                if (minutescells + 30 == 60) {
                    minutescells = 0;
                    hourscells++;
                    if (hourscells == 24) {
                        hourscells = 0;
                    }
                    obj = $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]);
                    obj.css("background", "blue");
                    obj.prop('title', 'Текущая сессия (' + data.con[i][5] + ' чел.), начало в ' + data.con[i][4]);
                    obj.text(data.con[i][6]);
                }
                else {
                    minutescells = 30;
                    obj = $("." + hourscells + "-" + minutescells + "-" + data.con[i][0]);
                    obj.css("background", "blue");
                    obj.prop('title', 'Текущая сессия (' + data.con[i][5] + ' чел.), начало в ' + data.con[i][4])
                    obj.text(data.con[i][6]);
                }
            }
        }
        for (var i = 0; i < data.qS[5]; i++) {
            if (data.cB[i][4] < data.cB[i][1]) {
                cells = (24 + data.cB[i][4] - data.cB[i][1]) * 2;
            }
            else {
                cells = (data.cB[i][4] - data.cB[i][1]) * 2;
            }
            if (data.cB[i][2] > data.cB[i][5]) {
                cells--;
            }
            else if (data.cB[i][2] < data.cB[i][5]) {
                cells++;
            }
            hourscells = data.cB[i][1];
            minutescells = data.cB[i][2];
            $("." + hourscells + "-" + minutescells + "-" + data.cB[i][0]).css("background", "pink");
            $("." + hourscells + "-" + minutescells + "-" + data.cB[i][0]).prop("title", data.cB[i][7]);
            $("." + hourscells + "-" + minutescells + "-" + data.cB[i][0]).attr("id", "0-" + String(data.cB[i][9]));
            Text = data.cB[i][8].split(";")
            $("." + hourscells + "-" + minutescells + "-" + data.cB[i][0]).text(Text[0]);
            for (var j = 1; j < cells; j++) {
                if (minutescells + 30 == 60) {
                    minutescells = 0;
                    hourscells++;
                    if (hourscells == 24) {
                        hourscells = 0;
                    }
                    obj = $("." + hourscells + "-" + minutescells + "-" + data.cB[i][0]);
                    obj.attr("id", String(j) + "-" + String(data.cB[i][9]));
                    obj.css("background", "pink");
                    obj.prop("title", data.cB[i][7]);
                    if (j == 1) {
                        obj.text(Text[1]);
                    }

                }
                else {
                    minutescells = 30;
                    obj = $("." + hourscells + "-" + minutescells + "-" + data.cB[i][0]);
                    obj.attr("id", String(j) + "-" + String(data.cB[i][9]));
                    obj.prop("title", data.cB[i][7]);
                    if (j == 1) {
                        obj.text(Text[1]);
                    }
                    obj.css("background", "pink");
                }
            }
        }
    }
}

$(document).ready(function () {
    document.oncontextmenu = function () { return false; };
    $("#editing_module").hide(600);
    var shift;
    $.ajax({
        method: "POST",
        url: "/daycalendar/getall/",
        data: {
        },
        dataType: 'json',
        success: function (data) {
            GetterCalendar(data);
            timelineMove(data.qS[2], data.qS[3], data.qS[4]);
            shift = data.qS[6];
            sessionStorage.setItem('shiftBook', shift);

        }
    })
    var allTdClicked = [];
    var TdClickedRoom = "";
    function compareNumeric(a, b) {
        a1 = a;
        b1 = b;
        if ((Number(a.split("-")[0]) < Number(shift.split("-")[0])) || (Number(a.split("-")[0]) == Number(shift.split("-")[0]) && Number(a.split("-")[1]) < Number(shift.split("-")[1]))) {
            x = Number(a.split("-")[0]) + 24;
            a1 = String(x) + "-" + a.split("-")[1];
        }
        if ((Number(b.split("-")[0]) < Number(shift.split("-")[0])) || (Number(b.split("-")[0]) == Number(shift.split("-")[0]) && Number(b.split("-")[1]) < Number(shift.split("-")[1]))) {
            x = Number(b.split("-")[0]) + 24;
            b1 = String(x) + "-" + b.split("-")[1];
        }
        if (Number(a1.split("-")[0]) > Number(b1.split("-")[0])) return 1;
        if (Number(a1.split("-")[0]) < Number(b1.split("-")[0])) return -1;
        if (Number(a1.split("-")[1]) > Number(b1.split("-")[1])) return 1;
        if (Number(a1.split("-")[1]) < Number(b1.split("-")[1])) return -1;
    }
    function creaseData(a, b) {
        a1 = a;
        b1 = b;
        if ((Number(a.split("-")[0]) < Number(shift.split("-")[0])) || (Number(a.split("-")[0]) == Number(shift.split("-")[0]) && Number(a.split("-")[1]) < Number(shift.split("-")[1]))) {
            x = Number(a.split("-")[0]) + 24;
            a1 = String(x) + "-" + a.split("-")[1];
        }
        if ((Number(b.split("-")[0]) < Number(shift.split("-")[0])) || (Number(b.split("-")[0]) == Number(shift.split("-")[0]) && Number(b.split("-")[1]) < Number(shift.split("-")[1]))) {
            x = Number(b.split("-")[0]) + 24;
            b1 = String(x) + "-" + b.split("-")[1];
        }
        var hours = Number(b1.split("-")[0]) - Number(a1.split("-")[0]);
        var minutes = Number(b1.split("-")[1]) - Number(a1.split("-")[1]);
        if (minutes < 0) {
            hours--;
            minutes = 30;
        }
        return (String(hours) + "-" + String(minutes));
    }

    $("body").on("dblclick", ".tdClicked", function (e) {
        if (this.style.background == "pink") {
            $(location).attr('href', "/booking/");
            sessionStorage.setItem('key', 'true');
            sessionStorage.setItem('sidActivateFromCalendar', $(this).attr("id").split("-")[1]);
        }
        else {
            allTdClicked.sort(compareNumeric);
            if (allTdClicked.length < 2) {
                allTdClicked.push(allTdClicked[0]);
            }
            var lastTdClicked = allTdClicked[allTdClicked.length - 1].split("-");
            if (lastTdClicked[1] == "30") {
                lastTdClicked[1] = "0";
                lastTdClicked[0]++;
                strlastTdClicked = String(lastTdClicked[0]) + "-" + lastTdClicked[1];
            }
            else if (lastTdClicked[1] == "0") {
                lastTdClicked[1] = "30";
                strlastTdClicked = String(lastTdClicked[0]) + "-" + lastTdClicked[1];
            }
            allTdClicked[allTdClicked.length - 1] = strlastTdClicked;
            var duration = creaseData(allTdClicked[0], allTdClicked[allTdClicked.length - 1])
            $(location).attr('href', "/booking/");
            sessionStorage.setItem('key', 'true');
            sessionStorage.setItem('startBook', allTdClicked[0]);
            sessionStorage.setItem('endBook', allTdClicked[allTdClicked.length - 1]);
            sessionStorage.setItem('durationBook', duration);
            sessionStorage.setItem('roomBook', TdClickedRoom);
            sessionStorage.setItem('dateBook', $("#dataCalendar").val());
        }
    });
    $("body").on("mousemove", ".tdClicked", function (e) {
        thisClr = this.style.background;
        if (e.which === 1 && thisClr == "white") {
            $(this).css("background", "red");
            var mas = this.className.split(" ")[0].split("-");
            var str = mas[0] + "-" + mas[1];
            console.log(str)
            TdClickedRoom = mas[2];
            allTdClicked.push(str);
        }

    });
    $("body").on("click", ".tdClicked", function (e) {
        if (e.which === 1) {
            thisClr = this.style.background;
            if (thisClr == "white") {
                $(this).css("background", "red");
                var mas = this.className.split(" ")[0].split("-");
                var str = mas[0] + "-" + mas[1];
                TdClickedRoom = mas[2];
                allTdClicked.push(str);
            }

            else if (this.style.background == "red") {
                $(this).css("background", "white");
                var mas = this.className.split(" ")[0].split("-")
                var str = mas[0] + "-" + mas[1];
                allTdClicked.splice(allTdClicked.indexOf(str), 1);
            }
        }
    });
    $('#dataCalendar').on('change', function () {
        $.ajax({
            method: "POST",
            url: "/daycalendar/getcalendar/",
            data: {
                'needDate': $("#dataCalendar").val(),
            },
            headers: { 'X-CSRFToken': '{{ csrf_token }}' },
            dataType: 'json',
            success: function (data) {
                GetterCalendar(data);
            }
        })
    })
    theTimeHelper();

    function timelineMove(hour, clearmin, dirtymin) {
        marginTimeline = $("." + hour + "-" + clearmin + "-1").position().top;
        marginTimeline += $("#tableCalendar").position().top;
        marginTimeline += (dirtymin - clearmin) * ($("." + hour + "-" + clearmin + "-1").outerHeight() / 30);
        $("#timeline").css("top", marginTimeline);
    }
});

function theTimeHelper() {
    setInterval('getTime()', 60000);
}
function getTime() {
    hour = moment().format("h");
    dirtymin = moment().format("mm");

    if (Number(dirtymin) < 30) {
        clearmin = 0;
    }
    else clearmin = 30;
    refreshBlocks();
}
function timelineMoves(hour, clearmin, dirtymin) {
    marginTimeline = $("." + hour + "-" + clearmin + "-1").position().top;
    marginTimeline += $("#tableCalendar").position().top;
    marginTimeline += (dirtymin - clearmin) * ($("." + hour + "-" + clearmin + "-1").outerHeight() / 30);
    $("#timeline").css("top", marginTimeline);
}
function refreshBlocks() {
    $.ajax({
        method: "POST",
        url: "/daycalendar/getall/",
        data: {
        },
        dataType: 'json',
        success: function (data) {
            GetterCalendar(data);
            timelineMoves(data.qS[2], data.qS[3], data.qS[4]);
        }
    })
}