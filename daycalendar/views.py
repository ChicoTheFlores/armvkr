from django.shortcuts import render
from booking.views import logoutView
from .models import *
import json
from datetime import datetime, timedelta
from session.models import *
from enter.models import *
from admins.models import Profile
from django.utils import dateformat
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.contrib.auth import authenticate, login


def daycalendar(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/")
    context = {}
    rooms = Room.objects.all()
    i = 1
    for room in rooms:
        room.rid = i
        i += 1
    context["rooms"] = rooms
    context['countSessions'] = Session.objects.filter(is_active=True).count()
    context['operatingCash'] = OperatingCash.objects.all().first()
    context['admin_groups'] = request.user.groups.filter(name='Админ').exists()
    try:
        context["resAdm"] = "(" + \
            str(Profile.objects.get(reservedWork=True).user.username) + ")"
    except:
        pass
    nowDate = datetime.now(tzl)
    shiftstart = []
    getterTime = ShiftStart.objects.first().timestart
    today = datetime.now()
    today = today.replace(hour=getterTime.hour, minute=getterTime.minute)
    shiftstart.append(getterTime)
    i = 0
    while (i < 47):
        today += timedelta(minutes=30)
        shiftstart.append(today.time)
        i += 1
    if ((nowDate.hour < getterTime.hour) or (nowDate.hour == getterTime.hour and nowDate.minute < getterTime.minute)):
        nowDate -= timedelta(days=1)
    context["nowDate"] = nowDate.date().strftime("%Y-%m-%d")
    context["shiftstart"] = shiftstart
    return render(request, 'awp/daycalendar.html', context)


@csrf_exempt
def getcalendar(request):
    if (request.method == "POST"):
        needDate = request.POST['needDate'].split("-")
        NeedDatetime = datetime(int(needDate[0]), int(
            needDate[1]), int(needDate[2]), 0, 0, tzinfo=tzl)
        if datetime.now(tzl).time() < ShiftStart.objects.first().timestart:
            NeedDatetime += timedelta(days=1)
        if (NeedDatetime.date() == datetime.now(tzl).date()):
            fullCon = CalendarGetter()
            return JsonResponse(calendarDict(fullCon[0], fullCon[1], fullCon[2]))

        else:
            if datetime.now(tzl).time() < ShiftStart.objects.first().timestart:
                NeedDatetime -= timedelta(days=1)
            bookingFull = Booking.objects.filter(
                start_booking=NeedDatetime.date())
            qbooking = len(bookingFull)
            con0 = dict()
            con = dict()
            con0.update({0: "B"})
            con0.update({1: qbooking})
            i = 0
            for book in bookingFull:
                if book.time_start.minute >= 30:
                    book.fullminute = 30
                else:
                    book.fullminute = 0
                if book.time_end.minute >= 30:
                    book.fullminuteEnd = 30
                else:
                    book.fullminuteEnd = 0
                dopInfo = ""
                smInfo = ""
                if book.name != None and book.name != "":
                    dopInfo += book.name + ", "
                    smInfo += book.name + ";"
                if book.phone != None and book.phone != "":
                    dopInfo += book.phone + ", "
                dopInfo += str(book.number_of_persons) + " чел., "
                # smInfo += str(book.number_of_persons) + " чел., "

                for obj in book.services.all():
                    dopInfo += obj.name_service + ","
                    smInfo += obj.name_service + ","
                smInfo = smInfo[:-1]
                if book.comment != None:
                    if book.comment.text != None and book.comment.text != "":
                        dopInfo += book.comment.text + ", "
                    for obj in book.comment.addons.all():
                        dopInfo += obj.name + ","
                        smInfo += obj.name + ","
                con.update({i: [book.room.id, book.time_start.hour, book.fullminute, book.time_start.strftime("%H:%M"),
                                book.time_end.hour, book.fullminuteEnd, book.time_end.strftime("%H:%M"), dopInfo, smInfo, book.id]})
                i += 1
            return JsonResponse(fulldict(con0, con))


@csrf_exempt
def getall(request):
    if (request.method == "POST"):
        fullCon = CalendarGetter()
        return JsonResponse(calendarDict(fullCon[0], fullCon[1], fullCon[2]))
