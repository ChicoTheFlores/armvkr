# Generated by Django 2.2.2 on 2019-09-15 08:01

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('daycalendar', '0002_runstart'),
    ]

    operations = [
        migrations.AlterField(
            model_name='runstart',
            name='date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='Дата последнего логина/логаута'),
        ),
    ]
