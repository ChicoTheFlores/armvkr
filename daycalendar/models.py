from django.db import models
from django.conf import settings
from django.utils import timezone
from booking.models import *
from .models import *
from datetime import datetime, timedelta
from AWPkamenka.settings import TIME_ZONE
from django.contrib.auth.models import User
import pytz
tzl = pytz.timezone(TIME_ZONE)


class ShiftStart(models.Model):
    timestart = models.TimeField(verbose_name="Обычное время начала смены")
    timetransfer = models.TimeField(
        verbose_name="Обычное время пересмены", default=datetime.now, null=True)
    timeend = models.TimeField(verbose_name="Обычное время конца смены")
    timeweekend = models.TimeField(
        verbose_name="Предпраздничное время конца смены")

    class Meta:
        verbose_name = "Настройка времени"
        verbose_name_plural = "Настройки времени"


class SettingStart(models.Model):
    timetodelete = models.PositiveIntegerField(
        verbose_name="Кол-во минут, при котором можно удалить посетителя", default=2)

    class Meta:
        verbose_name = "Другие настройки"
        verbose_name_plural = "Другие настройки"


class PrinterStart(models.Model):
    is_print = models.BooleanField(
        verbose_name="Принтер подключен", default=False)
    network = models.CharField(
        verbose_name="Network", default="", max_length=50)

    class Meta:
        verbose_name = "Настройка принтера"
        verbose_name_plural = "Настройки принтера"


class RunStart(models.Model):
    is_run = models.BooleanField(verbose_name="Переключатель", default=False)

    class Meta:
        verbose_name = "Настройка run.py"
        verbose_name_plural = "Настройки run.py"


def CalendarGetter():
    nowDate = datetime.today().date()
    sessions = Session.objects.filter(is_active=True)
    _sessions = dict()
    for session in sessions:
        if session.company not in _sessions:
            _sessions[session.company] = session
    sessions = _sessions.values()
    qSession = len(sessions)
    fullCon = dict()
    con0 = dict()
    con1 = dict()
    con = dict()
    con0.update({0: qSession})
    con0.update({1: nowDate})
    nowHstr = datetime.today().hour
    nowMstr = datetime.today().minute
    con0.update({2: nowHstr})
    if nowMstr >= 30:
        con0.update({3: 30})
        con0.update({4: nowMstr})
    else:
        con0.update({3: 0})
        con0.update({4: nowMstr})
    i = 0
    for ses in sessions:
        smInfo = ""
        if ses.start_dt.astimezone(tzl).minute >= 30:
            ses.fullminute = 30
        else:
            ses.fullminute = 0
        ses.updminute = ses.start_dt.astimezone(tzl).strftime("%H:%M")
        if ses.comment != None:
            for obj in ses.comment.addons.all():
                smInfo += obj.name + ","
        con.update({i: [ses.room.id, ses.start_dt.date(), ses.start_dt.astimezone(tzl).hour,
                        ses.fullminute, ses.updminute, len(Session.objects.filter(room=ses.room)), smInfo]})
        i += 1
    if len(con) == 0:
        con.update({0: [Room.objects.first().id]})
    if datetime.now(tzl).time() < ShiftStart.objects.first().timestart:
        bookingFull = Booking.objects.filter(
            start_booking=datetime.now(tzl).date()-timedelta(days=1))
    else:
        bookingFull = Booking.objects.filter(
            start_booking=datetime.now(tzl).date())
    qbooking = len(bookingFull)
    con0.update({5: qbooking})
    con0.update({6: str(ShiftStart.objects.first().timestart.hour) +
                 "-"+str(ShiftStart.objects.first().timestart.minute)})
    i = 0
    for book in bookingFull:
        if book.time_start.minute >= 30:
            book.fullminute = 30
        else:
            book.fullminute = 0
        if book.time_end.minute >= 30:
            book.fullminuteEnd = 30
        else:
            book.fullminuteEnd = 0
        dopInfo = ""
        smInfo = ""

        if book.name != None and book.name != "":
            dopInfo += book.name + ", "
            smInfo += book.name + ";"

        if book.phone != None and book.phone != "":
            dopInfo += book.phone + ", "
        dopInfo += str(book.number_of_persons) + " чел., "
        # smInfo += str(book.number_of_persons) + " чел., "
        for obj in book.services.all():
            dopInfo += obj.name_service + ","
            smInfo += obj.name_service + ","
        smInfo = smInfo[:-1]
        if book.comment != None:
            if book.comment.text != None and book.comment.text != "":
                dopInfo += book.comment.text + ", "
            for obj in book.comment.addons.all():
                dopInfo += obj.name + ","
        con1.update({i: [book.room.id, book.time_start.hour, book.fullminute, book.time_start.strftime("%H:%M"),
                         book.time_end.hour, book.fullminuteEnd, book.time_end.strftime("%H:%M"), dopInfo, smInfo, book.id]})
        i += 1
    fullCon.update({0: con0})
    fullCon.update({1: con})
    fullCon.update({2: con1})
    return fullCon
