from django.contrib import admin
from .models import *
admin.site.register(PrinterStart)
admin.site.register(SettingStart)
admin.site.register(RunStart)


class adminShiftStart(admin.ModelAdmin):
    model = ShiftStart

    def save_model(self, request, obj, form, change):
        if ((len(ShiftStart.objects.all()) == 1 and obj == ShiftStart.objects.get()) or len(ShiftStart.objects.all()) < 1):
            obj.save()


admin.site.register(ShiftStart, adminShiftStart)
