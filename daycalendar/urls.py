from django.conf.urls import url, include

from . import views

urlpatterns = [
    url(r'^logout/$', views.logoutView.as_view(), name="logout"),
    url(r'^$', views.daycalendar, name='daycalendar'),
    url(r'^getall/$', views.getall),
    url(r'^getcalendar/$', views.getcalendar),
]