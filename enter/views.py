from django.shortcuts import render
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.generic.edit import FormView
import logging
from .models import *
from admins.models import *
from daycalendar.models import *
from run import buildChangedPlan, removeChangedPlan
from datetime import datetime, timezone, time
from AWPkamenka.settings import TIME_ZONE
import pytz
from django.db.models import Q

tzl = pytz.timezone(TIME_ZONE)

logging.basicConfig(level=logging.DEBUG)


class LoginEnterForm(FormView):
    form_class = AuthenticationForm
    template_name = "enter/auth.html"
    success_url = "/"

    def get(self, request):
        if request.user.is_authenticated:
            return HttpResponseRedirect("/session/")
        else:
            context = {}
            context["form"] = LoginEnterForm()
            return self.render_to_response(self.get_context_data())

    def post(self, request):
        userP = request.POST['username']
        passP = request.POST['password']
        user = authenticate(username=userP, password=passP)
        if user is not None:
            if user.is_active:
                login(request, user)
                if request.user.groups.filter(name='Админ').exists():
                    prof_object = Profile.objects.get(
                        user=User.objects.get(username=userP))
                    prof_object.enter_time = datetime.now(tzl)
                    prof_object.save()
                    return HttpResponseRedirect("/session/")
                elif request.user.groups.filter(name='Кладовщик').exists():
                    return HttpResponseRedirect("/warehouse/")
                else:
                    return HttpResponseRedirect("/admins/")
            else:
                return HttpResponseRedirect("/")
        else:
            return HttpResponseRedirect("/")
