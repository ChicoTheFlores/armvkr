from django.contrib.admin import widgets
from django.forms import ModelForm, CheckboxSelectMultiple, Textarea, TimeInput, DateInput, CharField, MultiValueField, ChoiceField, MultiWidget, TextInput, Select
from daycalendar.models import ShiftStart
from booking.models import *
from django import forms
from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from AWPkamenka.settings import TIME_ZONE
import pytz
tzl = pytz.timezone(TIME_ZONE)


class PhoneWidget(MultiWidget):
    def __init__(self,  num_length=10, attrs=None):
        widgets = [TextInput(
            attrs={'size': num_length, 'maxlength': num_length, 'class': 'rightPos'})]
        super(PhoneWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.number[0]]
        else:
            return ['']

    def format_output(self, rendered_widgets):
        return '+7' + rendered_widgets[0]


class PhoneField(MultiValueField):
    def __init__(self, num_length, *args, **kwargs):
        list_fields = [CharField()]
        super(PhoneField, self).__init__(list_fields,
                                         widget=PhoneWidget(num_length), *args, **kwargs)

    def compress(self, values):
        try:
            return '+7' + values[0]
        except:
            return None


class NewSessionForm(ModelForm):
    commentAddons = forms.ModelMultipleChoiceField(
        AddOn.objects.all(), label="Бесплатные услуги", required=False)
    commentAddonsChange = forms.ModelMultipleChoiceField(
        AddOn.objects.all(), required=False, label="")
    servicesAll = forms.ModelMultipleChoiceField(
        Service.objects.all(), label="Платные услуги", required=False)
    servicesAllChange = forms.ModelMultipleChoiceField(
        Service.objects.all(), required=False, label="")
    numberPersons = forms.IntegerField(label="Количество человек", widget=TextInput(
        attrs={'min': 0, 'type': 'number', 'class': 'rightPos'}))
    commentText = forms.CharField(label="Комментарий", max_length=50,
                                  required=False, widget=TextInput(attrs={'class': 'rightPos'}))
    depositSum = forms.IntegerField(label="Сумма депозита", required=False, widget=TextInput(
        attrs={'type': 'number', 'class': 'rightPos'}))
    isCard = forms.BooleanField(label="Депозит картой", required=False)
    field_order = ['room', 'tariff', 'numberPersons']

    class Meta:
        model = Session
        fields = ('room', 'tariff', 'discount', )
        widgets = {
            'room': Select(attrs={'class': 'rightPos'}),
            'tariff': Select(attrs={'class': 'rightPos'}),
            'discount': Select(attrs={'class': 'rightPos'}),
        }

    def clean_discount(self):
        data = self.cleaned_data['discount']
        dataTariff = self.cleaned_data['tariff']
        if data != None:
            if not Discounts.objects.get(id=data.id) in Tarif.objects.get(id=dataTariff.id).discounts.all():
                raise forms.ValidationError(
                    'Для данного тарифа недоступна выбранная скидка!')

        return data


class EditSessionForm(ModelForm):
    commentAddons = forms.ModelChoiceField(AddOn.objects.all(
    ), label="Бесплатные услуги", required=False, widget=Select(attrs={'class': 'rightPos'}))
    servicesAll = forms.ModelChoiceField(Service.objects.all(
    ), label="Платные услуги", required=False, widget=Select(attrs={'class': 'rightPos'}))
    commentText = forms.CharField(label="Комментарий", max_length=50,
                                  required=False, widget=TextInput(attrs={'class': 'rightPos'}))
    depositSum = forms.IntegerField(label="Сумма депозита(наличн)", required=False, widget=TextInput(
        attrs={'disabled': 'disabled', 'class': 'rightPos'}))
    depositSumC = forms.IntegerField(label="Сумма депозита(картой)", required=False, widget=TextInput(
        attrs={'disabled': 'disabled', 'class': 'rightPos'}))
    adddepositSum = forms.IntegerField(required=False, label="Добавить", initial=0, widget=TextInput(
        attrs={'class': 'rightPos', 'type': 'number'}))
    isCard = forms.BooleanField(label="Депозит картой", required=False)

    field_order = ['room', 'tariff']

    class Meta:
        model = Session
        fields = ('room', 'tariff', 'discount',)
        widgets = {
            'room': Select(attrs={'class': 'rightPos'}),
            'tariff': Select(attrs={'class': 'rightPos'}),
            'discount': Select(attrs={'class': 'rightPos'}),

        }

    def clean_discount(self):
        data = self.cleaned_data['discount']
        dataTariff = self.cleaned_data['tariff']
        if data != None:
            if not Discounts.objects.get(id=data.id) in Tarif.objects.get(id=dataTariff.id).discounts.all():
                raise ValidationError(
                    'Для данного тарифа недоступна выбранная скидка!')

        return data


class BookingForm(ModelForm):
    idBooking = forms.IntegerField(required=False, initial=-1)
    commentAddons = forms.ModelMultipleChoiceField(
        AddOn.objects.all(), label="Бесплатные услуги", required=False)
    commentAddons1 = forms.ModelMultipleChoiceField(
        AddOn.objects.all(), required=False, label="")
    commentText = forms.CharField(label="Комментарий", max_length=50,
                                  required=False, widget=TextInput(attrs={'class': 'rightPos'}))
    duration = forms.TimeField(label="Длительность", widget=TimeInput(
        attrs={'type': 'time', 'step': 1800, 'min': "00:00", 'max': "24:00", 'class': 'rightPos'}))
    servicesChange = forms.ModelMultipleChoiceField(
        Service.objects.all(), required=False, label="")
    p_num = PhoneField(num_length=10, label="Телефон (без +7)", required=False)
    field_order = ['start_booking', 'time_start', 'time_end', 'duration', 'name', 'p_num', 'room',
                   'tariff', 'number_of_persons', 'services', 'servicesChange', 'commentAddons', 'commentAddons1']

    def clean(self):
        start = self.cleaned_data['time_start']
        end = self.cleaned_data['time_end']
        roomId = self.cleaned_data['room']
        date = self.cleaned_data['start_booking']
        r = Room.objects.filter(name_room=roomId).first()
        allBooking = Booking.objects.filter(room=r, start_booking=date).exclude(
            id=self.cleaned_data['idBooking'])
        shift = ShiftStart.objects.first().timestart
        dn = datetime.now(tzl)
        startT = datetime(date.year, date.month, date.day,
                          start.hour, start.minute, 0, tzinfo=tzl)
        if start < shift:
            startT += timedelta(days=1)
        endT = datetime(date.year, date.month, date.day,
                        end.hour, end.minute, 0, tzinfo=tzl)
        if end < shift:
            endT += timedelta(days=1)
        flag = False
        for book in allBooking:
            thisStartT = datetime(book.start_booking.year, book.start_booking.month,
                                  book.start_booking.day, book.time_start.hour, book.time_start.minute, 0, tzinfo=tzl)
            if book.time_start < shift:
                thisStartT += timedelta(days=1)
            thisEndT = datetime(book.start_booking.year, book.start_booking.month, book.start_booking.day,
                                book.time_end.hour, book.time_end.minute, 0, tzinfo=tzl)
            if book.time_end < shift:
                thisEndT += timedelta(days=1)
            if startT >= thisStartT and startT < thisEndT or endT > thisStartT and endT <= thisEndT or startT <= thisStartT and endT >= thisEndT:
                flag = True
                break
        if flag:
            raise ValidationError(
                "Данная комната занята в указанный промежуток времени!")
        return self.cleaned_data

    class Meta:
        model = Booking
        fields = ('start_booking', 'name', 'time_start', 'time_end',
                  'number_of_persons', 'room', 'tariff', 'services', 'deposit', 'deposit_card')
        widgets = {
            'start_booking': DateInput(attrs={'type': 'date', 'class': 'rightPos'}),
            'time_start': TimeInput(attrs={'type': 'time', 'step': 1800, 'min': "00:00", 'max': "24:00", 'class': 'rightPos'}),
            'time_end': TimeInput(attrs={'type': 'time', 'step': 1800, 'min': "00:00", 'max': "24:00", 'class': 'rightPos'}),
            'name': TextInput(attrs={'class': 'rightPos'}),
            'room': Select(attrs={'class': 'rightPos'}),
            'tariff': Select(attrs={'class': 'rightPos'}),
            'number_of_persons': TextInput(attrs={'type': 'number', 'class': 'rightPos'}),
            'deposit': TextInput(attrs={'type': 'number', 'class': 'rightPos'}),
            'deposit_card': TextInput(attrs={'type': 'number', 'class': 'rightPos'}),
            'commentText': TextInput(attrs={'class': 'rightPos'}),

        }
