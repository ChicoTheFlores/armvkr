from django.conf.urls import url, include
from . import views
from django.urls import path
urlpatterns = [
    url(r'^logout/$', views.logoutView.as_view(), name="logout"),
    url(r'^$', views.templ, name='session'),
    path('<int:pk>/edit/', views.edit_session),
    url(r'^info-session/$', views.info_session),
    url(r'^session-pay/$', views.session_pay),
    url(r'^session-reset/$', views.session_reset),
    url(r'^stop-session/$', views.stop_session),
    url(r'^filter-session/$', views.filter_session),
    url(r'^session-error-delete/$', views.session_error_delete),
    url(r'^print-session/$', views.print_session),
    url(r'^admin-helper/$', views.admin_helper),
    url(r'^weekday-tar-dis/$', views.weekday_tar_dis),
    url(r'^tariff-discount/$', views.tar_dis),
    url(r'^set-default/$', views.set_default),
    url(r'^discount-tariff/$', views.dis_tar),
    url(r'^clientPage/$', views.clientPage),
    url(r'^changeClientPage/$', views.changeClientPage),
    url(r'^updateClientInfo/$', views.updateClientInfo),
    url(r'^setClientPageStatus/$', views.setClientPageStatus),
    url(r'^checkClientPageStatus/$', views.checkClientPageStatus),
    url(r'^clearClientPageInfo/$', views.clearClientPageInfo),
]