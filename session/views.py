from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.generic.edit import FormView
from django.views.generic.base import View
from django.utils import dateformat
from django.contrib.auth import logout
from transliterate import translit, get_available_language_codes
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from .forms import NewSessionForm, EditSessionForm
from django.core import serializers
from admins.models import *
from run import buildChangedPlan, removeChangedPlan
from daycalendar.models import *
from escpos import escpos
from escpos.printer import Network  
from escpos.image import EscposImage as EI
from booking.models import *
from AWPkamenka.settings import STATIC_ROOT
from AWPkamenka.timework import *

from enter.models import *
import io
import os
#import cups

from django.contrib.staticfiles.templatetags.staticfiles import static
from xhtml2pdf import pisa
tzl = pytz.timezone(TIME_ZONE)


def templ(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect("/")
    if request.method == 'POST':
        formNewSession = NewSessionForm(request.POST)
        if formNewSession.is_valid():
            i = 0
            try:
                simpleSum = formNewSession.cleaned_data["depositSum"] // formNewSession.cleaned_data["numberPersons"]
                addingSum = formNewSession.cleaned_data["depositSum"] % formNewSession.cleaned_data["numberPersons"]
            except:
                simpleSum = -1
            try:
                CompprevSes = Session.objects.filter(start_dt__gte=datetime.now(
                    tzl).replace(day=1, hour=0)).last().company+1
            except:
                CompprevSes = 0
            try:
                IDprevSes = int(Session.objects.all().last().SID)+1
            except:
                IDprevSes = 0
            session = formNewSession.save(commit=False)
            session.SID = IDprevSes
            session.company = CompprevSes
            session.start_dt = datetime.now(tzl)
            if Tarif.objects.get(name_tariff=formNewSession.cleaned_data["tariff"]).people_free1 > 0:
                session.owner = True
            try:
                if Discounts.objects.get(name=formNewSession.cleaned_data["discount"]).people_free > 0:
                    session.owner = True
                    discountsfree = Discounts.objects.get(
                        name=formNewSession.cleaned_data["discount"]).people_free - 1
            except:
                discountsfree = 0
            session.save()
            for us in formNewSession.cleaned_data["servicesAllChange"]:
                useservice = UseService.objects.create(
                    add_serv=Service.objects.filter(name_service=us).first())
                useservice.save()
                session.services.add(useservice)
            session.save()
            if (len(formNewSession.cleaned_data["commentAddonsChange"]) > 0) or len(formNewSession.cleaned_data["commentText"]):
                comment = UserComments.objects.create(
                    text=formNewSession.cleaned_data["commentText"])
                comment.addons.set(
                    formNewSession.cleaned_data["commentAddonsChange"])
                comment.save()
                session.comment = comment
            session.save()
            if simpleSum > 0:
                depF = Deposit.objects.create()
                if formNewSession.cleaned_data["isCard"]:
                    depF.card = addingSum + simpleSum
                else:
                    depF.cash = addingSum + simpleSum
                depF.save()
                session.deposit = depF
            session.save()
            sid = getSID(session.company, session.SID, session.start_dt)
            printerStart = PrinterStart.objects.first()
            if printerStart.is_print:
                printer = Network(printerStart.network)
                fullCheck(session, printer, sid)
            i = 1
            IDprevSes += 1
            people_free1 = session.tariff.people_free1 - 1
            while i < formNewSession.cleaned_data["numberPersons"]:
                ses = Session.objects.create(SID=IDprevSes,
                                             start_dt=datetime.now(tzl),
                                             company=CompprevSes,
                                             tariff=session.tariff,
                                             room=session.room
                                             )
                if people_free1 > 0:
                    ses.owner = True
                    people_free1 -= 1
                if discountsfree > 0:
                    ses.owner = True
                    discountsfree -= 1
                ses.save()
                if simpleSum > 0:
                    dep = Deposit.objects.create()
                    if formNewSession.cleaned_data["isCard"]:
                        dep.card = simpleSum
                    else:
                        dep.cash = simpleSum
                    dep.save()
                    ses.deposit = dep
                    ses.save()
                sid = getSID(ses.company, ses.SID, ses.start_dt)
                if printerStart.is_print:
                    fullCheck(ses, printer, sid)
                IDprevSes += 1
                i += 1
            return HttpResponseRedirect('/session/')
        else:
            return HttpResponse(str(formNewSession.errors)+"<br><a href='/session/'>Вернуться назад</a>")
    else:
        context = {}
        operatingCash = OperatingCash.objects.all().first()
        context['operatingCash'] = operatingCash

        filterService = Service.objects.all()
        filterTariff = Tarif.objects.all()
        filterRoom = Room.objects.all()

        context['filterRoom'] = filterRoom
        context['filterTariff'] = filterTariff
        context['filterService'] = filterService

        filterGroup = set()
        filterSID = set()
        listsess = Session.objects.filter(is_active=True).order_by("SID")
        countSessions = listsess.count()
        context['countSessions'] = countSessions
        for ses in listsess:
            ses.end_dt = datetime.now(tzl)
            ses.save()
            filterGroup.add(ses.company)
            filterSID.add(ses.SID)
            ses.fullprice = 0
            ses.fullservs = ""
            for serv in ses.services.all():
                ses.fullservs += serv.add_serv.name_service
                if serv.count_serv == 1:
                    ses.fullservs += ","
                else:
                    ses.fullservs += "-" + str(serv.count_serv)+"шт.,"
                ses.fullprice += serv.count_serv * serv.add_serv.price_service
            ses.fullservs = ses.fullservs[:-1]
            try:
                ses.comservs = ""
                for comserv in ses.comment.addons.all():
                    ses.comservs += comserv.name + ","
                ses.comservs = ses.comservs[:-1]
            except:
                pass
        context['sessions'] = listsess
        shiftstart = ShiftStart.objects.first()
        start = datetime.now(tzl).replace(hour=shiftstart.timestart.hour,
                                          minute=shiftstart.timestart.minute)
        end = datetime.now(tzl)
        if datetime.now(tzl).time() < shiftstart.timestart:
            start -= timedelta(days=1)
        listOffSess = Session.objects.filter(
            is_active=False, start_dt__range=(start, end)).order_by("SID")
        for ses in listOffSess:
            ses.fullpay = 0
            if ses.card is not None:
                ses.cardpay = ses.card.amount_card
                ses.fullpay += ses.cardpay
            if ses.cash is not None:
                ses.cashpay = ses.cash.amount_cash
                ses.fullpay += ses.cashpay
            ses.fullprice = 0
            ses.fullservs = ""
            for serv in ses.services.all():
                ses.fullservs += serv.add_serv.name_service
                if serv.count_serv == 1:
                    ses.fullservs += ","
                else:
                    ses.fullservs += "-" + str(serv.count_serv)+"шт.,"
                ses.fullprice += serv.count_serv * serv.add_serv.price_service
            ses.fullservs = ses.fullservs[:-1]
            try:
                ses.comservs = ""
                for comserv in ses.comment.addons.all():
                    ses.comservs += comserv.name + ","
                ses.comservs = ses.comservs[:-1]
            except:
                pass

        context['sessionsOff'] = listOffSess
        context['filterGroup'] = filterGroup
        context['filterSID'] = filterSID
        context['formNewSession'] = NewSessionForm()
        context['formEditSession'] = EditSessionForm()
        context['admin_groups'] = request.user.groups.filter(
            name='Админ').exists()
        try:
            context["resAdm"] = "(" + str(Profile.objects.get(
                reservedWork=True).user.username) + ")"
        except:
            pass
        return render(request, 'session/session.html', context)


def fullCheck(session, printer, sid):
    printer.set(align='center')
    printer.image(os.path.join(STATIC_ROOT, 'img/kamenka256.jpg'))
    printer.set(text_type='B', width=2, height=1, align='left')
    text = '\n' + "Time-Club KAMENKA" + '\n' + "VISITOR TICKET"
    printer.text(text)
    printer.set(text_type='NORMAL', width=1, height=1, align='left')
    text = '\n' + '\n' + "SID:        " + sid + '\n' + "ROOM:       " + translit(session.room.name_room,
                                                                                 reversed=True) + '\n' + "TARIFF:     " + translit(
        session.tariff.name_tariff, reversed=True) + '\n' + "START:      " + datetime.strftime(session.start_dt,
                                                                                               "%Y-%m-%d %H:%M") + '\n'
    if session.tariff.type == "fp":
        text += "END:        " + datetime.strftime(session.start_dt + timedelta(hours=session.tariff.time_limit.hour,
                                                                                minutes=session.tariff.time_limit.minute),
                                                   "%Y-%m-%d %H:%M") + '\n'
        text += "TOTAL TIME: " + \
            str(session.tariff.time_limit) + " min." + '\n'
    else:
        text += "END:        -----" + '\n'
        text += "TOTAL TIME: -----" + '\n'
    if session.services.all().count() > 0:
        text += "SERVICES:   "
        for service in session.services.all():
            text += translit(service.add_serv.name_service, reversed=True) + " - " + str(
                service.add_serv.price_service * service.count_serv) + " rub." + '\n'
    else:
        text += "SERVICES:   -----" + '\n'
    if session.deposit is not None:
        text += "DEPOSIT:    " + \
            str(session.deposit.cash + session.deposit.card) + " rub." + '\n'
    printer.text(text)
    printer.cut()


def finalCheck(session, printer, sid, i, sumD, addD):
    printer.set(text_type='NORMAL', width=1, height=1, align='left')
    text = '\n' + '\n' + "SID:        " + sid + '\n' + "ROOM:       " + translit(session.room.name_room,
                                                                                 reversed=True) + '\n' + "TARIFF:     " + translit(
        session.tariff.name_tariff, reversed=True) + '\n' + "START:      " + datetime.strftime(session.start_dt,
                                                                                               "%Y-%m-%d %H:%M") + '\n'
    text += "END:        " + \
        datetime.strftime(session.end_dt, "%Y-%m-%d %H:%M") + '\n'
    text += "TOTAL TIME: " + str(int((session.end_dt.astimezone(tzl) -
                                      session.start_dt.astimezone(tzl)).seconds / 60)) + " min." + '\n'

    if session.services.all().count() > 0:
        text += "SERVICES:   "
        for service in session.services.all():
            text += translit(service.add_serv.name_service, reversed=True) + " - " + str(
                service.add_serv.price_service * service.count_serv) + " rub." + '\n'
    fullSum = 0
    if session.cash != None:
        fullSum += session.cash.amount_cash
    if session.card != None:
        fullSum += session.card.amount_card
    text += "SUM: " + str(fullSum) + " rub." + '\n'
    if i == 0:
        text += "PAYMENT:    " + str(fullSum+sumD+addD) + " rub." + '\n'
    else:
        text += "PAYMENT:    " + str(fullSum + sumD) + " rub." + '\n'
    if session.cash != None:
        text += "CASH:       " + str(session.cash.amount_cash) + " rub." + '\n'
    if session.card != None:
        text += "CARD:       " + str(session.card.amount_card) + " rub." + '\n'
    if i == 0:
        text += "CHANGE:   " + str(sumD+addD) + " rub." + '\n' + '\n' + '\n'
    else:
        text += "CHANGE:   " + str(sumD) + " rub." + '\n' + '\n' + '\n'
    printer.text(text)


def stopCheck(session, printer, sid, popul):
    if popul == 0:
        printer.set(align='center')
        printer.image(os.path.join(STATIC_ROOT, 'img/kamenka256.jpg'))
        printer.set(text_type='B', width=2, height=1, align='left')
        text = '\n' + "Time-Club KAMENKA" + '\n' + "VISITOR ACCOUNT"
        printer.text(text)
    printer.set(text_type='NORMAL', width=1, height=1, align='left')

    text = '\n' + '\n' + "SID:          " + sid + '\n' + "ROOM:         " + translit(session.room.name_room,
                                                                                     reversed=True) + '\n' + "TARIFF:     " + translit(
        session.tariff.name_tariff, reversed=True) + '\n' + "START:        " + datetime.strftime(session.start_dt,
                                                                                                 "%Y-%m-%d %H:%M") + '\n'
    text += "END:          " + \
        datetime.strftime(session.end_dt, "%Y-%m-%d %H:%M") + '\n'
    text += "TOTAL TIME:   " + str(int((session.end_dt.astimezone(
        tzl) - session.start_dt.astimezone(tzl)).seconds / 60)) + " min." + '\n'

    if session.services.all().count() > 0:
        text += "SERVICES:     "
        for service in session.services.all():
            text += translit(service.add_serv.name_service, reversed=True) + " - " + str(
                service.add_serv.price_service * service.count_serv) + " rub." + '\n'
    if session.deposit is not None:
        text += "DEPOSIT:      " + \
            str(session.deposit.cash + session.deposit.card) + " rub." + '\n'
    text += "SUM:          " + \
        str(session.sum_session) + " rub." + '\n' + \
        "-----------------------------" + '\n'
    printer.text(text)


def getSID(comp, sid, dt):
    if comp < 10:
        a = datetime.strftime(dt, "%y%m%d") + "00" + str(comp)
    elif comp < 100:
        a = datetime.strftime(dt, "%y%m%d") + "0" + str(comp)
    else:
        a = datetime.strftime(dt, "%y%m%d") + str(comp)
    if sid < 10:
        a += "0" + str(sid)
    else:
        a += str(sid)
    return a


def edit_session(request, pk):
    session = get_object_or_404(Session, SID=int(pk))
    if request.method == "POST":
        form = EditSessionForm(request.POST, instance=session)
        if form.is_valid():
            session = form.save(commit=False)
            if form.cleaned_data["servicesAll"] != None:
                flag = True
                for s in session.services.all():
                    if (s.add_serv == form.cleaned_data["servicesAll"]):
                        s.count_serv += 1
                        s.save()
                        flag = False
                        break
                if flag:
                    useservice = UseService.objects.create(add_serv=Service.objects.filter(
                        name_service=form.cleaned_data["servicesAll"]).first())
                    useservice.save()
                    session.services.add(useservice)
            session.save()
            if (form.cleaned_data["commentAddons"] != None) or len(form.cleaned_data["commentText"]):
                if session.comment:
                    comment = session.comment
                    comment.text = form.cleaned_data["commentText"]
                else:
                    comment = UserComments.objects.create(
                        text=form.cleaned_data["commentText"])
                if form.cleaned_data["commentAddons"] != None:
                    comment.addons.add(form.cleaned_data["commentAddons"])
                comment.save()
                session.comment = comment
            session.save()
            if form.cleaned_data["adddepositSum"] > 0:
                if session.deposit is None:
                    dep = Deposit.objects.create()
                    session.deposit = dep
                else:
                    dep = session.deposit
                if form.cleaned_data["isCard"]:
                    dep.card += form.cleaned_data["adddepositSum"]
                else:
                    dep.cash += form.cleaned_data["adddepositSum"]
                dep.save()
                session.save()
            return redirect('session')
        else:
            return HttpResponse(str(form.errors) + "<br><a href='/session/'>Вернуться назад</a>")
    else:
        form = EditSessionForm(instance=session)
    return HttpResponseRedirect('/session/')


@csrf_exempt
def info_session(request):
    if request.POST:
        if request.is_ajax():
            thisSID = request.POST['thisSID']
            obj = Session.objects.get(SID=thisSID)
            con = dict()
            try:
                con.update(
                    {0: [obj.room.id, obj.tariff.id, obj.SID, obj.discount.id]})
            except:
                con.update({0: [obj.room.id, obj.tariff.id, obj.SID]})
            con.update({1: [""]})
            if obj.comment:
                con.update({1: [obj.comment.text]})
            if obj.deposit is not None:
                con.update({2: [obj.deposit.card, obj.deposit.cash]})
            else:
                con.update({2: [0, 0]})
            return JsonResponse(onemes(con))


@csrf_exempt
def filter_session(request):
    if (request.method == "POST"):
        if request.is_ajax():
            ThisRoom = request.POST['ThisRoom']
            ThisGroup = request.POST['ThisGroup']
            ThisSID = request.POST['ThisSID']
            allRoom = set()
            allGroup = set()
            allSID = set()
            if (ThisRoom != "hide"):
                allSession = Session.objects.filter(
                    is_active=True, room=Room.objects.get(name_room=ThisRoom))
            else:
                allSession = Session.objects.filter(is_active=True)
            if (ThisGroup != "hide"):
                allSession = allSession.filter(company=ThisGroup)
            if (ThisSID != "hide"):
                allSession = allSession.filter(SID=ThisSID)
            qSession = len(allSession)
            con = dict()
            i = 0
            for ses in allSession:
                allGroup.add(ses.company)
                allSID.add(ses.SID)
                allRoom.add(ses.room.name_room)
                ses.fullprice = 0
                ses.fullservs = ""
                for serv in ses.services.all():
                    ses.fullservs += serv.add_serv.name_service
                    if serv.count_serv == 1:
                        ses.fullservs += ","
                    else:
                        ses.fullservs += "-" + str(serv.count_serv) + "шт.,"
                    ses.fullprice += serv.count_serv * serv.add_serv.price_service
                ses.fullservs = ses.fullservs[:-1]
                try:
                    ses.comservs = ""
                    for comserv in ses.comment.addons.all():
                        ses.comservs += comserv.name + ","
                    ses.comservs = ses.comservs[:-1]
                except:
                    pass
                try:
                    com = ses.comment.text
                except:
                    com = ""
                con.update({i: [ses.SID, ses.company, ses.tariff.name_tariff, ses.room.name_room, ses.fullservs,
                                ses.fullprice, ses.comservs, com, ses.sum_session, dateformat.format(ses.start_dt.astimezone(tzl), 'd E Y г. H:i')]})
                i += 1
            allRoom = list(allRoom)
            allGroup = list(allGroup)
            allSID = list(allSID)
            return JsonResponse(fullHDdict(qSession, con, allRoom, allGroup, allSID))


def toFixed(f: float, n=0):
    a, b = str(f).split('.')
    return '{}.{}{}'.format(a, b[:n], '0'*(n-len(b)))


class logoutView(View):
    def get(self, request):
        object = RunStart.objects.first()
        if object.is_run and date.today().weekday() == 5:
            buildChangedPlan()
            object.is_run = False
            object.save()
        if request.user.groups.filter(name='Админ').exists():
            shift = ShiftStart.objects.first()
            prof_object = Profile.objects.get(
                user=User.objects.get(username=request.user))
            obj = UserSalary.objects.create(user=prof_object,
                                            record_type="Рабочая смена",
                                            datetime_start=prof_object.enter_time.astimezone(
                                                tzl),
                                            datetime_end=None)
            shiftTimeStart = datetime.now(tzl).replace(
                hour=shift.timestart.hour, minute=shift.timestart.minute, second=0)
            if obj.datetime_start.time() < shift.timestart and obj.datetime_start.time() > shift.timeweekend:
                obj.datetime_start = obj.datetime_start.replace(
                    hour=shiftTimeStart.hour, minute=shiftTimeStart.minute)

            start = date(obj.datetime_start.year,
                         obj.datetime_start.month, obj.datetime_start.day)
            try:
                weekend = Weekends.objects.get(date=start + timedelta(days=1))
                end = datetime.now(tzl).replace(
                    hour=shift.timeweekend.hour, minute=shift.timeweekend.minute, second=0)
            except:
                end = datetime.now(tzl).replace(
                    hour=shift.timeend.hour, minute=shift.timeend.minute, second=0)
            if end < shiftTimeStart:
                end += timedelta(days=1)

            computeAdminZP(obj, end)

            try:
                res_object = Profile.objects.get(reservedWork=True)
                objR = UserSalary.objects.create(user=res_object, record_type="Рабочая смена",
                                                 datetime_start=res_object.enter_time.astimezone(
                                                     tzl),
                                                 datetime_end=None)
                if objR.datetime_start.time() < shift.timestart and objR.datetime_start.time() > shift.timeweekend:
                    objR.datetime_start = shiftTimeStart
                res_object.reservedWork = False
                res_object.save()
                computeAdminZP(objR, end)
            except:
                pass
            shiftObj = shift.timestart
            if datetime.now(tzl).time() < shiftObj:
                cashYesterday = list(CashPay.objects.filter(is_payed=False, payment_dt__date=datetime.now(
                    tzl).date() - timedelta(days=1)).filter(payment_dt__time__gt=shiftObj))
                cashToday = list(CashPay.objects.filter(
                    is_payed=False, payment_dt__date=date.today()))
                cashToday += cashYesterday
                cardYesterday = list(CardPay.objects.filter(is_payed=False, payment_dt__date=datetime.now(
                    tzl).date() - timedelta(days=1)).filter(payment_dt__time__gt=shiftObj))
                cardToday = list(CardPay.objects.filter(
                    is_payed=False, payment_dt__date=date.today()))
                cardToday += cardYesterday
                listSes = list(Session.objects.filter(
                    end_dt__date=date.today()))
                listSes += list(Session.objects.filter(end_dt__date=datetime.now(
                    tzl).date() - timedelta(days=1), end_dt__time__gt=shiftObj))
                listZP = UserSalary.objects.filter(Q(datetime_end__date=date.today()) | Q(
                    datetime_end__date=datetime.now(tzl).date() - timedelta(days=1), datetime_end__time__gt=shiftObj))
            else:
                cardToday = list(CardPay.objects.filter(
                    is_payed=False, payment_dt__date=date.today(), payment_dt__time__gt=shiftObj))
                cashToday = list(CashPay.objects.filter(
                    is_payed=False, payment_dt__date=date.today(), payment_dt__time__gt=shiftObj))
                listSes = list(Session.objects.filter(
                    end_dt__date=date.today(), end_dt__time__gte=shiftObj))
                listZP = UserSalary.objects.filter(
                    datetime_end__date=date.today(), datetime_end__time__gt=shiftObj)

            _listZP = dict()
            for l in listZP:
                _listZP[l.user.user.username] = 0
            cashMoney = 0
            for cash in cashToday:
                cash.is_payed = True
                cash.save()
                cashMoney += cash.amount_cash
            cardMoney = 0
            for card in cardToday:
                card.is_payed = True
                card.save()
                cardMoney += card.amount_card
            if cashMoney+cardMoney > 0:
                revenue = Orders.objects.create(admin=prof_object, date=datetime.now(tzl), amount=cashMoney+cardMoney, name=TypeOfMO.objects.get(
                    moneyoper=MoneyOperations.objects.get(is_revenue=True)), comment="Нал.: " + str(cashMoney) + " руб., Картой: " + str(cardMoney) + " руб.")
                if datetime.now(tzl).time() < shiftObj:
                    revenue.date -= timedelta(days=1)
                    revenue.save()
            for s in listSes:
                price = 0
                if s.cash != None:
                    price += s.cash.amount_cash
                if s.card != None:
                    price += s.card.amount_card
                admins = listZP.filter(
                    datetime_start__lte=s.end_dt, datetime_end__gte=s.start_dt)
                lenAdmins = admins.count()
                for admin in admins:
                    _listZP[admin.user.user.username] = _listZP.get(
                        admin.user.user.username) + (price*(admin.user.procent / 100))/lenAdmins
            userReportList = ''
            for admin in listZP:
                admin.payedProcent = int(_listZP.get(admin.user.user.username))
                admin.save()
                userReportList += admin.user.user.username + \
                    ' - ' + str(admin.amount) + ' ч., '
            userReportList = userReportList[:-2]
            logout(request)
            return JsonResponse({'card': cardMoney, 'cash': cashMoney, 'ses': len(listSes), 'admins': userReportList})

        else:
            logout(request)
            return HttpResponseRedirect("/")


def computeAdminZP(obj, end):
    if datetime.now(tzl) > end and len(Session.objects.filter(end_dt__gte=end)) > 0:
        obj.datetime_end = end
        e_m = obj.datetime_end.minute / 60
        toFixed(e_m, 2)
        s_m = obj.datetime_start.minute / 60
        toFixed(s_m, 2)
        e = obj.datetime_end.astimezone(tzl).hour + e_m
        s = obj.datetime_start.astimezone(tzl).hour + s_m
        obj.amount = e - s
        object = UserSalary.objects.create(
            user=obj.user,
            record_type="Сверхурочно", datetime_start=end, datetime_end=datetime.now(tzl))

        sverhurochno_e_m = object.datetime_end.minute / 60
        toFixed(sverhurochno_e_m, 2)
        sverhurochno_s_m = object.datetime_start.minute / 60
        toFixed(sverhurochno_s_m, 2)
        sverhurochno_e_h = object.datetime_end.astimezone(
            tzl).hour + sverhurochno_e_m
        sverhurochno_s_h = object.datetime_start.hour + sverhurochno_s_m
        object.amount = sverhurochno_e_h - sverhurochno_s_h
        object.BaseSum = object.user.baseRate * object.amount * 2
        object.BonusSum = object.user.bonusRate * object.amount * 2
        object.save()

    else:
        t_now = datetime.now(tzl)
        t_tra = end
        if t_now <= t_tra and (t_tra - t_now).seconds <= 300:
            obj.datetime_end = t_tra
        elif t_now >= t_tra and (t_now - t_tra).seconds <= 600:
            obj.datetime_end = t_tra
        else:
            obj.datetime_end = t_now
        dts = obj.datetime_start.astimezone(tzl)
        e_m = obj.datetime_end.minute / 60
        toFixed(e_m, 2)
        s_m = dts.minute / 60
        toFixed(s_m, 2)
        e_h = obj.datetime_end.hour + e_m
        s_h = dts.hour + s_m
        obj.amount = e_h - s_h
        if obj.amount < 0:
            obj.amount = 24 + obj.amount
        obj.BaseSum = obj.user.baseRate * obj.amount
        obj.BonusSum = obj.user.bonusRate * obj.amount
    obj.save()


@csrf_exempt
def session_reset(request):
    if (request.method == "POST"):
        if request.is_ajax():
            qSession = len(Session.objects.filter(is_active=True))
            allSession = Session.objects.filter(is_active=True)
            con = dict()
            i = 0
            for ses in allSession:
                ses.fullprice = 0
                ses.fullservs = ""
                for serv in ses.services.all():
                    ses.fullservs += serv.add_serv.name_service
                    if serv.count_serv == 1:
                        ses.fullservs += ","
                    else:
                        ses.fullservs += "-" + str(serv.count_serv) + "шт.,"
                    ses.fullprice += serv.count_serv * serv.add_serv.price_service
                ses.fullservs = ses.fullservs[:-1]
                try:
                    ses.comservs = ""
                    for comserv in ses.comment.addons.all():
                        ses.comservs += comserv.name + ","
                    ses.comservs = ses.comservs[:-1]
                except:
                    pass
                try:
                    com = ses.comment.text
                except:
                    com = ""
                con.update({i: [ses.SID, ses.company, ses.tariff.name_tariff, ses.room.name_room, ses.fullservs,
                                ses.fullprice, ses.comservs, com, ses.sum_session, dateformat.format(ses.start_dt.astimezone(tzl), 'd E Y г. H:i')]})
                i += 1
            s = clientPageStatus.objects.get()
            s.status = request.POST['status']
            s.save()
            f = clientPageInfo.objects.get()
            f.room = "---------"
            f.tariff = "---------"
            f.quantity = ''
            f.services = ''
            f.discount = "---------"
            f.deposit = ''
            f.sum = ''
            f.save()
            shiftstart = ShiftStart.objects.first()
            start = datetime.now(tzl).replace(hour=shiftstart.timestart.hour,
                                              minute=shiftstart.timestart.minute)
            end = datetime.now(tzl)
            if datetime.now(tzl).time() < shiftstart.timestart:
                start -= timedelta(days=1)
            listOffSess = Session.objects.filter(
                is_active=False, start_dt__range=(start, end)).order_by("SID")
            qSession1 = listOffSess.count()
            con1 = dict()
            i = 0
            for ses in listOffSess:
                ses.fullprice = 0
                ses.fullservs = ""
                for serv in ses.services.all():
                    ses.fullservs += serv.add_serv.name_service
                    if serv.count_serv == 1:
                        ses.fullservs += ","
                    else:
                        ses.fullservs += "-" + str(serv.count_serv) + "шт.,"
                    ses.fullprice += serv.count_serv * serv.add_serv.price_service
                ses.fullservs = ses.fullservs[:-1]
                try:
                    ses.comservs = ""
                    for comserv in ses.comment.addons.all():
                        ses.comservs += comserv.name + ","
                    ses.comservs = ses.comservs[:-1]
                except:
                    pass
                try:
                    com = ses.comment.text
                except:
                    com = ""

                con1.update({i: [ses.SID, ses.company, ses.tariff.name_tariff, ses.room.name_room, ses.fullservs, ses.fullprice, ses.comservs, com, 0, dateformat.format(
                    ses.start_dt.astimezone(tzl), 'd E Y г. H:i'), dateformat.format(ses.end_dt.astimezone(tzl), 'd E Y г. H:i'), 0, 0]})
                if ses.card is not None:
                    list = con1.pop(i)
                    list[11] = ses.card.amount_card
                    con1.update({i: list})
                if ses.cash is not None:
                    list = con1.pop(i)
                    list[12] = ses.cash.amount_cash
                    con1.update({i: list})
                list = con1.pop(i)
                list[8] = int(list[11]) + int(list[12])
                con1.update({i: list})
                i += 1
            return JsonResponse(fullfulldict(qSession, con, qSession1, con1))


@csrf_exempt
def session_pay(request):
    if (request.method == "POST"):
        if request.is_ajax():
            sids = request.POST['out1'].split(",")
            sids.remove("")
            cashpaymentsum = request.POST['cashPaymentSum']
            cardpaymentsum = request.POST['cardPaymentSum']
            allpaymentsum = request.POST['allPaymentSum']
            if (len(cashpaymentsum) == 0):
                cashpaymentsum = "0"
            cashpaymentsum = int(cashpaymentsum)
            if (len(cardpaymentsum) == 0):
                cardpaymentsum = "0"
            cardpaymentsum = int(cardpaymentsum)
            if (len(allpaymentsum) == 0):
                allpaymentsum = "0"
            allpaymentsum = int(allpaymentsum)
            my_objects = []
            operatingCashObj = OperatingCash.objects.all().first()
            for elem in sids:
                obj = Session.objects.get(SID=elem)
                if obj.deposit is not None:
                    cardpaymentsum += obj.deposit.card
                    cashpaymentsum += obj.deposit.cash
                    allpaymentsum += obj.deposit.cash + obj.deposit.card
                    obj.sum_session += obj.deposit.cash + obj.deposit.card
                    obj.save()
                my_objects.append(obj)
            paymentclient = allpaymentsum-(cashpaymentsum+cardpaymentsum)
            if paymentclient > 0:
                ret = "error"
                return JsonResponse(fulldict(ret, paymentclient))
            else:
                for obj in my_objects:
                    while (obj.sum_session > 0):
                        if (cardpaymentsum > 0):
                            if (obj.card == None):
                                pay = CardPay(
                                    payment_dt=datetime.now(timezone.utc))
                            else:
                                pay = obj.card
                            if (cardpaymentsum >= obj.sum_session):
                                pay.amount_card = obj.sum_session
                                cardpaymentsum -= pay.amount_card
                                obj.sum_session = 0
                            else:
                                pay.amount_card = cardpaymentsum
                                obj.sum_session -= pay.amount_card
                                cardpaymentsum = 0
                            pay.save()
                            obj.card = pay
                        elif (cashpaymentsum > 0):
                            if (obj.cash == None):
                                pay = CashPay(
                                    payment_dt=datetime.now(timezone.utc))
                            else:
                                pay = obj.cash
                            if (cashpaymentsum >= obj.sum_session):
                                pay.amount_cash = obj.sum_session
                                cashpaymentsum -= pay.amount_cash
                                obj.sum_session = 0
                            else:
                                pay.amount_cash = cashpaymentsum
                                obj.sum_session -= pay.amount_cash
                                cashpaymentsum = 0

                            operatingCashObj.cash_sum_now += pay.amount_cash
                            pay.save()
                            obj.cash = pay
                    obj.is_active = False
                    obj.save()
                    try:
                        depObj = Deposit.objects.get(session=obj)
                        depObj.delete()
                    except:
                        pass
                con = dict()
                if paymentclient == 0:
                    ret = "equal"
                else:
                    paymentclient = -paymentclient
                    ret = "dolg"
                operatingCashObj.save()
                lenSes = len(Session.objects.filter(is_active=True))
                i = 0
                try:
                    sumDolg = int(paymentclient / len(my_objects))
                    addingDolg = int(paymentclient % len(my_objects))
                except:
                    sumDolg = 0
                    addingDolg = 0
                printerStart = PrinterStart.objects.first()
                if printerStart.is_print:
                    printer = Network(printerStart.network)
                    printer.set(align='center')
                    printer.image(os.path.join(
                        STATIC_ROOT, 'img/kamenka256.jpg'))
                    printer.set(text_type='B', width=2, height=1, align='left')
                    text = '\n' + "Time-Club KAMENKA" + '\n' + "VISITOR CHECK"
                    printer.text(text)
                for obj in my_objects:
                    sid = getSID(obj.company, obj.SID, obj.start_dt)
                    if printerStart.is_print:
                        finalCheck(obj, printer, sid, i, sumDolg, addingDolg)
                    i += 1
                if printerStart.is_print:
                    printer.set(text_type='B', width=4,
                                height=2, align='center')
                    printer.text("CHECK PAYED!")
                    printer.cut()
                    printer.set(text_type='NORMAL', width=1,
                                height=1, align='left')
                con.update(
                    {0: [paymentclient, operatingCashObj.cash_sum_now, lenSes]})
                return JsonResponse(fulldict(ret, con))


@csrf_exempt
def stop_session(request):
    if (request.method == "POST"):
        if request.is_ajax():
            sids = request.POST['out']
            sids = sids[:-1].split(',')
            con = dict()
            allprices = dict()
            listSes = list()
            counter = 0
            freePeople = 0
            allprice = 0
            noOwners = dict()
            deps = 0
            for s in sids:
                listSes.append(Session.objects.get(SID=s))
            for session in listSes:
                session.end_dt = datetime.now(tzl)
                tDelta = int((session.end_dt.astimezone(tzl) -
                              session.start_dt.astimezone(tzl)).seconds / 60)
                if session.tariff.type == "tp":
                    if session.discount != None:
                        if session.owner:
                            session.sum_session = 0
                            try:
                                del noOwners[session.SID]
                            except:
                                pass
                        else:
                            session.sum_session = int(
                                tDelta * session.tariff.price_tariff * session.discount.multiplier)
                            for noOwner in Session.objects.filter(company=session.company, owner=True):
                                noOwners[noOwner.SID] = noOwner.SID
                    else:
                        session.sum_session = int(
                            tDelta * session.tariff.price_tariff)
                elif session.tariff.type == "fp":
                    if (session.owner == False):
                        session.sum_session = session.tariff.price_fix1
                    else:
                        session.sum_session = 0
                    externtime = tDelta - \
                        (session.tariff.time_limit.hour*60 +
                         session.tariff.time_limit.minute)
                    if externtime > 0:
                        session.sum_session += externtime * session.tariff.price_tariff
                commentService = ""
                for serv in session.services.all():
                    session.sum_session += serv.add_serv.price_service * serv.count_serv
                    commentService += str(serv.add_serv.name_service) + " - " + str(
                        serv.count_serv) + "шт. на сумму: " + str(serv.add_serv.price_service * serv.count_serv)
                if session.deposit is not None:
                    deps += session.deposit.cash + session.deposit.card
                    session.sum_session -= session.deposit.cash
                    session.sum_session -= session.deposit.card
                allprice += session.sum_session
                session.save()
                commentDate = datetime.strftime(session.start_dt.astimezone(
                    tzl), "%H:%M") + " - " + datetime.strftime(session.end_dt.astimezone(tzl), "%H:%M") + " (" + str(tDelta) + " мин.)"
                con.update({counter: [commentDate, session.tariff.name_tariff,
                                      session.sum_session, commentService, session.room.name_room]})
                counter += 1
            for noOwner in noOwners:
                obj = Session.objects.get(SID=noOwner)
                obj.owner = False
                obj.save()
            allprices.update({0: allprice})
            allprices.update({1: counter})
            s = clientPageStatus.objects.get()
            s.status = request.POST['status']
            s.save()
            f = clientPageInfo.objects.get()
            f.quantity = len(sids)
            f.services = request.POST['servs']
            f.aSum = allprice
            f.deposit = deps
            f.room = request.POST['room']
            f.tariff = request.POST['tariff']
            f.save()
            return JsonResponse(fulldict(allprices, con))


@csrf_exempt
def admin_helper(request):
    if (request.method == "POST"):
        if request.is_ajax():
            depositSet = Session.objects.filter(
                is_active=True).exclude(deposit=None)
            con = dict()
            con1 = dict()
            qS = dict()
            i = 0
            includeThisDeposit = False
            for session in depositSet:
                session.end_dt = datetime.now(tzl)
                tDelta = int((session.end_dt.astimezone(tzl) -
                              session.start_dt.astimezone(tzl)).seconds / 60)
                try:
                    session.sum_session = int(
                        tDelta * session.tariff.price_tariff * session.discount.multiplier)
                except:
                    session.sum_session = int(
                        tDelta * session.tariff.price_tariff)
                sumDelta = session.deposit.cash + session.deposit.card - session.sum_session
                if sumDelta <= (10 * session.tariff.price_tariff) and sumDelta >= 0:
                    includeThisDeposit = True
                    sumDelta /= session.tariff.price_tariff
                    con.update(
                        {i: [session.SID, session.room.name_room, session.company, sumDelta]})
                    i += 1
            if (includeThisDeposit):
                qS.update({0: "+"})
                qS.update({1: i})
                qS.update({2: 0})
            else:
                qS.update({0: "-"})
                qS.update({1: 0})
                qS.update({2: 0})

            # depositSet = depositSet.filter(session__tariff__type="fp", session__owner = False).distinct("session__company")
            depositSet = depositSet.filter(tariff__type="fp", owner=False)
            _depositSet = dict()
            for session in depositSet:
                if session.company not in _depositSet:
                    _depositSet[session.company] = session
            depositSet = _depositSet.values()

            i = 0
            includeThisDeposit = False

            for session in depositSet:
                session.end_dt = datetime.now(tzl)
                timeLimit = (session.end_dt - session.start_dt)/60
                if timeLimit.seconds < 10 and timeLimit.seconds >= 0:
                    includeThisDeposit = True
                    con1.update(
                        {i: [session.room.name_room, session.company, timeLimit.seconds]})
                    i += 1
            if (includeThisDeposit):
                qS.update({0: "+"})
                qS.update({2: i})
            return JsonResponse(calendarDict(qS, con, con1))


@csrf_exempt
def dis_tar(request):
    if request.POST:
        if request.is_ajax():
            con = dict()
            i = 0
            con.update({0: 0})
            try:
                thisDiscount = Discounts.objects.get(
                    id=int(request.POST['idDiscount']))
                con.update({0: thisDiscount.people_limit})
            except:
                thisTariff = Tarif.objects.get(
                    id=int(request.POST['idTariff']))
                con.update({1: thisTariff.people_limit1})
            return JsonResponse(onemes(con))


@csrf_exempt
def tar_dis(request):
    if request.POST:
        if request.is_ajax():
            con = dict()
            i = 0
            con.update({0: [-1]})
            try:
                thisTariff = Tarif.objects.get(
                    id=int(request.POST['idTariff']))
                for discount in thisTariff.discounts.filter(days__id=1+datetime.today().weekday()):
                    con.update({i: [discount.id, thisTariff.people_limit1]})
                    i += 1
            except:
                pass
            return JsonResponse(onemes(con))


@csrf_exempt
def session_error_delete(request):
    if request.POST:
        if request.is_ajax():
            sids = request.POST['out2'].split(",")
            sids.remove("")
            con = dict()
            i = 0
            timeToDelete = SettingStart.objects.first().timetodelete
            for sid in sids:
                ses = Session.objects.get(SID=int(sid))
                ses.end_dt = datetime.now(tzl)
                tDelta = int((ses.end_dt.astimezone(tzl) -
                              ses.start_dt.astimezone(tzl)).seconds / 60)
                if tDelta <= timeToDelete:
                    if ses.comment is not None:
                        ses.comment.delete()
                    for service in ses.services.all():
                        service.delete()
                    ses.delete()
            listsess = Session.objects.filter(is_active=True).order_by("SID")
            for ses in listsess:
                ses.fullprice = 0
                ses.fullservs = ""
                for serv in ses.services.all():
                    ses.fullservs += serv.add_serv.name_service
                    if serv.count_serv == 1:
                        ses.fullservs += ","
                    else:
                        ses.fullservs += "-" + str(serv.count_serv) + "шт.,"
                    ses.fullprice += serv.count_serv * serv.add_serv.price_service
                ses.fullservs = ses.fullservs[:-1]
                try:
                    ses.comservs = ""
                    for comserv in ses.comment.addons.all():
                        ses.comservs += comserv.name + ","
                    ses.comservs = ses.comservs[:-1]
                except:
                    pass
                try:
                    com = ses.comment.text
                except:
                    com = ""
                con.update({i: [ses.SID, ses.company, ses.tariff.name_tariff, ses.room.name_room, ses.fullservs,
                                ses.fullprice, ses.comservs, com, ses.sum_session,
                                dateformat.format(ses.start_dt.astimezone(tzl), 'd E Y г. H:i')]})
                i += 1
            return JsonResponse(fulldict(i, con))


@csrf_exempt
def print_session(request):
    if request.POST:
        if request.is_ajax():
            sids = request.POST['out']
            sids = sids[:-1].split(',')
            printerStart = PrinterStart.objects.first()
            if printerStart.is_print:
                printer = Network(printerStart.network)
            fullSum = 0
            popul = 0
            for s in sids:
                ses = Session.objects.get(SID=s)
                sid = getSID(ses.company, ses.SID, ses.start_dt)
                if printerStart.is_print:
                    stopCheck(ses, printer, sid, popul)
                fullSum += ses.sum_session
                popul += 1
            if printerStart.is_print:
                text = "TOTAL AMOUNT: " + str(fullSum) + " rub." + '\n'
                printer.text(text)
                printer.set(align='center')
                qrcode = "smsto:900:Перевод " + PhoneSberbank.objects.first().phone + " " + \
                    str(fullSum)
                printer.qr(qrcode, size=8)
                printer.cut()
            return JsonResponse(onemes("OK"))


def clientPage(request):
    context = {}
    return render(request, 'session/clientPage.html', context)


@csrf_exempt
def changeClientPage(request):
    if (request.method == "POST"):
        if request.is_ajax():
            f = clientPageInfo.objects.get()
            setattr(f, request.POST['field'], request.POST['text'])
            f.save()
            return HttpResponse('')


@csrf_exempt
def updateClientInfo(request):
    if (request.method == "POST"):
        if request.is_ajax():
            f = clientPageInfo.objects.get()
            r = f.room
            t = f.tariff
            q = f.quantity
            s = f.services
            d = f.discount
            p = f.deposit
            m = f.aSum
            return JsonResponse({'r': r, 't': t, 'q': q, 's': s, 'd': d, 'p': p, 'm': m})


@csrf_exempt
def setClientPageStatus(request):
    if (request.method == "POST"):
        if request.is_ajax():
            s = clientPageStatus.objects.get()
            s.status = request.POST['status']
            s.save()
            return HttpResponse('')


@csrf_exempt
def checkClientPageStatus(request):
    if (request.method == "POST"):
        if request.is_ajax():
            s = clientPageStatus.objects.get()
            return JsonResponse({'s': s.status})


@csrf_exempt
def clearClientPageInfo(request):
    if (request.method == "POST"):
        if request.is_ajax():
            f = clientPageInfo.objects.get()
            f.room = "---------"
            f.tariff = "---------"
            f.quantity = ''
            f.services = ''
            f.discount = "---------"
            f.deposit = ''
            f.sum = ''
            f.save()
            return HttpResponse('')


@csrf_exempt
def set_default(request):
    if request.POST:
        if request.is_ajax():
            sessions = Session.objects.filter(is_active=True)
            _sessions = dict()
            for session in sessions:
                if session.room not in _sessions:
                    _sessions[session.room] = session.room
            sessions = _sessions.values()
            rooms = Room.objects.all().order_by("capasity")

            for room in rooms:
                if room not in _sessions:
                    return JsonResponse(onemes(room.id))
            return JsonResponse(onemes("-1"))


@csrf_exempt
def weekday_tar_dis(request):
    if request.is_ajax():
        con0 = dict()
        con1 = dict()
        i = 0
        for discount in Discounts.objects.filter(days__id=1+datetime.today().weekday()):
            con1.update({i: [discount.id, discount.name]})
            i += 1
        con0.update({0: i})
        con2 = dict()
        i = 0
        for tarif in Tarif.objects.filter(days__id=1 + datetime.today().weekday()):
            con2.update({i: [tarif.id, tarif.name_tariff]})
            i += 1
        con0.update({1: i})

        return JsonResponse(calendarDict(con1, con0, con2))
