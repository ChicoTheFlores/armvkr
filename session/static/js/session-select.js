

$('#registry-buttonsOfType-Room').on('change', function () { SessionFilter("r", "#registry-buttonsOfType-Room", "#registry-buttonsOfType-Group", "#registry-buttonsOfType-SID") })
$('#registry-buttonsOfType-Group').on('change', function () { SessionFilter("g", "#registry-buttonsOfType-Room", "#registry-buttonsOfType-Group", "#registry-buttonsOfType-SID") })
$('#registry-buttonsOfType-SID').on('change', function () { SessionFilter("s", "#registry-buttonsOfType-Room", "#registry-buttonsOfType-Group", "#registry-buttonsOfType-SID") })
function SessionFilter(type, roomId, groupId, sidId) {
    if (type == "r") {
        $(groupId).val("hide");
        $(sidId).val("hide");
    }
    else if (type == "g") {
        $(sidId).val("hide");
    }
    $.ajax({
        method: "POST",
        url: '/session/filter-session/',
        data: {
            'ThisRoom': $(roomId + " :selected").val(),
            'ThisGroup': $(groupId + " :selected").val(),
            'ThisSID': $(sidId + " :selected").val(),
        },
        dataType: 'json',
        success: function (data) {
            $("#wd_id tr").remove();
            for (var i = 0; i < data.qS; i += 1) {
                var table_row = '<tr class="table-registry-tr simpleTr" id="' + data.con[i][0] + '">';
                table_row += '<td class="SID"><p>' + data.con[i][0] + '</p></td>';
                table_row += '<td><p>';
                table_row += data.con[i][3];
                table_row += '</p></td><td><p>' + data.con[i][1] + '</p></td>';
                table_row += '<td><p>' + data.con[i][2] + '</p></td>';
                table_row += '<td><p>' + data.con[i][9] + '</p></td>';
                table_row += '<td><p title="' + data.con[i][5] + ' руб.">' + data.con[i][5] + '</p></td>';
                table_row += '<td><p>' + data.con[i][6] + '</p></td>';
                table_row += '<td class="COM"><p>' + data.con[i][7] + '</p></td><td class="SUM"><p>' + data.con[i][8] + '</p></td>';
                table_row += '</tr>'
                $('#wd_id').append(table_row);
            }
            sidSelected = $(sidId + " :selected").val();
            if (type == "r") {
                selectReDrawSes(groupId, data.aG, 'Все компании');
            }
            selectReDrawSes(sidId, data.aS, 'Все посетители');
            $(sidId).val(sidSelected);
        }
    })
};

function selectReDrawSes(selector, items, text) {
    $(selector).find('option').remove();
    $(selector).append($('<option>', {
        value: 'hide',
        text: text
    }));
    $.each(items, function (i, item) {
        $(selector).append($('<option>', {
            value: item,
            text: item
        }));
    });
}