
$('#warehouseIn-category').on('change', function () { WarehouseFilter("cat", "#warehouseIn-category", "#warehouseIn-subcategory", "#warehouseIn-good") })
$('#warehouseIn-subcategory').on('change', function () { WarehouseFilter("subcat", "#warehouseIn-category", "#warehouseIn-subcategory", "#warehouseIn-good") })

$('#warehouseOut-category').on('change', function () { WarehouseFilter("cat", "#warehouseOut-category", "#warehouseOut-subcategory", "#warehouseOut-good"); killSelect('#warehouseOut-volume', '-- Объем --') })
$('#warehouseOut-subcategory').on('change', function () { WarehouseFilter("subcat", "#warehouseOut-category", "#warehouseOut-subcategory", "#warehouseOut-good"); killSelect('#warehouseOut-volume', '-- Объем --') })

$('#warehouse-selects-category').on('change', function () { WarehouseFilter("cat", "#warehouse-selects-category", "#warehouse-selects-subcategory", "#warehouse-selects-good", true) })
$('#warehouse-selects-subcategory').on('change', function () { WarehouseFilter("subcat", "#warehouse-selects-category", "#warehouse-selects-subcategory", "#warehouse-selects-good", true) })
$('#warehouse-selects-good').on('change', function () { WarehouseFilter("good", "#warehouse-selects-category", "#warehouse-selects-subcategory", "#warehouse-selects-good", true) })
$('#period-selects-category').on('change', function () { WarehouseFilter("cat", "#period-selects-category", "#period-selects-subcategory", "#period-selects-good", true, ".period-table-tr", ".period-table-good-td") })
$('#period-selects-subcategory').on('change', function () { WarehouseFilter("subcat", "#period-selects-category", "#period-selects-subcategory", "#period-selects-good", true, ".period-table-tr", ".period-table-good-td") })
$('#period-selects-good').on('change', function () { WarehouseFilter("good", "#period-selects-category", "#period-selects-subcategory", "#period-selects-good", true, ".period-table-tr", ".period-table-good-td") })
$('#warehouseOut-good').on('change', function () {
    $volume = $('#warehouseOut-volume');
    if ($("#warehouseOut-good" + ' :selected').val() != 'hide') {
        $($volume).find('option').remove();
        $.ajax({
            method: "POST",
            url: '/warehouse/VolumeFilter/',
            data: {
                'good': $("#warehouseOut-good" + " :selected").val(),
            },
            dataType: 'json',
            success: function (data) {
                selectReDraw($volume, data.aV, '-- Объем --')
            }
        })
    }
    else {
        killSelect($volume, '-- Объем --');
    }
})
$('#warehouseOut-volume').on('change', function () {
    $volume = $("#warehouseOut-volume" + ' :selected').val();
    if ($volume != 'hide') {
        $good = $("#warehouseOut-good" + ' :selected').val();
        $('#outModalAmount').prop('disabled', false);
        $.ajax({
            method: "POST",
            url: '/warehouse/AmountFilter/',
            data: {
                'good': $good,
                'volume': $volume,
            },
            dataType: 'json',
            success: function (data) {
                $('#outModalAmount').prop('max', data.a);
                $('#outModalAmount').val('');
            }
        })
    }
});
$('#logout_button_simple').click(function () {
    $.ajax({
        method: 'GET',
        url: '/session/logout/',
        success: function (data) {
            $('#dailyReport-customers').text('Посетителей:' + data.ses);
            $('#dailyReport-cashIncome').text('Наличные: ' + data.cash);
            $('#dailyReport-cardIncome').text('Карта: ' + data.card);
            $('#dailyReport-admins').text('Админы: ' + data.admins);
        }
    })
})
$('#cashreg-selects-users').on('change', function () {
    $admin = $('#cashreg-selects-users :selected').val();
    if ($admin != 'hide') {
        $('body').find('.table-cash-content-tr').each(function () {
            if ($(this).find('.table-cash-content-tr-profile').text() == $admin) {
                if ($(this).hasClass('disabledByUsers')) {
                    $(this).removeClass('disabledByUsers');
                }
            }
            else {
                $(this).addClass('disabledByUsers')
            }
        })
    }
    else {
        $('body').find('.table-cash-content-tr').each(function () {
            if ($(this).hasClass('disabledByUsers')) {
                $(this).removeClass('disabledByUsers');
            }
        })
    }
    computeTotalSumD();
})
$('#cashreg-selects-category').on('change', function () {
    $cat = $('#cashreg-selects-category :selected').val();
    $.ajax({
        method: 'POST',
        url: '/admins/selectOperation/',
        data: {
            'cat': $cat,
        },
        dataType: 'json',
        success: function (data) {
            selectReDraw('#cashreg-selects-subcategory', data.aG, 'Все операции');
            $('body').find('.table-cash-content-tr').each(function () {
                if ($.inArray($(this).find('.table-cash-content-tr-detailed').text(), data.aG) != -1) {
                    if ($(this).hasClass('disabledByOperations')) {
                        $(this).removeClass('disabledByOperations');
                    }
                }
                else {
                    $(this).addClass('disabledByOperations')
                }
            })
            computeTotalSumD();
        }
    })
})
$('#cashreg-selects-subcategory').on('change', function () {
    $val = $('#cashreg-selects-subcategory :selected').val();
    if ($val != 'hide') {
        $('body').find('.table-cash-content-tr').each(function () {
            if ($(this).find('.table-cash-content-tr-detailed').text() == $val) {
                if ($(this).hasClass('disabledByOperations')) {
                    $(this).removeClass('disabledByOperations');
                }
            }
            else {
                $(this).addClass('disabledByOperations');
            }
        })
    }
    else {
        $('body').find('.table-cash-content-tr').each(function () {
            if ($(this).hasClass('disabledByOperations')) {
                $(this).removeClass('disabledByOperations');
            }
        })
    }
    computeTotalSumD();
})
function WarehouseFilter(type, catId, subcatId, goodId, cons = false, ttr = '.table-consumables-tr', ttd = '.table-consumables-good-td') {
    $cat = $(catId + " :selected").val();
    if (type == 'cat') {
        $subcat = 'hide';
        $good = 'hide';
    }
    else {
        $subcat = $(subcatId + " :selected").val();
    }

    $.ajax({
        method: "POST",
        url: '/warehouse/WarehouseFilter/',
        data: {
            'ThisCat': $cat,
            'ThisSubcat': $subcat,
        },
        dataType: 'json',
        success: function (data) {
            if (type != "good") {
                if (cons == true) {
                    $('body').find(ttr).each(function () {
                        var t = ($(this).find(ttd).text());
                        if ($.inArray(t, data.aG) != -1) {
                            console.log(this);
                            $(this).css({ display: 'table-row' });
                        }
                        else {
                            $(this).css({ display: 'none' });
                        }
                    })
                }

                if (type == "cat") {
                    selectReDraw(subcatId, data.aS, '-- Все подкатегории --');
                }
                selectReDraw(goodId, data.aG, '-- Все товары --');
            }
            else {
                var tG = $(goodId + " :selected").val();
                if (tG != "hide") {
                    $('body').find(ttr).each(function () {
                        var t = ($(this).find(ttd).text());
                        if (tG == t) {
                            $(this).css({ display: 'table-row' });
                        }
                        else {
                            $(this).css({ display: 'none' });
                        }
                    })
                }
                else {
                    $('body').find(ttr).each(function () {
                        var t = ($(this).find(ttd).text());
                        if ($.inArray(t, data.aG) != -1) {
                            $(this).css({ display: 'table-row' });
                        }
                        else {
                            $(this).css({ display: 'none' });
                        }
                    })
                }
            }

        }
    })
};

function selectReDraw(selector, items, text) {
    $(selector).find('option').remove();
    $(selector).append($('<option>', {
        value: 'hide',
        text: text
    }));
    $.each(items, function (i, item) {
        $(selector).append($('<option>', {
            value: item,
            text: item
        }));
    });
}
function killSelect(select, text) {
    $(select).find('option').remove();
    $(select).append($('<option>', {
        value: 'hide',
        text: text
    }));
    $('#outModalAmount').prop('disabled', true);
    $('#outModalAmount').val('');
}
function computeTotalSumD() {
    $td = '#cashregTotalSum';
    $sum = 0;
    $('body').find('.table-cash-content-tr').each(function () {
        if ($(this).css('display') != 'none') {
            if ($(this).find('.table-cash-content-tr-type').text() == 'Приход') {
                $sum += parseInt($(this).find('.table-cash-content-tr-sum').text());
            }

            else {
                $sum -= parseInt($(this).find('.table-cash-content-tr-sum').text());
            }
        }
    })
    $($td).text($sum);
}